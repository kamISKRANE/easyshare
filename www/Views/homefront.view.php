<!DOCTYPE html>
<html>

<head>
    <title>Accueil Easy-Share</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/grid.css">
    <meta name="description" content="Créez facilement votre plateforme multimédia avec Easy-Share. Partagez vos médias .">
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>

    <main>

        <section id="banner" class="row">
            <article class=" col-md-offset-2 col-md-8">
                <div class="container2 col-md-9">
                    <h1>Vivez votre passion pour la musique comme vous le sentez</h1>
                </div>
                <div class="container2 col-md-6">
                    <h2>Bénéficiez d'un accès privilégié 
                     aux meilleures nouveautés de la planète, 
                     sélectionnées par une équipe de passionnés 
                     de la qualité, de la créativité et de la beauté.
                  </h2>
            </article>
            </div>
            <article class=" col-md-offset-2 col-md-6">
                <div class="container2 col-md-12">
                    <a href="/creation_de_compte" class="cta-button cta-button--white">Découvrir</a>
                </div>
            </article>
        </section>

        <section id="services" class="row">
            <article class="col-md-offset-2 col-md-8">
                
                    <div class="box4 col-md-12">
                        <h1>Qu’est-ce l'expérience <i>Easy-Share ?</i></h1>
            </article>
            <article class="col-md-offset-2 col-md-4">
                <div class="box2">
                    <div class="col-md-12">
                        <img src="public/icons/equalizer.png" id="toto">
                        <p>Bénéficiez des statistiques en temps réel ! Voyez quels fans écoutent le plus vos morceaux et où se trouvent vos meilleurs fans.
                            <br>
                            <a href="/creation_de_compte" class="cta-button1 cta-button1--green">Découvrir</a>
            </article>
            <br>
            <article class="col-md-4">
                <div class="box2">
                    <div class="col-md-12">
                        <img src="public/icons/users.png">
                        <p>Trouvez votre communauté ! Partagez votre travail avec des millions d'auditeurs quotidiens ou partagez un lien privé avec vos collaborateurs.
                            <br>
                            <a href="/creation_de_compte" class="cta-button1 cta-button1--green">Découvrir</a>
            </article>
            <br>
            <article class="col-md-offset-2 col-md-4">
                <div class="box2">
                    <div class="col-md-12">
                        <img src="public/icons/chat (2).png">
                        <p>Ne faites plus qu'un avec vos fans ! Grâce aux interactions et aux commentaires, ne manquez jamais une occasion de vous connecter et d'obtenir des commentaires.
                            <br>
                            <a href="/creation_de_compte" class="cta-button1 cta-button1--green">Découvrir</a>
                            <br>
                    </div>
            </article>
            <article class=" col-md-4">
                <div class="box2">
                    <div class="col-md-12">
                        <img src="public/icons/computer.png">
                        <p>Soyez maitre de votre plateforme. Nous vous aidons à le construire pas à pas pour qu'il soit à votre image et en accord avec vos besoins.
                            <br>
                            <a href="/creation_de_compte" class="cta-button1 cta-button1--green">Découvrir</a>
                            <br>
                    </div>
            </article>
        </section>

        <section id="banner2" class="row">
            <article class=" col-md-offset-2 col-md-4">
                <div class="container2 col-md-12">
                    <h1><b>Le coeur, les idées, l'intimité des artistes.</h1></b>
                </div>
            </article>
            <article class="  col-md-4">
                <div class="container2 col-md-12">
                    <div style="padding:50% 0 0 0;position:relative;">
                        <iframe src="https://player.vimeo.com/video/325915924?title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                    </div>
                    <script src="https://player.vimeo.com/api/player.js"></script>
                </div>
            </article>
            <article class=" col-md-offset-2 col-md-4">
                <div class="container2 col-md-12">
                    <h2>Redécouvrez la musique, son histoire, ses secrets, 
         ses racines, ses influences, à travers des contenus
         éditoriaux exclusivement réservés à nos abonnés 
         (articles, chroniques, interviews, vidéos...)</h2>
            </article>
            </div>
        </section>

        <section id="decouvertes" class="row">
            <article class=" col-md-offset-2 col-md-8">
                <div class="box4 col-md-12">
                    <h1><b>Découvertes</h1></b>
                </div>
            </article>
            <article class="album col-md-offset-2 col-md-2">
                <figure></figure>
                <h2>The Coast - 2019</h2>
            </article>
            <article class="album col-md-2">
                <figure></figure>
                <h2>Cosmic Surfer - 2018</h2>
            </article>
            <article class="album col-md-2">
                <figure></figure>
                <h2>A day in the life - 2019</h2>
            </article>
            <article class="album col-md-2">
                <figure></figure>
                <h2>In the shadows - 2019</h2>
            </article>
        </section>

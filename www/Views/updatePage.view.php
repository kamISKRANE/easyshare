<?php

    namespace Easyshare\Views;

use Easyshare\Core\Routing;
    use Easyshare\Core\Form;

    ?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title></title>
	<script src="https://cdn.ckeditor.com/4.8.0/full-all/ckeditor.js"></script>
</head>

<body>
        <section class="row">
           <article class="col-md-offset-1 col-md-10">
            </br><?php Form::showFormInformationsAndErrors(); ?>
                <label for="title"><h1>Modification de la page Web : <span style="color:green;"><?php echo preg_replace('#/#', '', $oldUrl); ?></span></label>
                    <img src="public/icons/edit.png" class="icon-side">
                </h1>
            </article></section>
        <section class"box1 row col-md-offset-1 col-md-10">
            <article class="box1 col-md-offset-1 col-md-10">
                <?php
                    $this->addModal("form", $configFormUpdatePage);
                ?>
            </article>
        </section>

    <script>
        
        CKEDITOR.replace('editor');
        var urlAlreadyTaken = <?php echo $urlAlreadyTaken ; ?> ;
        var currentUrl = "<?php echo $oldUrl ; ?>";

        function checkIfUrlIsAvailable(){
            var urlElement = document.getElementById('newUrl');
            var url = urlElement.value.trim();

            //Verif si l'url choisie par l'Admin n'est pas deja utilisé par une autre page.
            if( (urlAlreadyTaken.indexOf('/'+url.toLowerCase())!= -1) && (url != currentUrl) ){
                alert("Cette URL n'est pas disponible, veuillez en saisir une autre");
                urlElement.style.background = '#FF3232';
                return false;
            }
            return true;
        };

    </script>
</body>
</html>
<section id="services" class="row">
	<article class="col-md-offset-2 col-md-8">

	<h1> Mentions Légales </h1>
		<p>Conformément aux dispositions des Articles 6-III et 19 de la Loi n°2004-575 du 21 juin 2004
		pour la Confiance dans l'économie numérique, dite L.C.E.N., il est porté à la connaissance
		des utilisateurs et visiteurs du site EasyShare les présentes mentions légales.
		Le site Easy Share est accessible à l'adresse suivante : www.easyshare.fr . L'accès et
		l'utilisation du Site sont soumis aux présentes " Mentions légales" détaillées ci-après ainsi
		qu'aux lois et/ou règlements applicables.<br><br>
		La connexion, l'utilisation et l'accès à ce Site impliquent l'acceptation intégrale et sans
		réserve de l'internaute de toutes les dispositions des présentes Mentions Légales.<br><br></p>

	<h3>ARTICLE 1 - INFORMATIONS LÉGALES</h3>
		<p>En vertu de l'Article 6 de la Loi n° 2004-575 du 21 juin 2004 pour la confiance dans
		l'économie numérique, il est précisé dans cet article l'identité des différents intervenants
		dans le cadre de sa réalisation et de son suivi.<br><br>
		<b>A. Editeur du site</b><br><br>
		Le site EasyShare est édité par :
		Kamel ISKRANE<br>
		Domicilié à l'adresse suivante : 17 rue richard lenoir 75011.<br>
		Téléphone : 0601797435<br>
		Adresse e-mail : easyshare.esgi1@gmail.com<br><br>
		<b>B. Directeur de publication</b><br><br>
		Le Directeur de publication est :
		Kamel ISKRANE<br>
		Adresse e-mail de contact : easyshare.esgi1@gmail.com<br><br>
		
		<b>C. Hébergeur du site</b><br><br>
		Le site Easy Share est hébergé par :
		easyshare<br>
		dont le siège est situé à l'adresse suivante : 17 rue richard lenoir 75011<br>
		Téléphone : 0601797435<br>
		Adresse e-mail : easyshare.esgi1@gmail.com.<br><br>
		D. Utilisateurs<br>
		Sont considérés comme utilisateurs tous les internautes qui naviguent, lisent, visionnent et
		utilisent le site Easy Share.<br><br></p>

	<h3>ARTICLE 2 - ACCESSIBILITÉ</h3>
		<p>Le Site est par principe accessible aux Utilisateurs 24/24h et 7/7j, sauf interruption,
		programmée ou non, pour des besoins de maintenance ou en cas de force majeure.
		En cas d'impossibilité d'accès au Site, celui-ci s'engage à faire son maximum afin d'en rétablir
		l'accès. Le Site ne saurait être tenu pour responsable de tout dommage, quelle qu'en soit la
		nature, résultant de son indisponibilité.<br><br></p>

	<h3>ARTICLE 3 - LOI APPLICABLE ET JURIDICTION</h3>
		<p>Les présentes Mentions Légales sont régies par la loi française. En cas de différend et à
		défaut d'accord amiable, le litige sera porté devant les tribunaux français conformément aux
		règles de compétence en vigueur.<br><br></p>

	<h3>ARTICLE 4 - CONTACT</h3>
		<p>Pour tout signalement de contenus ou d'activités illicites, l'Utilisateur peut contacter
		l'Éditeur à l'adresse suivante : easyshare.esgi1@gmail.com, ou par courrier recommandé avec
		accusé de réception adressé à l'Éditeur aux coordonnées précisées dans les présentes
		mentions légales.
		Données anonymes
		Nous pouvons également utiliser des cookies, et éventuellement, d’autres captures de variables
		d’environnement, gestion de fichiers d’audit et d’ouverture du répertoire des fichiers Internet
		2
		temporaires (cache). Dans ce cas, vous êtes informés de l’existence de ces procédés, de leur objet, et
		de leur faculté de vous y opposer. Navigation Nous pouvons également recueillir d’autres
		informations, notamment votre adresse IP, le système d’exploitation, le type de votre navigateur, vos
		temps d’accès et les adresses des sites que vous visitez, mais ces renseignements ne sont en aucun
		cas liés à vos informations personnelles.
		Données personnels
		Données personnels recueillis:
		- Adresse email : Donnée servant de login et de contact permettant le bon fonctionnement du
		site
		- Nom et prénom :Donnée recueillis seulement pour les compte “artiste” afin d’identifier
		clairement la personne mettant en ligne du contenu.
		- adresse IP: Donnée recueillis à des fins statistiques.<br><br>
		Toute donnée personnel collectés sera conservée pour une durée de 24 mois suite à l’inactivité
		constatée de l'utilisateur (période de 2 mois sans connexion).
		Ces donnée pourront être consultée par l’équipe d’administration du site.
		En acceptant les conditions générales d’utilisation du site l’utilisateur consent à ce que
		ses donnée soit utilisées à des fins de fonctionnement du site. En aucun cas cs données
		ne pourront être cédées à des fins commerciales.
		Vos donnée personnels sont consultable via votre page profil. Sur cette même page
		vous pouvez définir à quels abonnement (mailing) vous souscrivez.
		En cas d’invocation du droit à l’oubli un formulaire de demande et disponible sur la page
		profil et entraînera la suppression de votre compte et des donnée associées.
		Dashboard<br><br>
		Lors de l’accès au BackOffice, il faut que les personnes habilitées puissent avoir rapidement accès aux
		informations capitales qui sont le nombre de mises en ligne et d’écoutes des musique. Dans le cadre
		de ce projet, il s’agit de tout le tracking concernant les visiteurs et l'activité du site :
		- Le nombre de visiteurs différents aujourd’hui.<br>
		- Le nombre de écoute/likes/commentaire d’un artiste ou d’une musique.<br>
		- Les commentaires signalés en attente de traitement.<br>
		- Le nombre de musique ajoutées.<br>
		- Le nombre d’utilisateurs en ligne.<br><br><br><br><p>
	</article>
</section>



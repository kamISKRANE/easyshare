<?php use Easyshare\Core\Acl;

?>

<!DOCTYPE html>
    <html>
        <head>

            <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css'>
            <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Raleway'>
            <link rel="stylesheet" href="public/css/styleplayermedia.css">
            <link rel="stylesheet" href="public/css/adminStyle.css">
            <script src="public/js/scriptplayermedia.js"></script>

        </head>
        <body id="front" class="frontAdminPageContent">
            
            <br><section id="services" class="row">
                <article class="col-md-offset-2 col-md-8 box5">
                
                <?php
                    //Button pour Modifier la page, Droit Admin Obligatoire
                    //Renvoye directement sur le C4editor avec le content
                    if (isset($updatePage)) {
                        echo $updatePage.'&nbsp';
                    }
                    //Supprime la page
                    if (isset($deletePage)) {
                        echo $deletePage.'</br></br>';
                    }
                    //Affiche le contenu de la page.
                    if (isset($content)) { ?>
                        <div id=""><?php
                            echo $content;  ?>
                        </div><?php
                    }
                ?>
               </article>
            </section>

            <section id="services" class="row">
                <article class="col-md-offset-2 col-md-8 box6">
                    <h2> Commentaires </h2><br>
                    <div id="comments" class="col-md-12">
                    <?php
                        if (Acl::userConnected()) {
                            echo isset($comments)?$comments:"";
                            echo isset($comments_add_field)?$comments_add_field:"";
                        }
                    ?>
                    </div>
                </article>
            </section>
        <br><br><br><br><br><br><br><br><br><br><br>
            <!-- Affiche le player de music avec la playlist déroulante . --> 
            <?php if (isset($medias) and $medias != null) { ?>
                <div class="playerMedia">

                    <div class="player">
                        <div class="playlistIcon">
                            <i class="icons ion-android-list plIcon"></i>
                        </div>
                        <div class="clearfix"></div>
                        <div class="trackDetails ">
                            <span class="artist"></span>
                            <span class="title"></span>
                        </div>

                        <div class="controls">
                            <div class="rew">
                                <i class="icons ion-ios-skipbackward"></i>
                            </div>
                            <div class="play">
                                <i class="icons ion-ios-play"></i>
                            </div>
                            <div class="pause">
                                <i class="icons ion-ios-pause"></i>
                            </div>
                            <div class="fwd">
                                <i class="icons ion-ios-skipforward"></i>
                            </div>
                        </div>
                        <div class="tracker"></div>
                    </div>


                    <ul class="playlist" id="playlist">
                        <?php foreach ($medias as $key => $media) {
                        $labelMedia = str_replace('&quot;', '', $media); ?>
                                <li label="<?php echo $labelMedia ?>" audioURL="/public/music/<?php echo $labelMedia ?>"><?php echo $labelMedia ?></li>
                        <?php
                    } ?>

                </div>
            <?php } ?>
        </body>

            <!--Permet de recupérer le slug de la page dans le fichier scriptPlayerMedia.js -->
            <span id="slugPage" name="<?php echo (isset($slug))? $slug :'' ; ?>" style="display: none"></span>

        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js'></script>


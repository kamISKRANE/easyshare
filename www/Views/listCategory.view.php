<?php
namespace Easyshare\Views;

use Easyshare\Core\Routing;

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

?>
<!DOCTYPE html>
<html>
  <head>
    <title>Liste des catégories</title>
  </head>

  <body id="listCategory">

    <section class="row">
      <article class="col-md-offset-1 col-md-10">
         <label for="title"><h1>Liste des catégories</label>
         <img src="public/icons/edit.png" class="icon-side"></h1>
      </article>
    </section>

    <section class="box1 row col-md-offset-1 col-md-10">
      <article class="col-md-offset-1 col-md-10"></br>
        <div class="col-md-4">
          <input type="text" id="myInput" onkeyup="searchElement()" placeholder="Tapez le nom de la catégorie.." title="Type in a name" > <br><br>
        </div>
        <table id="myTable">
          <thead>
            <tr>
              <th><center>iD</center></th>
              <th><center>Label</center></th>
              <th><center>Date de créationnnnn</center></th>
              <th><center>Action</center></th>

            </tr>
          </thead>

          <tbody>
            <?php foreach ($categories as $category) : ?>

              <tr>
                <td><center><?php echo $category["id"]; ?></center> </td>
                <td><center><div title="Cliquez sur le label pour le modifier"><b>
                  <span href="#"  id="<?php echo $category['id']; ?>" label="<?php echo $category['label'];?>" onClick="confirmForChangeCategoryLabel(this)"> <?php echo $category["label"]; ?></span>
                  </b></div></center></td>

                <td><center><?php echo date("d-m-Y", strtotime($category["create_date"])); ?> </center></td>
                <td><center><img src="../public/images/poubelle.png" id="<?php echo $category['id']; ?>" label="<?php echo $category['label'];?>" onClick="confirmForDeleteCategory(this)"></td> 

              </tr>

            <?php endforeach; ?>
          </tbody>

        </table>
        </br></br><a href="<?php echo $pathForCreate; ?>"><input type="button" class="form-submit" value="Ajouter une categorie"></a>

      </article>
    </section>

    <script>

      var pathForDelete = "<?php echo $pathForDelete ; ?>";
      var pathForUpdate = "<?php echo $pathForUpdate ; ?>";
      

      function getAttributeObj(obj, attribute)
      {
        return obj.getAttribute(attribute);
      }

      function confirmForChangeCategoryLabel(category){
        var responseLastname = confirm('Voulez-vous vraiment le nom de la la catégorie '+getAttributeObj(category,'label')+' ?');
        if(responseLastname){
          var newLabel = prompt('veuillez indiquez le nouveaux nom de cette catégorie').trim();
          if(newLabel.length != 0 && newLabel != null){
            window.location.href = pathForUpdate+'?id='+getAttributeObj(category,'id')+'&label='+newLabel;
          }
        }
      }

      function confirmForDeleteCategory(category){
        var responseForDelete = confirm('Voulez-vous vraiment supprimer la catégorie : '+getAttributeObj(category,'label')+' ?');
        if(responseForDelete){
          window.location.href = pathForDelete+'?labelCategory='+getAttributeObj(category,'id');  
        }
      }

      function searchElement() {
        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
          td = tr[i].getElementsByTagName("td")[1];
          if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
              tr[i].style.display = "";
            } else {
              tr[i].style.display = "none";
            }
          }       
        }
      }

    </script>

  </body>
</html>


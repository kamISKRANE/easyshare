<?php
namespace Easyshare\Views;

use Easyshare\Core\Routing;
use Easyshare\Core\Form;

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Liste des musiques</title>
	</head>

	<body id="listmedia">

		<section class="row">
		  <article class="col-md-offset-1 col-md-10">
		  	 <?php Form::showFormInformationsAndErrors(); ?>
		     <label for="title"><h1>Liste des musiques</label>
		     <img src="public/icons/edit.png" class="icon-side"></h1>
		  </article>
		</section>

		<section class="box1 row col-md-offset-1 col-md-10">
		  <article class="col-md-offset-1 col-md-10"></br>
		  	<div class="col-md-4">
     			<input type="text" id="myInput" onkeyup="myFunction()" placeholder="Tapez un titre de musique.." title="Tapez un titre de musique" > <br><br>
   			</div>
		    
		   <table id="myTable">

		      <thead>
		        <tr>
		        	<th>&nbsp ID</th>
		          	<th>&nbsp Label</th>
		          	<th>&nbsp Categorie</th>
		          	<th>&nbsp Date d'ajout</th>
		          	<th>&nbsp Supprimer</th>
		        </tr>
		      </thead>

		      <tbody>
		        <?php foreach ($medias as $media) : ?>

		          <tr>
		          	<td>&nbsp<?php echo $media["id"] ; ?></td>
		            <td>&nbsp<?php echo $media["label"] ; ?></td>
		            <td>&nbsp<?php echo $media["categ_media"] ; ?></td>
		            <td><center>&nbsp<?php echo date("d-m-Y", strtotime($media["create_date"])) ?></center></td>
		         	<td><center>&nbsp<img src="/public/images/poubelle.png" label="<?php echo $media["label"] ; ?>" onClick="confirmForDeleteMedia(this)"></td></center>
		          </tr>
		        <?php endforeach; ?>
		      </tbody>

		    </table>

		    </br></br><a href="/ajouter_musique"><input type="submit" class="form-submit" value="Ajouter un media" ></input></a>

		  </article>
		</section>

		<script type="text/javascript">

			var pathForDelete = "<?php echo $pathForDelete ;?>";
			
			function getAttributeObj(obj, attribute){
		        return obj.getAttribute(attribute);
	      	}

			function confirmForDeleteMedia(media){

		        var responseForDelete = confirm('Voulez-vous vraiment supprimer la musique : '+getAttributeObj(media,'label')+' ?');

		        if(responseForDelete){
		         	window.location.href = pathForDelete+'?media='+getAttributeObj(media,'label');  
		        }
      		}

		</script>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>


<?php
    namespace Easyshare\Views;

use Easyshare\Core\Routing;
    use Easyshare\Core\Form;

    ?>
<!DOCTYPE html>
<html>
  <head>
    <title>Ajouter une musique</title>
  </head>

  <section class="row">
    <article class="col-md-offset-1 col-md-10">
      </br><?php Form::showFormInformationsAndErrors(); ?>
      <label for="title"><h1>Ajouter une page</label>
        <img src="public/icons/edit.png" class="icon-side">
      </h1>
    </article>
  </section>
        
    <section class="row">
      <article class="col-md-offset-1 col-md-10">
        <?php Form::showFormInformationsAndErrors(); ?>
            <h2 class="form-title">Personalisation du site web</h2></br>
      </article>
    </section>

    <section >
      <article class="box1 col-md-offset-1 col-md-10">
            <?php
                $this->addModal("form", $configStyleWebsite);
            ?>
            <br><a href="<?php echo Routing::getSlug('Template', 'restoreDefaultConfig') ; ?>"><input type="button" name="restoreDefaultConfig" value="reinitialiser les paramètres" class="form-submit"></a>
            <div class="text-center"> 
                
            </div>
        </div>
      </article>
    </section>
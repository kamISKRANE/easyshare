<?php
  
  namespace Easyshare\Views\modals;

$data = ($config['config']['method'] == "POST")? $_POST: $_GET;
?>


<?php if (!empty($config['errors'])):?>
  <div class="alert alert-danger">
  <?php foreach ($config['errors'] as $error):?>
    <li><?php echo $error;?></li>
  <?php endforeach;?>
  </div>
<?php endif;?>




<form method="<?php echo $config['config']['method'];?>"
      action="<?php echo $config['config']['action'];?>" 
      class="<?php echo $config['config']['class'];?>" 
      id="<?php echo $config['config']['id'];?>"
      <?php if (isset($config['config']['onsubmit'])) { ?> 
      onsubmit="<?php echo $config['config']['onsubmit']; ?>" 
      <?php } if (isset($config['config']['enctype'])) { ?>
      enctype="<?php echo $config['config']['enctype'] ; ?>"
      <?php } ?>

>



      <?php foreach ($config['data'] as $key => $value):?>
        
        <div class="form-group">
          <div class="form-label-group">

            <?php
                if ($value["type"]=="password") {
                    unset($data[$key]);
                }
                //Permet de mettre un label juste avant notre élément.
                if (isset($value['message'])) {
                    echo $value['message'] ;
                }

            switch ($value['tag']) {
              case 'input':

                ?>

                  <input type="<?php echo $value["type"];?>" 
                          name="<?php echo $key;?>"
                          placeholder="<?php echo $value["placeholder"];?>"
                          class="<?php echo $value["class"];?>"
                          id="<?php echo $value["id"];?>"
                          <?php echo ($value["required"])?'required="required"':'';?>
                          value="<?php echo $value["value"]?? $data[$key]?? ''; ?>"
                          <?php echo ($value['type'] == 'checkbox' && isset($value["value"]) && $value["value"] == 1)? 'checked': '' ;?>
                  >
               
              <?php
                break;

                case 'textarea':
              ?>
                  <textarea type="<?php echo $value["type"];?>" 
                          name="<?php echo $key;?>"
                          placeholder="<?php echo $value["placeholder"];?>"
                          class="<?php echo $value["class"];?>"
                          id="<?php echo $value["id"];?>"
                          <?php echo ($value["required"])?'required="required"':'';?>
                  ><?php echo $value["value"]?? $data[$key]?? ''; ?></textarea>
              <?php
                break;

                case 'select': ?>

                <select name="<?php echo $key ;?>"> 
                  <option value=""></option>
                  <?php
                    if ($value['value'] != "") {
                        $arrayCategory = explode(";", $value['value']);
                        foreach ($arrayCategory as $key => $category) {
                            echo "<option value='".$category."'>".$category."</option>";
                        }
                    }
                  ?>
                </select>
              <?php
                break;
                
              default:
                break;
            }

            ?>
            <br/><br/>
          </div>
        </div>
      <?php endforeach;?>

<div class="form-group form-button">
<input type="submit" class="form-submit" value="<?php echo $config['config']['submit'];?>">

<?php if (!empty($config['config']['reset'])):?>
  <input type="reset" class="form-submit" value="<?php echo $config['config']['reset'];?>">
<?php endif;?>
</div>
</form>
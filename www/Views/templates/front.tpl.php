<?php

use Easyshare\Core\Acl;

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <title><?php echo $this->title ; ?></title>
    <meta name="description" content="<?php echo $this->description; ?>">
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <meta name="author" content="Admin"/>
    <link rel="stylesheet" href="public/css/grid.css"/>
    <link rel="stylesheet" href="public/fonts/material-icon/css/material-design-iconic-font.min.css"/>
    <link rel="stylesheet" href="public/css/style.css"/>
    <script src="public/js/jquery.min.js"></script>
    <script src="public/js/jquery-ui.min.js"></script>
    <script src="public/js/jquery.modal.min.js"></script>
    <link rel="stylesheet" href="public/css/jquery.modal.min.css" />
    <script src="public/js/script.js"></script>


</head>
<body id="front">
    <header class="container-fluid header">
        <section class="row">
            <article class=" col-md-offset-2 col-md-3">
                <div class="col-md-2 col-xs-1 col-sm-1 ">
                    <a href="/" id="logo-image"></a>
                </div>
            </article>

            <article class="col-md-offset-1 col-md-4">
                <nav>
                    <ul class="dropdown">
                        <?php if (Acl::userConnected()) { 
                            echo $defaultMenu;
                        } else { 
                            echo '<li><a href="/creation_de_compte">S\'inscrire</a>
                            <li><a href="/connexion">Se connecter</a>';
                        } ?>
                    </ul>
                </nav>
            </article>
        </section>
    </header><br/><br/>

    <main>
      <?php include $this->v;?>

    </main>

    <footer class="footer">
        <section class="row">
            <article class=" clearfix col-md-offset-2 col-md-6">
                <nav>
                    <ul>
                        <li><a href="/mentions">Mentions légales</a></li>
                        <li><a href="mailto:easyshare.esgi1@gmail.com">Nous contacter</a></li>
                        <li><a href="/nous">À propos de nous</a></li>
                        <li><a><span id="hide">Easyshare</span></a></li>
                    </ul>
                </nav>

            </article>
        </section>
    </footer>

    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="js/main.js"></script>
</body>
</html>

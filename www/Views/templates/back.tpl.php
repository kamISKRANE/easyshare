<?php
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <title>EASY-SHARE Administration</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="AdminPage">
    <meta name="author" content="Admin">
    <link rel="stylesheet" href="public/css/grid.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="public/css/ez-icons.css">
    <script src="public/js/Chart.min.js"></script>
    <script src="public/js/jquery.min.js"></script>
    <script src="public/js/jquery-ui.min.js"></script>
    <script src="public/js/jquery.modal.min.js"></script>
    <link rel="stylesheet" href="public/css/jquery.modal.min.css" />
    <script type="application/javascript" src="public/js/script.js"></script>
    <title><?php echo $this->title ; ?></title>
  </head>

   <body id="back">
      <header class="container-fluid header" role="banner">
         <section class="row">
            <a href="/" class="row logo-back col-md-2 col-xs-1 col-sm-1 "  >
            <img src="public/images/logo.png" alt="Avatar">
            </a>
         </section>
      </header>
      <nav id="navbar" class="container-fluid">
         
         
            <div class="nav row">
            <ul class="navUl">
               <li><a href="/" title="Accueil"><i class="icon-home"></i><span class="mobile-hide">&nbsp;&nbsp;&nbsp;Accueil</span></a>
               </li>
              <div class="dropdown">
               <li><a href="/liste_des_utilisateurs" title="liste des utilisateurs" class="dropbtn">
                  <i class="icon-list"></i>
                  <span class="mobile-hide">&nbsp;&nbsp;&nbsp;Mes utilisateurs</span>
                </a>
               </li>
              </div>
              <div class="dropdown">
               <li><a href="/liste_des_pages" title="liste des pages"><i class="icon-pencil">
                </i><span class="mobile-hide">&nbsp;&nbsp;&nbsp;Mes pages</span></a>
                  <div class="dropdown-content">
                    <a href="/liste_des_pages"><input type="submit" class="dropdownbtn" value="Voir mes pages" ></input></a>
                    <a href="/creation_de_page"><input type="submit" class="dropdownbtn" value="Ajouter une page" ></input></a>
                  </div>
               </li>
             </div>
              <div class="dropdown">
               <li><a href="/liste_des_musiques" title="liste des musiques"><i class="icon-headphones"></i>
                  <span class="mobile-hide">&nbsp;&nbsp;&nbsp;Mes Musiques</span></a>
                  <div class="dropdown-content">
                    <div class="submenu">
                    <a href="/liste_des_musiques"><input type="submit" class="dropdownbtn" value="Voir mes musiques" ></input></a>
                    <a href="/ajouter_musique"><input type="submit" class="dropdownbtn" value="Ajouter une musique" ></input></a>
                  </div>
                  </div>
               </li>
              </div>

              <div class="dropdown">
               <li>
                <a href="afficher_les_categories" title="afficher les categories"><i class="icon-folder"></i>
                  <span class="mobile-hide">&nbsp;&nbsp;&nbsp;Mes Catégories</span></a>
                  <div class="dropdown-content">
                    <div class="submenu">
                    <a href="/afficher_les_categories"><input type="submit" class="dropdownbtn" value="Voir mes catégories" ></input></a>
                    <a href="/ajouter_une_categorie"><input type="submit" class="dropdownbtn" value="Ajouter une catégorie" ></input></a>
                  </div>
                  </div>
               </li>
             </div>

            <li>
              <a href="/profil" title="profil"><i class="icon-user"></i><span class="mobile-hide">&nbsp;&nbsp;&nbsp;Mon compte</span></a>
            </li>

            <li>
              <a href="/menu" title="Menus" ><i class="icon-menu"></i><span class="mobile-hide">&nbsp;&nbsp;&nbsp;Mes menus</span></a>
            </li>

            <li>
              <a href="/stat" title="Statistiques" ><i class="icon-pie-chart"></i><span class="mobile-hide">&nbsp;&nbsp;&nbsp;Mes statistiques</span></a>
            </li>
          </ul>
          <ul class="navUl">
               <li><a href="/personalisation_du_site" title="Paramètres"><i class="icon-wrench"></i><span class="mobile-hide">&nbsp;&nbsp;&nbsp;Paramètres</span></a>
               </li>
               <li><a href="/aide" title="Paramètres"><i class="icon-question"></i><span class="mobile-hide">&nbsp;&nbsp;&nbsp;Centre d'aide</span></a>
               </li>
               <li><a href="/deconnexion" title="Paramètres"><i class="icon-exit"></i><span class="mobile-hide">&nbsp;&nbsp;&nbsp;Me déconnecter</span></a></li>
            </ul>
         </div>
      </nav>
      <main class="container">
        <?php include $this->v;?>
      </main>
   </body>

</html>
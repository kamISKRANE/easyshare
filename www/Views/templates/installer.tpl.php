<?php
    namespace Easyshare\Views;

use Easyshare\Core\Form;
    use Easyshare\Core\Acl;

    ?>

    <link rel="stylesheet" href="/public/css/style.css">
       <body id="login">

        <section class="sign-in">
            <div class="container">

                <section class="signup">

                <!-- affiche les informations global :'Vous êtes bien inscrit'.-->
                <?php
                Acl::showMessageErrorAcl();
                Form::showFormInformationsAndErrors(); ?>

                <div class="signup-content">
                    <div class="alert alert-danger">
                    </div>
        
                                <?php include $this->v;?>

                        
                        
                    </section>
                </div>
            </section>

         </body>
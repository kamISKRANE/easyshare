<?php
namespace Easyshare\Views;

use Easyshare\Core\Routing;

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

?>
<!DOCTYPE html>
<html>
  <head>
    <title>Liste des utilisateurs</title>
  </head>

  <body id="listUser">

    <section class="row">
      <article class="col-md-offset-1 col-md-10">
         <label for="title"><h1>Liste des utilisateurs</label>
         <img src="public/icons/edit.png" class="icon-side"></h1>
      </article>
    </section>

    <section class="box1 row col-md-offset-1 col-md-10">
      <article class="col-md-offset-1 col-md-10"></br>
        <div class="col-md-4">
      <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Tapez le nom de l'utilisateur.." title="Type in a name" > <br><br>
    </div>
        <table id="myTable">
          
          <thead>
            <tr>
              <th>iD</th>
              <th>Rôle</th>
              <th>Nom de famille</th>
              <th>Prénom</th>
              <th>E-mail</th>
              <th>Date de naissance</th>
              <th>Action</th>
              <th>Modifier les droits</th>
            </tr>
          </thead>

          <tbody>
            <?php foreach ($users as $user) : ?>
              <tr>
                <td><?php echo($user["id"]); ?></td>
                <td><?php echo($user["role_id"] == 1)? 'Administrateur':'Utilisateur'; ?></td>
                <td><div title="Cliquez sur le nom pour le modifier"><b><span href="#" class="user" userid="<?php echo $user['id'];?>" firstname="<?php echo $user["firstname"]?>" lastname="<?php echo($user["lastname"]); ?>" onClick="confirmForChangeUserLastname(this)"> <?php echo($user["lastname"]); ?>
                </span></b>
                </div></td>

                
                <td><div title="Cliquez sur le prénom pour le modifier"><b><span href="#" class="user" userid="<?php echo $user['id'];?>" firstname="<?php echo $user["firstname"]?>" lastname="<?php echo($user["lastname"]); ?>" onClick="confirmForChangeUserFirstname(this)"> <?php echo($user["firstname"]); ?>
                </span> </b>
                </div></td>

                <td><?php echo($user["email"]); ?></td>
                <td><?php echo date("d-m-Y", strtotime($user["birth_date"])) ?>
                </td>

                <td><?php if ($user["email"] != $_SESSION["user"]["email"]) {  ?>
                  <img src="../public/images/poubelle.png" href="#" class="user" userid="<?php echo $user['id'];?>" firstname="<?php echo $user["firstname"]?>" lastname="<?php echo($user["lastname"]); ?>" onClick="confirmForDeleteUser(this)">
                <?php } else {
    echo ' /--/ ';
}  ?>
                </td> 

                <td><?php if ($user["email"] != $_SESSION["user"]["email"]) {  ?>
                    <b><a href="#" class="user" userid="<?php echo $user['id'];?>" role="<?php echo $user['role_id'];?>" firstname="<?php echo $user["firstname"]?>" lastname="<?php echo($user["lastname"]); ?>" onClick="confirmForChangeUserRole(this)" > 
                <?php echo($user["role_id"] == 1)?'<img src="../public/images/downgrade.png">':'<img src="../public/images/upgrade.png">'?>
                </a></b>
                <?php } else {
    echo ' /--/ ';
}  ?></td> 

              </tr>
            <?php endforeach; ?>
          </tbody>

        </table>

      </article>
    </section>

    <script type="text/javascript">
      var pathForChangeName     = "<?php echo $pathForChangeName ;?>";
      var pathForDelete         = "<?php echo $pathForDelete ;?>";
      var pathForUpgradeRole    = "<?php echo $pathForUpgradeRole ;?>";
      var pathForDowngradeRole  = "<?php echo $pathForDowngradeRole ;?>"; 

      function getAttributeObj(obj, attribute){
        return obj.getAttribute(attribute);
      }

      function confirmForChangeUserLastname(user){
        var responseLastname = confirm('Voulez-vous vraiment modifier le nom de famille de '+getAttributeObj(user,'lastname')+' '+getAttributeObj(user,'firstname')+' ?');
        if(responseLastname){
          var newLastname = prompt('veuillez indiquez le nouveaux nom de famille').trim();
          if(newLastname.length != 0 && newLastname != null){
            window.location.href = pathForChangeName+'?id='+getAttributeObj(user,'userid')+'&lastname='+newLastname;
          }
        }
      }

      function confirmForChangeUserFirstname(user){
        var responseLastname = confirm('Voulez-vous vraiment modifier le prénom de '+getAttributeObj(user,'lastname')+' '+getAttributeObj(user,'firstname')+' ?');
        if(responseLastname){
          var newFirstname = prompt('veuillez indiquez le nouveaux prénom').trim();
          if(newFirstname.length != 0 && newFirstname != null){
            window.location.href = pathForChangeName+'?id='+getAttributeObj(user,'userid')+'&firstname='+newFirstname;
          }

        }
      }

      function confirmForDeleteUser(user){
        var responseForDelete = confirm('Voulez-vous vraiment supprimer '+getAttributeObj(user,'lastname')+' '+getAttributeObj(user,'firstname')+' ?');

        if(responseForDelete){
          window.location.href = pathForDelete+'?id='+getAttributeObj(user,'userid');  
        }
      }

      function confirmForChangeUserRole(user){

        if(getAttributeObj(user,'role') == 1){
          var newRole = 'Utilisateur';
        }else{
          var newRole = 'Administrateur';
        }

        var responseForRole = confirm('Voulez-vous vraiment faire passer '+getAttributeObj(user,'lastname')+' '+getAttributeObj(user,'firstname')+' en status: \n \" '+newRole+' \" sur votre site ?');

        if(responseForRole && newRole == 'Utilisateur'){
          window.location.href = pathForDowngradeRole+'?id='+getAttributeObj(user,'userid');
        }else if(responseForRole && newRole == 'Administrateur'){
          window.location.href = pathForUpgradeRole+'?id='+getAttributeObj(user,'userid');
        }
      }

    </script>





<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[2];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>








  </body>
</html>


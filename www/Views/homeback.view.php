         <div class="row">
            <article class=" col-offset-2 col-md-12 container">
            <header class="row">
               <h1 class="col-md-8>">Bonsoir, <?= $_SESSION["user"]["firstname"];?><br/> <div class="subtitle"><p>Voici ce qu'il se passe sur votre site aujourd'hui.</p></div>
               </h1>
            </header>
      <section class="row">
               <article class="box col-md-3">
                  <div class="dashboard-card stat">
                     <div class="dashboard-card-content">
                        <h3><?php echo($nbUser["nbUser"]);?></h3>
                        <p>inscrit(s) sur votre site</p>
                     </div>
                  </div>
               </article>                                                          
                <article class="box col-md-3">
                  <div class="dashboard-card stat">
                     <div class="dashboard-card-content">
                        <h3><?php echo($nbMusic["nbMusic"]);?></h3>
                        <p>musiques ajoutées</p>
                     </div>
                  </div>
               </article>
               <article class="box col-md-3">
                  <div class="dashboard-card stat">
                     <div class="dashboard-card-content">
                        <h3><?php echo($nbPageView["nbPageView"]==null)?0:($nbPageView["nbPageView"]);?></h3>
                        <p>vues sur vos pages</p>
                     </div>
                  </div>
               </article>
               <article class="box col-md-3">
                  <div class="dashboard-card stat">
                     <div class="dashboard-card-content">
                        <h3><?php echo($nbComment["nbComment"]);?></h3>
                        <p>commentaire(s) posté(s) au total</p>
                     </div>
                  </div>
               </article>
      </section>
      <section class="row">
                    <article class="box col-md-12 ">
                  <div class="rectangle" >
                     <div class="notification-text" >
                        <span><?php echo($nbCommentToday["nbComment"]) ?> &nbsp;&nbsp;commentaire(s) publié(s) aujourd'hui</span>
            <img src="public/icons/inbox.png" class="icon-side">
                     </div>
                  </div>
                </article>
      </section>
      <section class="row">

                <article class="box col-md-10">
                    <div class="table-card stat">
                        <h1 class="col-md-8>">Vos 5 musiques les plus écoutées</h1>
                     <div class="table-card-content">
                       <canvas id="bar-chart"></canvas>
                     </div>
                  </div>
                </article>
                <article class="box col-md-2">
                  <div class="side-card stat">
                     <div class="side-card-content">
                  <div class="side">
                     <div class="side-text" >
                       <span>Mes musiques</span>
                       <a href="/liste_des_musiques"><img src="public/icons/headset.png" class="icon-side"></a>
                     </div>
                  </div>
                  <div class="side">
                      <div class="side-text" >
                        <span>Mes articles</span>
                        <a href="/liste_des_pages"><img src="public/icons/edit.png" class="icon-side"></a>
                     </div>
                  </div>
                  <div class="side" >
                     <div class="side-text" >
                        <span>Mes statistiques</span>
                        <a href="/stat"><img src="public/icons/more.png" class="icon-side"></a>
                     </div>
                  </div>
                  <div class="side" >
                     <div class="side-text" >
                        <span>Mes utilisateurs</span>
                        <a href="/liste_des_utilisateurs">
                        <img src="public/icons/user.png" class="icon-side"></a>
                     </div>
                  </div>
                     </div>
                  </div>
                </article>
            </section>
      </main>
    <script>src="js/index.js"</script>

<script>

$arr=<?php echo json_encode($StatMusic) ?>;
let result = $arr;
let labels = result.map(e => e.titleMusic);
let data = result.map(e => e.countView);

new Chart(document.getElementById("bar-chart"), 
{
    type: 'bar',
    data: 
    {
      labels: labels,
      datasets: 
      [
        {
          backgroundColor: ["#25275a", "#25275a","#4346a3","#5c5fbc","#8183cb"],
          data: data
        }
      ]
    },

    options: {
        scales: {
            yAxes: 
                  [
                    {
                    ticks: {
                            suggestedMin: 0,
                            suggestedMax: 100
                           }
                    }
                  ]
                }
              }
}
);
</script> 


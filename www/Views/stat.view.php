
         <div class="row">
            <article class=" col-offset-2 col-md-12 container">
            <header class="row">
               <h1 class="col-md-8>">Vos statistiques<br>
               </h1>
            </header>

    <section class="boxa row col-md-12">
      <article class="col-md-12"></br>
        <p> Attention : Aucune statistique ne vous sera retournée si aucune page, musique ou catégorie n'est ajoutée.
          </p>
      </article>
    </section>
    <br>


    <section class="box1 row col-md-12">
        <!-- Pie Chart -->
        <article class="col-md-4"></br>
            <canvas id="pie-chart"></canvas>
        </article>

            <!-- Donut Chart -->
        <article class="col-md-4"></br>
            <canvas id="pie-chart3"></canvas>
        </article>
    
        <!-- Polar Chart -->
        <article class="col-md-4"></br>
            <canvas id="pie-chart2"></canvas>
        </article>
    </section><br><br>    



<!-- Statistiques sur les musiques -->
<script>
$arr=<?php echo json_encode($StatMusic) ?>;
let music = $arr;
let titleMusic = music.map(e => e.titleMusic);
let viewsMusic = music.map(e => e.countView);

    new Chart(document.getElementById("pie-chart"), {
    type: 'pie',
    data: 
    {
      labels: titleMusic,
      datasets: 
      [
        {
          backgroundColor: ["#25275a", "#25275a","#4346a3","#5c5fbc","#8183cb"],
          data: viewsMusic
        }
      ]
    },

    options: {
        title: {
        display: true,
        text: 'Les 5 musiques les plus écoutées'
               }
             }
});

</script>

<!-- Statistiques sur les catégories -->
<script>
$arr=<?php echo json_encode($StatCategory) ?>;
let cat = $arr;
let categorie = cat.map(e => e.category);
let nbcategorie = cat.map(e => e.count_category);  
new Chart(document.getElementById("pie-chart2"), {
    type: 'pie',
    data: 
    {
      labels: categorie,
      datasets: 
      [
        {
            backgroundColor: ["#25275a", "#25275a","#4346a3","#5c5fbc","#8183cb"],
            data: nbcategorie
        }
      ]
    },
    options: {
        title: {
          display: true,
          text: 'Les 3 catégories les plus présentes'
               }
             }
});
</script>

<!-- Statistiques sur les pages -->
<script>
$arr=<?php echo json_encode($StatPage) ?>;
let page = $arr;
let titrePage = page.map(e => e.titlePage);
let viewsPage = page.map(e => e.nbViewsPage);  
new Chart(document.getElementById("pie-chart3"), {
    type: 'pie',
    data: 
    {
      labels: titrePage,
      datasets: 
      [
        {
            backgroundColor: ["#25275a", "#25275a","#4346a3","#5c5fbc","#8183cb"],
            data: viewsPage
        }
      ]
    },
    options: {
        title: {
          display: true,
          text: 'Les 5 catégories les plus présentes'
               }
             }
});
</script>

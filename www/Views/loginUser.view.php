<?php

    namespace Easyshare\Views;

use Easyshare\Core\Routing;
    use Easyshare\Core\Form;
    use Easyshare\Core\Acl;

    ?>

                    <?php
                        // Affiche : 'Vous n'avez pas les droits suffisants, veuillez vous connecter avec un compte administrateur' => si l'action necessite un droit admin et que l'user ne le possède pas.
                        Acl::showMessageErrorAcl();
                       
                    ?>
                        <div class="signup-form">

                            <h2 class="form-title">Se connecter</h2>
                            <?php
                                $this->addModal("form", $configFormRegister);
                            ?>
                            <br>

                          
                            <div class="text-center">
                                <a class="signup-image-link" href="<?php echo Routing::getSlug("User", "add") ?>">S'inscrire</a>
                                <a class="signup-image-link" href="<?php echo Routing::getSlug("User", "forgetPassword") ?>">Mot de passe oublié ?</a><br/> <hr style="height: 1px; color: #F0F0F0; background-color: #F0F0F0; width: 100%; border: none;"><br/><br/>
                                <a class="fb" href="loginFacebook"> <img src="../public/images/facebook.png" alt="sign up image">&ensp;Se connecter avec Facebook</a>
                                <a class="google" href="/googleLogin"> <img src="../public/images/google.png" alt="sign up image">&ensp;Se connecter avec Google</a>
                            </div>
                        </div>
                        <div class="signup-image">
                            
                            <figure><img src="../public/images/connexion.jpg" alt="sign up image"></figure>
                        </div>
                    </div>


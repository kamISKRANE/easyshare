<?php
namespace Easyshare\Views;
use Easyshare\Core\Routing;
if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }

?>
<!DOCTYPE html>
<html>
  <head>
    <title>Liste des utilisateurs</title>
  </head>

  <body id="listUser">

    <section class="row">
      <article class="col-md-offset-1 col-md-10">
         <label for="title"><h1>Centre d'aide</label>
         <img src="public/icons/edit.png" class="icon-side"></h1>
      </article>
    </section>
    <section class="boxa row col-md-offset-1 col-md-10">
      <article class="col-md-offset-1 col-md-10"></br>
        <h1>Sommaire</h1>
          <p>Afin d'améliorer votre expérience sur EasyShare, retrouvez les réponses à vos questions sur cette page.</p>
          <a href="#administration"><input type="submit" class="dropdownbtn" value="Administration" ></input></a>
          <a href="#utilisateurs"><input type="submit" class="dropdownbtn" value="utilisateurs" ></input></a>
          <a href="#media"><input type="submit" class="dropdownbtn" value="Médias" ></input></a>
          <a href="#page"><input type="submit" class="dropdownbtn" value="Pages" ></input></a>
          <a href="#statistiques"><input type="submit" class="dropdownbtn" value="Statistiques" ></input></a>
          <a href="#theme"><input type="submit" class="dropdownbtn" value="Thème" ></input></a>
      </article>
    </section><br/><br/>

    <section class="boxa row col-md-offset-1 col-md-10">
      <article class="col-md-offset-1 col-md-10"></br>
        <div id="administration">
        <h1>Administration</h1>
            <p>EasyShare dispose de deux types de comptes avec différentes options d'autorisation : les administrateurs et les comptes utilisateurs. 
            Par défaut, l’administrateur est la personne dont l'adresse e-mail et les coordonnées ont été utilisées initialement pour s'inscrire à EasyShare. Seuls les administrateurs peuvent :<br/><br/>
            • Gérer les médias<br/><br/>
            • Gérer les pages<br/><br/>
            • Gérer les utilisateurs<br/><br/>
            • Gérer les menus<br/><br/>
            • Gérer les commentaires<br/><br/>
            • Accéder aux statistiques<br/><br/>
            • Modifier le style du site
            </p>
      </article>
      </div>
    </section><br/><br/>


    <section class="boxa row col-md-offset-1 col-md-10">
      <article class="col-md-offset-1 col-md-10"></br>
        <div id="utilisateurs">
        <h1>Gestion des utilisateurs</h1>
          <p>Sur la page dédiée à vos <a href="/liste_des_utilisateurs"><b>utilisateurs</b></a>, retrouvez la liste de tous les utilisateurs inscris sur votre site. <br/><br/>
            Une barre de recherche située au dessus de la liste est à votre disposition pour chercher un utilisateur par son nom de famille. De plus, vous avez la possibilité d'éditer le nom et prénom d'un utilisateur, pour cela il vous suffit de cliquer sur l'un des deux élèments, une fenêtre s'ouvrira vous donnant l'accès à la modification.
          </p>
        </div>
      </article>
    </section><br/><br/>
      
    <section class="boxa row col-md-offset-1 col-md-10">
      <article class="col-md-offset-1 col-md-10"></br>
        <div id="media">
        <h1>Gestion des médias</h1>
          <p>Sur la page dédiée aux <a href="/liste_des_musiques"><b>médias</b></a>, retrouvez la liste de tous vos médias ajoutés. Pour ajouter un média, il vous suffit de cliquer sur le bouton « Ajouter un média ». <br/><br/>
          Ajouter un média se fait en 3 étapes : <br/><br/>
          1)  Donnez un titre à votre média<br/><br/>
          2)  Choisissez une catégorie parmi celles que vous avez ajoutés<br/><br/>
          3)  Ajouter votre fichier au format <b>.mp3</b><br/><br/>
          Le tour est joué, si vous avez bien rempli tous les champs, votre musique est maintenant enregistrée.</p>
        </div>
      </article>
    </section><br/><br/>

    

    <section class="boxa row col-md-offset-1 col-md-10">
      <article class="col-md-offset-1 col-md-10"></br>
        <div id="page">
        <h1>Gestion des pages</h1>
          <p>Sur la page dédiée à vos pages, retrouvez la liste de toutes celles que vous avez ajoutées. Pour <a href="/creation_de_page"><b>Ajouter une page</b></a>, il vous suffit de cliquer sur le bouton « Ajouter une page ». <br/><br/>
          Ajouter un média se fait en 5 étapes : <br/><br/>
          1)  Saisissez le lien de votre page, le format sera https://site.fr/URL<br/><br/>
          2)  Cochez les cases si vous voulez rendre la page visible, activez les commentaires ou que celle-ci soit votre page d’accueil.<br/><br/>
          3)  Donnez un titre à votre page<br/><br/>
          4)  Remplir le contenu de la page<br/><br/>
          5)  Entrez une description, voici la structure pour ajouter une musique : <b>[["VOTREMUSIQUE.mp3"]]</b><br/>
              Attention, une seule balise est possible, pour ajouter plusieurs musiques, il suffit de les séparer par des virgules.
          <br/> <br/>
          Le tour est joué, votre page a été créée.</p>
        </div>
      </article>
    </section><br/><br/>


    <section class="boxa row col-md-offset-1 col-md-10">
      <article class="col-md-offset-1 col-md-10"></br>
        <div id="statistiques">
        <h1>Analyse de données</h1>
          <p>Vous pouvez utiliser les analyses de données sur la page <a href="/stat"><b>Statistiques</b></a> pour en savoir plus sur votre activité. Vous retrouverez par exemple vos 5 musiques les plus écoutées, les pages les plus consultés ou encore les catégories les plus présentes.</p>
      </article>
        </div>
    </section><br/><br/>

    <section class="boxa row col-md-offset-1 col-md-10">
      <article class="col-md-offset-1 col-md-10"></br>
        <div id="theme">
        <h1>Gestion des thèmes</h1>
          <p>Vous avez sur EasyShare la possibilité de personnaliser votre site, pour cela, rendez vous sur la page <a href="/creation_de_page"><b>Paramètres</b></a>. A partir de celle-ci, vous pourrez modifier la couleur et la taille de vos textes ou encore la police de votre choix.</p>
      </article>
        </div>
    </section>


  </body>
</html>


<?php
namespace Easyshare\Views;

use Easyshare\Core\Routing;

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Liste des Pages</title>
	</head>

	<body id="listpage">

		<section class="row">
		  <article class="col-md-offset-1 col-md-10">
		     <label for="title"><h1>Liste des pages</label>
		     <img src="public/icons/edit.png" class="icon-side"></h1>
		  </article>
		</section>

		<section class="box1 row col-md-offset-1 col-md-10">
		  <article class="col-md-offset-1 col-md-10"></br>
		  	   <div class="col-md-4">
      <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Tapez un titre de page.." title="Tapez un titre de page" > <br><br>
    </div>
		    
		   <table id="myTable">

		      <thead>
		        <tr>
		          <th>Label</th>
		          <th>Titre</th>
		          <th>Visible</th>
		          <th>Commentaires</th>
		          <th>Page d'accueil</th>
		          <th>Description</th>
		          <th>Nombre de vues</th>
		          <th>Date de création</th>
		          <th>Date de modification</th>
		          <th>Modifier</th>
		          <th>Supprimer</th>
		        </tr>
		      </thead>

		      <tbody>
		        <?php foreach ($pages as $page) : ?>

		          <tr>
		            <td><?php echo preg_replace('#/#', '', $page["label"]) ; ?></td>
		            <td><?php echo $page["title"] ; ?></td>
		            <td><?php echo ($page["available"] == 1) ? 'Oui' : 'Non' ; ?></td>
		            <td><?php echo ($page["comment"] == 1) ? 'Oui' : 'Non' ; ?></td>
		         	<td><?php echo ($page["homepage"] == 1) ? 'Oui' : 'Non' ; ?></td>
		         	<td><?php echo $page["description"] ; ?></td>
		         	<td><?php echo ($page["count_views"] != null)? $page["count_views"] : '0';  ?></td>
		         	<td><?php echo date("d-m-Y", strtotime($page["create_date"])) ; ?></td>
		         	<td><?php echo date("d-m-Y", strtotime($page["update_date"])) ; ?></td>
		         	<td><a href="<?php echo $pathForUpdate.'?label='.$page["label"] ; ?>"><img src="/public/images/edition.png"></a></td>
		         	<td><img src="/public/images/poubelle.png" label="<?php echo preg_replace('#/#', '', $page["label"]) ; ?>" onClick="confirmForDeleteUser(this)"></td>
		          </tr>
		        <?php endforeach; ?>
		      </tbody>

		    </table>

		    </br></br><a href="<?php echo $pathForCreate ; ?>"><input type="submit" class="form-submit" value="Ajouter une page" ></input></a>

		  </article>
		</section>

		<script type="text/javascript">

			var pathForDelete = "<?php echo $pathForDelete ;?>";
			
			function getAttributeObj(obj, attribute){
		        return obj.getAttribute(attribute);
	      	}

			function confirmForDeleteUser(page){

		        var responseForDelete = confirm('Voulez-vous vraiment supprimer la page : '+getAttributeObj(page,'label')+' ?');

		        if(responseForDelete){
		          window.location.href = pathForDelete+'?slug=/'+getAttributeObj(page,'label');  
		        }
      		}

		</script>

		<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>


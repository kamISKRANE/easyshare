<div id="comments" class="">
	<?php
        $comments = $this->displayCommentsAction();
        foreach ($comments as $comment) {
            echo '<div id="comment_'.$comment["id"].'"class="comment">
						<div class="comment_details">
							<p>Publié par : '.$comment["user"].'</p>
							<p>Date de publication : '.$comment["create_date"].'</p>
						</div>
						<div class="comment_content">'.$comment["content"].'</div>
						'.(($comment["user_id"] === $_SESSION['user']['id'])?'<a href="/supprimer_commentaire?'.$comment["id"].'">Supprimer</a>':($this->isAdmin()?'<a href="/supprimer_commentaire?'.$comment["id"].'">Supprimer</a>':''))
                        .'</div>';
        }
    
    ?>
</div>
<div id="comment_edit" class="">	
	<div class="comment_container">
		<form action="/ajouter_commentaire" method="POST">
		  <div class="row">
		    <label class="required" for="message">Your message:</label><br />
		    <textarea id="message" class="input" name="message" cols="92" rows="15"></textarea><br />
		  </div>
		  <input id="submit_button" type="submit" value="Poster" />
		</form>
	</div>
</div>

</article>
</section>
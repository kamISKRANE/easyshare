<?php
    namespace Easyshare\Views;

use Easyshare\Core\BaseSQL;

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
    
?>
 
  <section class="row">
   <article class="col-md-offset-1 col-md-10">
      <label for="title"><h1>Gérer mes menus</label>
        <img src="public/icons/edit.png" class="icon-side">
      </h1>
</article></section>
  <section>

    <article class="box1 col-md-offset-1 col-md-10">
    <section class="row">    
        <div class="menu_list_section col-md-6" id="menu_list_section">
            <ul id="menu_list">
                <?= $menuList; ?>
            </ul>
            <a href="#ajout_menu" rel="modal:open" ><input type="submit" class="form-submit" value="Ajouter un menu" ></input></a>
        </div>

        <!-- Liste des liens du menu -->
        <div id="menu_list_items" class="col-md-6">
            
        </div>
    </section>
    <div id="ajout_menu" class="modal">
        <form method="POST" action="/ajouter_menu">
            <h1> Ajouter un menu </h1>
            <input type="text" name="menu_name" placeholder="Titre du menu" required="required"><br><br>
            <input type="submit" class="form-submit" name="menu_add_btn">
        </form>
    </div>

    <!-- Modal Ajout de lien-->
    <div id="ajout_lien" class="modal">
        <h1> Ajouter un lien </h1>
        <input id="link_name_input" type="text" placeholder="Titre du lien" name="menu_name"><br><br>
        <input id="link_href_input" type="text" placeholder="href du lien" name="menu_href"><br><br>
        <input type="submit"  class="form-submit" name="link_add" id="add_link_item_btn" value="Ajouter" onclick="getMenuId()">
    </div>

    <!-- Modal modification titre d'un menu-->
    <div id="edit_menu" class="modal">
        <h1> Modifier le menu </h1>
        <input id="menu_name_edit" type="text" placeholder="Titre du menu" name="menu_name"><br><br><br>
        <a rel="modal:close" class="form-submit" name="update_menu" id="add_link_item_btn" onclick="updateMenu()">Modifier</a>
    </div>

    <!-- Modal modification d'un lien-->
    <div id="edit_link" class="modal">
        <input id="link_name_edit" type="text" placeholder="Titre du lien" name="menu_name">
        <input id="link_href_edit" type="text" placeholder="href du lien" name="menu_href">
        <input type="submit" name="link_add" id="add_link_item_btn" value="Ajouter" onclick="updateLink()">
    </div>

    <!-- Modal suppression d'un lien-->
    <div id="delete_link" class="modal">
        <h1>Voulez vous vraiment supprimer ce lien</h1>
        <a rel="modal:close" class="menu-delete" name="delete_link" id="delete_link_btn" onclick="deleteLinkSubmit()">Supprimer</a>
    </div>

    <!-- Modal suppression d'un menu-->
    <div id="delete_menu" class="modal">
        <p>Voulez vous vraiment supprimer ce menu</p>
        <a rel="modal:close" name="delete_menu" onclick="deleteMenu()">Supprimer</a>
    </div>

    <div class="test" id="add_link_section">
         
    </div>
    </article>

  </section>
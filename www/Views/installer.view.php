<?php

    namespace Easyshare\Views;

use Easyshare\Core\Routing;
    use Easyshare\Core\Form;
    use Easyshare\Core\Acl;

    // Affiche : 'Vous n'avez pas les droits suffisants, veuillez vous connecter avec un compte administrateur' => si l'action necessite un droit admin et que l'user ne le possède pas.
    Acl::showMessageErrorAcl();
       
?>

    <div class="install-form">
        <div id="imgInstaller">
            <img src="../public/images/Easyshare.jpg" width="30%" alt="sign up image">
        </div>
        <h1> BIENVENUE </h1>
                <hr style="height: 1px; color: #F0F0F0; background-color: grey; width: 100%; border: none;">
                <p> Bienvenue sur la page d'installation. Ne vous inquiétez pas, ca ne sera pas long ! </p><br/>
                <h1> INFORMATIONS NECESSAIRES </h1>
                <hr style="height: 1px; color: #F0F0F0; background-color: grey; width: 100%; border: none;">
                <p> Veuillez remplir toutes les informations ci-dessous. </p>
            <br/>
        <?php
            if (is_array($installForm)) {
                $this->addModal("form", $installForm);
            } else {
                echo $installForm;
            }
        ?>
               
    </div>
            
            
    </div>


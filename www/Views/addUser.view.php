<?php
    namespace Easyshare\Views;

use Easyshare\Core\Routing;
    use Easyshare\Core\Form;

    ?>
    
    <div class="signup-form">
        <h2 class="form-title">S'inscrire</h2>

        <?php
            $this->addModal("form", $configFormRegister);
        ?>
        <br><br>
        <div class="text-center"> 
            <a class="fb" href="https://www.facebook.com/v3.3/dialog/oauth?client_id=339208610104171&redirect_uri=https://easyshare.site/loginFacebook&state=433"> S'inscrire avec Facebook</a>
            <a class="google" href="https://accounts.google.com/o/oauth2/v2/auth?scope=https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email&redirect_uri=https://easyshare.site/loginGoogle&response_type=code&client_id=391186458705-gujh2sbe7nsltb7pklqp9qf35hcq6834.apps.googleusercontent.com"> S'inscrire avec Google</a><br><br>
            <a class="signup-image-link" href="<?php echo Routing::getSlug("User", "login") ?>">Se connecter</a>
            <a class="signup-image-link" href="<?php echo Routing::getSlug("User", "forgetPassword") ?>">Mot de passe oublié</a>
        </div>
    </div>
    <div class="signup-image">
        <figure><img src="../images/inscription.jpg" alt="sing up image"></figure>
        
    </div>
 

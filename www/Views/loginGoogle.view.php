<?php
    namespace Easyshare\Views;

use Easyshare\Core\Routing;
    use Easyshare\Core\Form;

    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
    require './Core/OAuth2/Google.php';

?>
    <div class="signup-form">
        <h3 class="form-title">Bonjour <?php echo $googlename ?>, encore un petit effort..</h3>
        <p>Votre nom</p>
        <input type="text" id="fname" name="firstname" disabled="true" value='<?php echo $googlelast_name ?>'>

        <p>Votre prénom</p>
        <input type="text" id="lname" name="lastname" disabled="true" value='<?php echo $googlename ?>'>

        <p>Votre adresse e-mail</p>
        <input type="text" id="lname" name="lastname" disabled="false" value='<?php echo $googleemail?>'>

        <form action="enregistrer_google_user" method="POST">
        <input type="submit" class="google-submit" value="S'inscrire" >
        </form>

        <form action="verification_connexion_google" method="POST">
        <input type="submit" class="google-submit" value="Se connecter">
        </form>
    </div>
    <div class="signup-image">
        <figure><img src="<?php echo $googleimage?>" alt="Facebook Sign Up image"></figure>
    </div>
                


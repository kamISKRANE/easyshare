<?php

    namespace Easyshare\Views;

use Easyshare\Core\Routing;
    use Easyshare\Core\Form;

    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body id="back">


  <section class="row">
    <article class="col-md-offset-1 col-md-10">
      </br><?php Form::showFormInformationsAndErrors(); ?>
      <label for="title"><h1>Ajouter une page</label>
        <img src="public/icons/edit.png" class="icon-side">
      </h1>
    </article>
  </section>
  
  <section >

    <article class="box1 col-md-offset-1 col-md-10">

    <?php
    $this->addModal("form", $configFormCreatePage);
    ?>
    </article>

  </section>
  
	<script src="https://cdn.ckeditor.com/4.8.0/full-all/ckeditor.js"></script>

  <script>

    CKEDITOR.replace('editor');
    var urlAlreadyTaken = <?php echo $urlAlreadyTaken ; ?> ;

    function checkIfUrlIsAvailable(){
      var urlElement = document.getElementById('url');
        var url = urlElement.value.trim();
        
        //Verif si l'url choisie par l'Admin n'est pas deja utilisé par une autre page.
        if(urlAlreadyTaken.indexOf('/'+url.toLowerCase())!= -1){
            alert("Cette URL n'est pas disponible, veuillez en saisir une autre");
            urlElement.style.background = '#FF3232';
            return false;
        }
        return true;
    };

  </script>

    
</body>
</html>





<?php
    namespace Easyshare\Views;

use Easyshare\Core\Routing;
    use Easyshare\Core\Form;

    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
    require './Core/OAuth2/Facebook.php';
    
?>
    <div class="signup-form">
        <h2 class="form-title">S'inscrire avec Facebook</h2>
        <p>Votre nom</p>
        <input type="text" id="fname" name="firstname" disabled="true" value='<?php echo $fblast_name ?>'>

        <p>Votre prénom</p>
        <input type="text" id="lname" name="lastname" disabled="true" value='<?php echo $fbname ?>'>

        <p>Votre adresse e-mail</p>
        <input type="text" id="lname" name="lastname" disabled="true" value='<?php echo $fbemail ?>'>

        <p>Votre date de naissance</p>
        <input type="text" id="lname" name="lastname" disabled="true" value='<?php echo $fbbirthday ?>'>
        <form action="enregistrer_fb_user" method="POST">
            <input type="submit" class="fb-submit" value="S'inscrire" >
        </form>

        <form action="verification_connexion_fb" method="POST">
            <input type="submit" class="fb-submit" value="Se connecter">
        </form>
    </div>
    <div class="signup-image">
        <figure><img src="../public/images/facebook.jpg" alt="Facebook Sign Up image"></figure>
    </div>


<?php
    namespace Easyshare\Views;

use Easyshare\Core\Routing;
    use Easyshare\Core\Form;

    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }

?>

                    <div class="signup-form">
                        <h2 class="form-title">Un dernier petit effort..</h2>
    <h1>Un lien de confirmation vous a été envoyé par mail. Veuillez l'utiliser pour activer votre compte.</h1>

<a class="google-submit" href="<?php echo "/resend_activate_mail?".$_SESSION["user"]["email"];?>">Envoyer à nouveau</a>

                    </div>
                    <div class="signup-image">
                        <figure><img src="../public/images/easyshare.jpg" alt="Logo Easyshare"></figure>
                        
                    </div>
                </div>


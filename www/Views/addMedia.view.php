<?php

namespace Easyshare\Views;

use Easyshare\Core\Routing;
use Easyshare\Core\Form;

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

?>
<!DOCTYPE html>
<html>
  <head>
    <title>Ajouter une musique</title>
  </head>

  <body id="back" class="container">
    
    <section class="row">
      <article class="col-md-offset-1 col-md-10">
        <?php Form::showFormInformationsAndErrors(); ?>
        <label for="title"><h1>Ajouter une musique
          <img src="public/icons/addmusic.png" ></h1></label>
        
      </article>
    </section>
    
    <section >
      <article class="box1 col-md-offset-1 col-md-10">
        <?php
          $this->addModal("form", $configFormAddMedia);
        ?>
      </article>
    </section>  
    
    <script  src="js/index.js"></script>

  </body>
        
</html>
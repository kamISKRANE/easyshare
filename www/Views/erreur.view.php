<!DOCTYPE html>
    <html>
        <head>
        </head>
        <body>
            <section id="error" class="row">
                <article class=" col-md-offset-2 col-md-8">
                    <div class="container2 col-md-9">
                        <h1>Oups ! </h1>
                    </div>
                    <div class="container2 col-md-12">
                        <h1>La page que vous cherchez semble introuvable..</h1>
                    </div>
                </article>
                       
                <article class=" col-md-offset-2 col-md-6">
                    <div class="container2 col-md-12">
                        <a href="/" class="cta-button cta-button--white">Retourner à la page d'accueil</a>
                    </div>
                </article>
            </section>
        </body>
    </html>
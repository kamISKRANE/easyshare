<?php
    namespace Easyshare\Views;

use Easyshare\Core\Routing;

    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
?>

        <div class="signup-form">
          <h2 class="form-title">Mot de passe oublié</h2>
						
        <div class="card-body">
          <div class="text-center mb-4">
            <h4>Aie.. Vous avez perdu votre mot de passe ? </h4>
            <p>Entrez votre mail pour qu'on puisse vous envoyez les instructions afin de le récupérer</p>
            </div><br>
            <?php
              $this->addModal("form", $forgotenPassForm);
            ?><br>
            <div class="text-center">
              <a class="signup-image-link" href="<?php echo Routing::getSlug("User", "add") ?>">S'inscrire</a>
              <a class="signup-image-link" href="<?php echo Routing::getSlug("User", "login") ?>">Se connecter</a>
            </div>
          </div>
        </div>


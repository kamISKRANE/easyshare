<?php
    return [
        '\\Easyshare\\Models\\'.Comment::class => function ($container) {
            return new \Easyshare\Models\Comment();
        },
        '\\Easyshare\\Models\\'.Media::class => function ($container) {
            return new \Easyshare\Models\Media();
        },
        '\\Easyshare\\Models\\'.Menu::class => function ($container) {
            return new \Easyshare\Models\Menu();
        },
        '\\Easyshare\\Models\\'.Page::class => function ($container) {
            return new \Easyshare\Models\Page();
        },
        '\\Easyshare\\Models\\'.User::class => function ($container) {
            return new \Easyshare\Models\User();
        },
        '\\Easyshare\\Models\\'.Template::class => function ($container) {
            return new \Easyshare\Models\Template();
        },
        '\\Easyshare\\Models\\'.Category::class => function ($container) {
            return new \Easyshare\Models\Category();
        },

        '\\Easyshare\\Controllers\\'.CommentController::class => function ($container) {
            $comment = $container['\\Easyshare\\Models\\'.Comment::class]($container);
            return new \Easyshare\Controllers\CommentController($comment);
        },
        '\\Easyshare\\Controllers\\'.MediaController::class => function ($container) {
            $media = $container['\\Easyshare\\Models\\'.Media::class]($container);
            return new \Easyshare\Controllers\MediaController($media);
        },
        '\\Easyshare\\Controllers\\'.MenuController::class => function ($container) {
            $menu = $container['\\Easyshare\\Models\\'.Menu::class]($container);
            return new \Easyshare\Controllers\MenuController($menu);
        },
        '\\Easyshare\\Controllers\\'.PageController::class => function ($container) {
            $page = $container['\\Easyshare\\Models\\'.Page::class]($container);
            return new \Easyshare\Controllers\PageController($page);
        },
        '\\Easyshare\\Controllers\\'.UserController::class => function ($container) {
            $user = $container['\\Easyshare\\Models\\'.User::class]($container);
            return new \Easyshare\Controllers\UserController($user);
        },
        '\\Easyshare\\Controllers\\'.TemplateController::class => function ($container) {
            $template = $container['\\Easyshare\\Models\\'.Template::class]($container);
            return new \Easyshare\Controllers\TemplateController($template);
        },
        '\\Easyshare\\Controllers\\'.CategoryController::class => function ($container) {
            $category = $container['\\Easyshare\\Models\\'.Category::class]($container);
            return new \Easyshare\Controllers\CategoryController($category);
        },
    ];

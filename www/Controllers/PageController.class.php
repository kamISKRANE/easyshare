<?php

declare(strict_types=1);

namespace Easyshare\Controllers;

use Easyshare\Core\View;
use Easyshare\Core\Routing;
use Easyshare\Core\BaseSQL;
use Easyshare\Core\Acl;
use Easyshare\Core\Form as formUtility;
use Easyshare\Views\modals\form;
use Easyshare\Models\Menu;
use Easyshare\Models\Page;
use Easyshare\Models\Comment;

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

class PageController
{
    private $pageModel;

    //Si on doit contracter une instanciation d'objet
    public function __construct(Page $page)
    {
        $this->pageModel = $page;
    }

    //Si l'utilisateur est un admin affiche la page Admin.
    //Si l'utilisateur est un visiteur afiche la page d'accueil de l'amin sinon, affiche la page générique d'accueil pour visiteur.
    public function defaultAction(): void
    {
        if (isset($_SESSION["user"]["role_id"]) && $_SESSION["user"]["role_id"] == 1) {
            $v = new View("homeback", "back");
            $v->assign("nbUser", $this->pageModel->getNbUser());
            $v->assign("nbPageView", $this->pageModel->getNbPageView());
            $v->assign("nbMusic", $this->pageModel->getNbMusic());
            $v->assign("nbComment", $this->pageModel->getNbComment());
            $v->assign("StatMusic", $this->pageModel->getStatMusic());
            $v->assign("nbCommentToday", $this->pageModel->getNbCommentToday());
        } else {
            $this->goToFrontAction();
        }
    }

    public function goToFrontAction(): void
    {
        $homepageOfAdmin = $this->pageModel->getOneBy(array('homepage'=> '1'));
        if ($homepageOfAdmin != null) {
            //On redirige vers la page d'accueil que l'administrateur à crée.
            header("location: ".$homepageOfAdmin['label']);
        } else {
            $v = new View("homefront", "front");
            $v->assign('defaultMenu', $this->getHomeMenu());
        }
    }

    public function showAllPageAction(): void
    {
        $pages = $this->pageModel->getAll();
        $view = new View("listPage", "back", "Liste des pages");
        $view->assign('pages', $pages);
        $view->assign('pathForCreate', Routing::getSlug("Page", "createPage"));
        $view->assign('pathForUpdate', Routing::getSlug("Page", "updatePage"));
        $view->assign('pathForDelete', Routing::getSlug("Page", "deletePage"));
    }

    //Affiche la page stocker en bdd en fonction du Slug.
    public function adminPageAction(): void
    {
        $slug = Routing::getSlugUrl();

        $content = $this->pageModel->getOneBy(array('label'=> $slug));
        
        if ($content == null) {
            header("location: /erreur");
        }

        //Si la page est non publique ET l'utilisateur n'a pas les droits : Redirige vers la page erreur.
        if (($content['available'] != 1)  and (Acl::currentUserIsAdmin() == false)) {
            header("location: /erreur");
        }
        $contentPage = $content['content'];
        $title = preg_replace('#/#', '', $slug);
        $view = new View("adminPage", "front", $content['title'], $content['description']);

        //On dissocie les medias du contenu de la page.
        if ($this->checkIfPageHasMedia($contentPage)) {
            //Recupère le contenu de la page ainsi que les medias associée
            $contentAndMedia = $this->exportMediaOfContent($contentPage);

            //On reafecte le contenue de la page sans les medias
            $contentPage = $contentAndMedia["content"];

            //Si une ou plusieur musiques que l'Admin demande, existe sur le serveur:
            if ($contentAndMedia["media"] != false) {
                //On assigne uniquement si il y a des medias dans la pages
                $view->assign("medias", $contentAndMedia["media"]);
            }
        }

        $view->assign("content", $contentPage);
        $view->assign("slug", $slug);
        
        $view->assign('defaultMenu', $this->getHomeMenu());
        if (intval($content["comment"]) == 1) {
            $comments = $this->getCommentsAction($content["id"]);
            $comment_section = '<div class="comment_container">
										<form action="/ajouter_commentaire?'.$content["id"].'" method="POST">
										  <div class="row">
										    <label class="required" for="message">Votre commentaire:</label><br />
										    <textarea id="message" class="input" name="message" placeholder="Donnez nous votre avis en écrivant ici.." cols="92" rows="15"></textarea><br />
										  </div>
										  <input id="submit_button" type="submit" value="Poster" /> <br><br>
										</form>
									</div>
								</div></article></section>';
            if (isset($comments)) {
                $view->assign('comments', $this->displayComments($comments));
            }
            $view->assign('comments_add_field', $comment_section);
        }
        
        //On affecter les boutons modifier et supprimer uniquement si l'utilisateur est administrateur
        if (Acl::currentUserIsAdmin() == true) {
            $view->assign("updatePage", "<a href=".Routing::getSlug('Page', 'updatePage')."?label=".$slug."><input style='background: #78C07B;border: 0px;color: #fff;font-size: 15px;width: auto;padding: 10px 15px;margin-bottom: 10px ;border-radius: 10px;' type='button' value='Modifier la page'></a>");
            $view->assign("deletePage", "<input id='deletePage'style='background: #db4a4a;border: 0px;color: #fff;font-size: 15px;width: auto;padding: 10px 15px;margin-bottom: 10px;border-radius: 10px;' type='button' value='Supprimer la page'>");
        }

        //On augmente de 1 le nombre de vues sur cette Page pour les statistiques.
        $newNumberOfView = ($content["count_views"] == null)? 1 : $content["count_views"] + 1 ;
        $this->pageModel->incraseViewsOfPage($content["id"], $newNumberOfView);
    }

    //Creation d'une nouvelle page.
    public function createPageAction(): void
    {
        $view = new View("createPage", "back", "Ajouter une page");
        $view->assign('configFormCreatePage', $this->pageModel->getFormCreatePage());

        $urlAlreadyTaken = array_map('strToLower', formUtility::tranformDoubeDimensionArrayToAnSingle($this->pageModel->getAll("label")));
        $view->assign('urlAlreadyTaken', json_encode($urlAlreadyTaken));
    }

    //Enregistre en BDD une nouvelle page
    public function savePageAction(): void
    {
        if (($_SERVER["REQUEST_METHOD"] == formUtility::getElementFromTheForm($this->pageModel->getFormCreatePage(), "config", "method")) && !empty($_POST['url']) && $this->checkShortcodeMedia($_POST['ckeditor']) && !empty($_POST['title']) && !empty($_POST['ckeditor']) && !empty($_POST['description'])) {
            //On verif une deuxième fois que l'URL est disponible
            if ($this->pageModel->getOneBy(array('label'=> '/'.$_POST['url']))!= false) {
                $_SESSION['form']['error'] = "Cette URL n'est pas disponible, veuillez en saisir une autre";
                header("location: /creation_de_page");
            }

            if (isset($_POST['available'])) {
                $this->pageModel->setAvailable(1);
            }
            if (isset($_POST['comment'])) {
                $this->pageModel->setComment(1);
            }
            if (isset($_POST['homepage'])) {
                $this->pageModel->setHomepage(1);
                //On désactive le propriété de "page d'accueil" sur l'ancienne page.
                $this->pageModel->disableAllHomepage();
            }
            $this->pageModel->setLabel('/'.$_POST['url']);
            $this->pageModel->setTitle($_POST['title']);
            $this->pageModel->setContent($_POST['ckeditor']);
            $this->pageModel->setDescription($_POST['description']);
            $this->pageModel->save();
            $this->generateSitemap();
            header("location: /".$_POST['url']);
        } else {
            if ($_POST['ckeditor'] == "") {
                $_SESSION['form']['error'] = formUtility::getElementFromTheForm($this->pageModel->getFormCreatePage(), "data", "ckeditor", "error");
            }
            header("location: /creation_de_page");
        }
    }

    //formulaire de modifiation de la page, en reprennant toute ces précedentes valeurs.
    public function updatePageAction(): void
    {
        if (isset($_GET['label'])) {
            $pageData = $this->pageModel->getOneBy(array('label'=> $_GET['label']));
        }
        if ($pageData != false) {
            $view = new View("updatePage", "back", "Modification ".$_GET['label']);
            $FormCreatePage =  $this->pageModel->getFormUpdatePage();
            
            $FormUpdatePage = formutility::insertDataIntoForm($pageData, $FormCreatePage);
            $urlAlreadyTaken = array_map('strToLower', formUtility::tranformDoubeDimensionArrayToAnSingle($this->pageModel->getAll("label")));

            $view->assign('configFormUpdatePage', $FormUpdatePage);
            $view->assign("formAction", Routing::getSlug("Page", "saveUpdatedPage"));
            $view->assign('urlAlreadyTaken', json_encode($urlAlreadyTaken));

            $oldUrl = (explode('/', $_GET['label']))[1];

            $view->assign("oldUrl", $oldUrl);
        } else {
            header("location: /");
        }
    }

    //enregistre en BDD la page modifié par l'administrateur
    public function saveUpdatedPageAction(): void
    {
        if (($_SERVER["REQUEST_METHOD"] == formUtility::getElementFromTheForm($this->pageModel->getFormUpdatePage(), "config", "method")) && !empty($_POST['oldUrl']) && $this->checkShortcodeMedia($_POST['ckeditor']) && !empty($_POST['newUrl']) && !empty($_POST['title'])  && !empty($_POST['ckeditor']) && !empty($_POST['description'])) {
            $idPage = $this->pageModel->getOneBy(array('label'=> $_POST['oldUrl']))['id'];

            if (isset($_POST['available'])) {
                $this->pageModel->setAvailable(1);
            }
            if (isset($_POST['comment'])) {
                $this->pageModel->setComment(1);
            }
            if (isset($_POST['homepage'])) {
                $this->pageModel->setHomepage(1);
                $this->pageModel->disableAllHomepage();
            }

            $this->pageModel->setId($idPage);
            $this->pageModel->setLabel(trim('/'.$_POST['newUrl']));
            $this->pageModel->setTitle($_POST['title']);
            $this->pageModel->setContent($_POST['ckeditor']);
            $this->pageModel->setDescription($_POST['description']);
            $this->pageModel->setCount_views($this->pageModel->getOneBy(array('id' => $idPage))['count_views']) ;
            $this->pageModel->save();
            $this->generateSitemap();

            header("location: ".$_POST['newUrl']);
        } else {
            header("location: ".Routing::getSlug("Page", "updatePage")."?label=".$_POST['oldUrl']);
        }
    }

    //Suppresion de la page
    public function deletePageAction(): void
    {
        //On peux se permettre de supprimer par le label car il est unique comme l'ID et donc on evite une requete en plus ( getIdPage() ).
        $this->pageModel->deletePage($_GET['slug']);
        $this->generateSitemap();
        header("location: /");
    }

    public function mentionsAction(): void
    {
        $view = new View("mentions", "front", "Mentions Légales", "Mentions Légales du CMS Easyshare");
    }

    public function nousAction(): void
    {
        $view = new View("nous", "front");
    }

    //récupérer le menu de la page d'acceuil.
    public function getHomeMenu(): ?String
    {
        $menu = new Menu();
        $result = $menu->getDefaultMenu();
        $menu_html = "<ul>";
        if (isset($result) && strlen($result["contenu"])) {
            $decoded = json_decode($result['contenu'], true);
            $menu_html .= $this->convertJson($decoded);
        }
        $menu_html .= '<li><a>Mon compte</a>
                            <div class="dropdown-contents">
                                <div class="submenu">
                                <a href="/profil"><input type="submit" class="dropdownbtn" value="Mon profil" ></input></a>
                                <a href="/deconnexion"><input type="submit" class="dropdownbtn" value="Se déconnecter" ></input></a>
                              </div>
                          </div>
                        </li>';
        $menu_html .= "</ul>";
        return $menu_html;
    }

    public function convertJson($menu)
    {
        $html = "";
        foreach ($menu as $link) {
            $html .= '<li><a href="/'.$link["href"].'">'.$link["name"].'</a>';
            if (count($link["children"]) > 0) {
                $html .= '<ul>'.$this->convertJson($link["children"]).'</ul>';
            }
            $html .= '</li>';
        }
        return $html;
    }

    //Récupère les commentaire associés à la page
    public function getCommentsAction($page_id) : ?array
    {
        $comment_object = new Comment();
        $where = ["page_id"=>$page_id];
        $comments = $comment_object->getComments($page_id);
        $result = null;
        if ($comments) {
            $result = array();
            foreach ($comments as $comment) {
                $user = $this->pageModel->getUser($comment["user_id"]);
                $comment["user"] = $user["firstname"];
                $result[] = $comment;
            }
        }
        return $result;
    }


    public function displayComments($comments)
    {
        $div_comments = "";
        if (Acl::userConnected()) {
            foreach ($comments as $comment) {
                $div_comments .= '<hr width="75%" color="#F1F1F1" align="left"><div id="comment_'.$comment["id"].'"class="comment">
									<div class="comment_details">
										<p><b>Publié par : </b>'.$comment["user"].'</p>
										<p><b>Date de publication :</b> '.date("d-m-Y H:i", strtotime($comment["create_date"])).'</p>
									</div>
									<div class="comment_content">'.$comment["content"].'</div>'.((Acl::currentUserIsAdmin())?'<br>
										<a href="/supprimer_commentaire?'.$comment["id"].'"><input type="delete" value="Supprimer" /></a>':'')
                                    .'</div><br><br>';
            }
        }
        return $div_comments;
    }

    public function erreurAction(): void
    {
        $view = new View("erreur", "front");
    }

    //Verifie si l'administrateur a insérer le shortCode Media/Musique
    public function checkIfPageHasMedia($content = ""): ?bool
    {
        if (!empty($content) && (strpos($content, "[[") !== false) && (strpos($content, "]]") !== false)) {
            return true;
        }
        return false;
    }

    //Export de tout les Medias/Musique qui ont été ajouter à la page.
    public function exportMediaOfContent($content): ?array
    {
        $mediaInContent = true;

        if (!empty($content) && (strpos($content, "[[") !== false) && (strpos($content, "]]") !== false)) {
            $newContent = explode('[[', $content)[0];

            $contentPartRight = explode('[[', $content)[1];

            $media = explode(']]', $contentPartRight)[0];
            
            $newContent .= explode(']]', $contentPartRight)[1];

            $media = str_replace('&quot;', '', $media);
            $allmediaArray = explode(',', $media);

            foreach ($allmediaArray as $key => $value) {
                $music = html_entity_decode($value);
                if (file_exists(dirname(__DIR__)."/public/music/".$music)) {
                    $mediasValidated[] = $value;
                }
            }
            $mediasValidated = (isset($mediasValidated))? $mediasValidated : false ;

            //Return un tableau ayant le contenu de la page et les medias dans deux index distinct
            return [
                'content' => $newContent,
                'media' => $mediasValidated
            ];
        }
        //Si il n'y a pas de media retourne le contenu de la page d'origine.
        return $content;
    }

    //Permet de s'assurer que l'admin bien rentré soit 1 shortcode pour les medias soit aucun
    public function checkShortcodeMedia($pageContent): ?bool
    {
        if ((substr_count($_POST['ckeditor'], "[[") == 1 && substr_count($_POST['ckeditor'], "]]") == 1) or (substr_count($_POST['ckeditor'], "[[") == 0 && substr_count($_POST['ckeditor'], "]]") == 0)) {
            return true;
        }
        $_SESSION['form']['error'] = 'Il y a une erreur au niveau du Shortcode musique.
			</br>Si vous voulez ajouter une ou plusieur musique à votre page, veuillez indiquer le nom du ficher entre guillemets en séparant vos musique par des virgules, le tout entre crochets. 
			</br>Exemple : [["musique1.mp3","musique2.mp3" ]] ';

		return false;
	}

	//affiche les statistiques global de l'application.
	public function GetStatAction(): void
	{
	        $view = new View("stat", "back");
	        $view->assign("StatMusic", $this->pageModel->getStatMusic());
	        $view->assign("StatCategory", $this->pageModel->getStatCategory()); 
            $view->assign("StatPage", $this->pageModel->GetStatPage());
	}

	public function GetAideAction(): void
	{
	        $view = new View("aide", "back");
	}

	//génération du fichier sitemap.xml
	public function generateSitemap(){
	    $xmlDoc = new \DOMDocument('1.0', 'utf-8');
        $urlset = $xmlDoc->createElement('urlset');
        $urlset->setAttribute('xmlns', "http://www.google.com/schemas/sitemap/0.9");

        //Ajout des pages du front
        $links = ["", "connexion", "creation_de_compte", "mot_de_passe_oublie", "home", "creation_de_compte",
        "reinitialiser_mot_de_passe", "nous", "erreur", "profil", "loginFacebook", "loginGoogle"];
        $arrayLength = count($links);
        $i = 0;
        while ($i < $arrayLength) {
            $url = $xmlDoc->createElement('url');
            $loc = $xmlDoc->createElement('loc', 'https://easyshare.site/' .$links[$i]);
            $priority = $xmlDoc->createElement('priority', "1");
            $lastmod = $xmlDoc->createElement('lastmod', date(DATE_ATOM));
            $url->appendChild($loc);
            $url->appendChild($priority);
            $url->appendChild($lastmod);
            $urlset->appendChild($url);
            echo $CodeWallTutorialArray[$i] ."<br />";
            $i++;
        }

        //Ajout de toutes les pages créées
        $pages = $this->pageModel->getAll();
        foreach ($pages as $page) {
            if ($page['available']==1) {
                $url = $xmlDoc->createElement('url');
                $loc = $xmlDoc->createElement('loc', 'https://easyshare.site' .$page['label']);
                $priority = $xmlDoc->createElement('priority', "1");
                $lastmod = $xmlDoc->createElement('lastmod', date(DATE_ATOM));
                $url->appendChild($loc);
                $url->appendChild($priority);
                $url->appendChild($lastmod);
                $urlset->appendChild($url);
            }
        }

        //Ajout des mentions légales
        $url = $xmlDoc->createElement('url');
        $loc = $xmlDoc->createElement('loc', 'https://easyshare.site/mentions');
        $priority = $xmlDoc->createElement('priority', "0.5");
        $lastmod = $xmlDoc->createElement('lastmod', date(DATE_ATOM));

        $url->appendChild($loc);
        $url->appendChild($priority);
        $url->appendChild($lastmod);
        $urlset->appendChild($url);
        $xmlDoc->appendChild($urlset);
        $xmlDoc->save('sitemap.xml');
    }
}

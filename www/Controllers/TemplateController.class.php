<?php

declare(strict_types=1);

namespace Easyshare\Controllers;

use Easyshare\Core\View;
use Easyshare\Core\Routing;
use Easyshare\Core\BaseSQL;
use Easyshare\Core\Form as formUtility;
use Easyshare\Core\Acl;
use Easyshare\Views\modals\form;
use Easyshare\Models\Menu;
use Easyshare\Models\Page;
use Easyshare\Models\Comment;
use Easyshare\Models\Template;
use Easyshare\Core\Validator;

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

class TemplateController
{
    private $templateModel;

    //Si on doit contracter une instanciation d'objet
    public function __construct(Template $template)
    {
        $this->templateModel = $template;
    }

    public function personalizationWebsiteAction(): void
    {
        $template = new View('personalizationWebsite', 'back', 'personalisation du site web');
        $template->assign('configStyleWebsite', $this->templateModel->getFormConfigStyleWebsite());
    }

    public function saveConfigAdminAction(): ?array
    {
        //API pour recupérer tout les font-family disponible via Google, à voir si on l'implémente via une liste déroulante.
        /* $apiKey = "AIzaSyAoB4hd63fPRusVeHcfoApkgZaHa1wM_Ek";
    	$UrlForStyle = https://www.googleapis.com/webfonts/v1/webfonts?key=AIzaSyAoB4hd63fPRusVeHcfoApkgZaHa1wM_Ek".$apiKey; */

        $configForm = $this->templateModel->getFormConfigStyleWebsite();
        $method = strtoupper($configForm["config"]["method"]);
        $data = $GLOBALS["_".$method];
        $validatorTemplate = new Validator($configForm, $data);
        $_SESSION['form']['error'] = $validatorTemplate->errors;
        if (empty($_SESSION['form']['error'])) {

            //On recupère le template original pour la personalisation
            $template =  file_get_contents(dirname(__DIR__).'/public/css/template/originStyle.css');

            if ($template != false) {
                //Pour chaque donnée reçu on remplace les index identiques avec la valeur reçu dans notre $template
                foreach ($_POST as $key => $value) {
                    if (strpos($template, '%'.$key.'%')) {
                        $template = str_replace('%'.$key.'%', $value, $template);
                    }
                }
                $newStyle = file_put_contents(dirname(__DIR__).'/public/css/adminStyle.css', $template);
            } elseif (($template != false) or ($newStyle == false)) {
                $_SESSION['form']['error'] = "Une erreur est survenue.";
                header("location: ". Routing::getslug("Template", "personalizationWebsite"));
            }
            header("location: /");
        } else {
            header("location: ". Routing::getslug("Template", "personalizationWebsite"));
        }
    }

    public function restoreDefaultConfigAction(): void
    {
        //On format les paramètre de style en vidant totalement le fichier adminStyle.css .
        if (file_put_contents(dirname(__DIR__).'/public/css/adminStyle.css', "") !== false) {
            $_SESSION['form']['info'] = "Les paramètres de style ont été reinitialisés.";
        } else {
            $_SESSION['form']['error'] = "Une erreur est survenue.";
        }
        header("location: ". Routing::getslug("Template", "personalizationWebsite"));
    }
}

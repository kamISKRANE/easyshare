<?php
declare(strict_types=1);
namespace Easyshare\Controllers;

use Easyshare\Models\User;
use Easyshare\Core\View;
use Easyshare\Core\Validator;
use Easyshare\Core\BaseSQL;
use Easyshare\Core\Routing;
use Easyshare\Core\Acl;
use Easyshare\Core\Form as formUtility;
use Easyshare\Views\loginUser;
use Easyshare\Views\modals\form;
use Easyshare\Core\Mail;
use Easyshare\Core\OAuth2\OauthFacebook;
use Easyshare\Core\OAuth2\OauthGoogle;
//require("public/vendor/PHPMailer/class.phpmailer.php");

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

class UserController{

	private $userModel;
	private $facebookLogin;
	private $googleLogin;

	public function __construct(User $user)
	{
		$this->userModel = $user;
		$this->facebookLogin = new OauthFacebook();
		$this->googleLogin = new OauthGoogle();
	}
    //Liste les utilisateurs
    public function defaultAction(): void
    {
        $view = new View("listUser", "back");
        $view->assign("users", $this->userModel->getAllUserAvailable());
            
        //Fournis a la vues les Url afin de changer le nom, supprimer, changer le role
        $view->assign("pathForChangeName", Routing::getSlug('User', 'updateUserNameByAdmin'));
        $view->assign("pathForDelete", Routing::getSlug('User', 'deleteUser'));
        $view->assign("pathForUpgradeRole", Routing::getSlug('User', 'upgradeAcl'));
        $view->assign("pathForDowngradeRole", Routing::getSlug('User', 'downgradeAcl'));
    }

    //Affiche le formulaire creation de compte
	public function addAction(): void
	{
		$v = new View("addUser", "login");
		$v->assign("configFormRegister", $this->userModel->getFormRegister());
		
	}
	
	//Verifie les données envoyé et si c'est correcte, enregistre en BDD.
	public function saveAction(): void
	{
		$configForm = $this->userModel->getFormRegister();
		$method = strtoupper($configForm["config"]["method"]);
		$data = $GLOBALS["_".$method];
		$key = md5(''.microtime(TRUE)*100000);
		if($_SERVER["REQUEST_METHOD"]==$method && !empty($data)){
			$validator = new Validator($configForm, $data);
			$_SESSION['form']['error'] = $validator->errors;
			if(empty($_SESSION['form']['error']) && $this->checkEmailExist($data['email']) == 1){
				$this->userModel->setFirstname($data["firstname"]);
				$this->userModel->setLastname($data["lastname"]);
				$this->userModel->setEmail($data["email"]);
				$this->userModel->setPwd($data["pass"]);
				$this->userModel->setDateNaissance($data["date_naissance"]);
				$this->userModel->setToken($key);
				$this->userModel->save();
				if($this->sendMail($this->userModel->getEmail(),"activation")){
					$v = new View("notActive","login");
				}
				else{
	                $_SESSION['form']['info']="L'email n'est pas envoyer";
	            }
			}else{
				if(! $this->checkEmailExist($data['email'])){
					$_SESSION['form']['error'] = "Cette adresse e-mail est déjà utilisée";
				}
				header('location: '.Routing::getSlug('User','add'));
			}
		}
		else{
			header('location: '.Routing::getSlug('User','add'));
		}
		
	}

    //Formulaire de connexion
    public function loginAction(): void
    {
        $v = new View("loginUser", "login");
        $v->assign("configFormRegister", $this->userModel->getFormLogin());
    }

    public function activateAccountAction(): void
    {
        $slug = $_SERVER["REQUEST_URI"];
        $slugExploded = explode("?", $slug);
        $activation = $slugExploded[1];
        $arguments = explode("&", $activation);

        $email = urldecode($arguments[0]);
        $key = $arguments[1];

        $where = ["email"=>$email];
        $result = $this->userModel->getOneBy($where);
        if (isset($result["token"]) && strcmp($result["token"], $key) == 0) {
            $this->userModel->initUser($result);
            $this->userModel->setActive(1);
            $this->userModel->save();
            $_SESSION["form"]["info"] = "Votre a bien été activé";
            $v = new View("loginUser", "login");
            $v->assign("configFormRegister", $this->userModel->getFormLogin());
        } else {
            echo "Votre clé n'est plus valable";
        }
    }

    //Verif les données de connexion de l'utilisateur.
    public function loginValidationAction(): void
    {
        $configForm = $this->userModel->getFormLogin();
        $method = strtoupper($configForm["config"]["method"]);
        $data = $GLOBALS["_".$method];
        $validator = new Validator($configForm, $data);
		$_SESSION['form']['error'] = $validator->errors;
		if (empty($_SESSION['form']['error'])) {
	        $result = $this->userModel->getUserByEmail($data["email"]);
	        if (!empty($result) && isset($result) && $result != false) {
	        	$this->userModel->initUser($result);
	        	if (password_verify($data["pwd"], $this->userModel->getPassword())) {
	        		if ($this->userModel->getActive() == 1) {
			            $_SESSION["user"] = array();
			            $_SESSION["user"]["id"] = $this->userModel->getId();
			            $_SESSION["user"]["role_id"] = $this->userModel->getRoleId();
			            $_SESSION["user"]["firstname"] = $this->userModel->getFirstname();
			            $_SESSION["user"]["lastname"] = $this->userModel->getlastname();
			            $_SESSION["user"]["email"] = $this->userModel->getEmail();
			            $_SESSION["user"]["birth_date"] = $this->userModel->getDateNaissance();
		            	header('location: /');
		            } else {
		            	$v = new View("notActive","login");
		            	$_SESSION["user"]["email"] = $this->userModel->getEmail();
		            }
	        	} else {
					$_SESSION['form']['error'] = "Vérifier les identifiants";
	        	    header('location: /connexion');
	                echo "Vérifier les identifiants";
	            }
	        } else {
	        	$_SESSION['form']['error'] = "Vérifier les identifiants";
	        	header('location: /connexion');
	        }
	    } else {
	    	header('location: '.Routing::getSlug("User", "login"));
	    }
    }
    
    //Methode de deconnexion
    public function logoutAction(): void
    {
        session_destroy();
        header('location: /');
    }
    
    //Mots de passe oublié
    public function forgetPasswordAction(): void
    {
        $v = new View("forgetPasswordUser", "login");
        $v->assign("forgotenPassForm", $this->userModel->getFormPasswordForgot());
    }

    public function reinitPasswordAction(): void
    {
        $data = $GLOBALS["_POST"];
        $where = ["email"=>$data["email"]];
        $result = $this->userModel->getOneBy($where);
        if (isset($result)) {
            $_SESSION["user"]["email"] = $result["email"];
            if ($result["active"] == 1) {
                if ($this->sendMail($data["email"], "reinitialisation")) {
                    header('location: '.Routing::getSlug("User", "login"));
                    $_SESSION['form']['info']="Un mail vous a été envoyé pour réinitialiser votre mot de passe";
                }
            } else {
                $v = new View("notActive", "login");
            }
        }
    }

    public function resendActivationMailAction()
    {
        $slug = $_SERVER["REQUEST_URI"];
        $slugExploded = explode("?", $slug);
        $email = $slugExploded[1];
        if ($this->sendMail($email, "activation")) {
            $v = new View("notActive", "login");
        }
    }

    public function resendReinitialisationMailAction()
    {
        $slug = $_SERVER["REQUEST_URI"];
        $slugExploded = explode("?", $slug);
        $email = $slugExploded[1];
        if ($this->sendMail($email, "reinitialisation")) {
            $v = new View("notActive", "login");
        }
    }

    public function sendMail($email, $type)
    {
        $mail = new Mail();
        $key = md5(''.microtime(true)*100000);
        $where = ["email"=>$email];
        $result = $this->userModel->getOneBy($where);
        $envoye = false;
        if (isset($result)) {
            $this->userModel->initUser($result);
            $this->userModel->setToken($key);
            $this->userModel->save();
            
            if ($type == "reinitialisation") {
                $envoye = $mail->reinitialisationMail($email, $key);
            } elseif ($type == "activation") {
                $envoye = $mail->registrationMail($email, $key);
            }
        }
        return $envoye;
    }

    public function changePasswordAction()
    {
        $slug = $_SERVER["REQUEST_URI"];
        $slugExploded = explode("?", $slug);
        $activation = $slugExploded[1];
        $arguments = explode("&", $activation);

        $email = urldecode($arguments[0]);
        $key = $arguments[1];
        $where = ["email"=>$email];
        $result = $this->userModel->getOneBy($where);
        if (isset($result["token"]) && strcmp($result["token"], $key) == 0) {
            if (session_status() == PHP_SESSION_NONE) {
                session_start();
            }
            $_SESSION["email"] = $email;
            $this->userModel->initUser($result);
            $v = new View("passwordChange", "front");
            $v->assign("newPassword", $this->userModel);
            $v->assign("userEmail", $email);
        }
    }

    public function changePasswordSubmitAction()
    {
        $data = $GLOBALS["_POST"];
        $email = (isset($_SESSION["user"]["email"])?$_SESSION["user"]["email"]:$_SESSION["email"]);
        session_destroy();
        $where = ["email"=>$email];
        $result = $this->userModel->getOneBy($where);
        if (isset($result)) {
            $this->userModel->initUser($result);
            $this->userModel->setPwd($data["pass"]);
            $this->userModel->save();
            header('location: '.Routing::getSlug("User", "login"));
        }
    }

    //Methode permettant de supprimer un utilisateur.
    public function deleteUserAction(): void
    {
        $this->userModel->disabledElementBy(['id'=>$_GET['id']]);
        header('location: '.Routing::getSlug('User', 'default'));
    }

    //Methode permetant de passe un utilisateur à l'état Adminitrateur.
    public function upgradeAclAction(): void
    {
        $this->userModel->updateAclUser(true, $_GET['id']);
        header('location: '.Routing::getSlug('User', 'default'));
    }

    //Methode permetant de passe un administrateur à l'état utilisateur.
    public function downgradeAclAction(): void
    {
        $this->userModel->updateAclUser(false, $_GET['id']);
        header('location: '.Routing::getSlug('User', 'default'));
    }

    //Modifie le nom ou le prenom de l'utilisateur en fonction donnée recu.
    public function updateUserNameByAdminAction(): void
    {
        //Permet de savoir si on modifie le Nom ou le prenom de l'utilisateur.
        if (isset($_GET['id']) && isset($_GET['lastname'])) {
            //On alimente notre objet $this->userModel
            $this->userModel->getOneBy(array('id'=>$_GET['id']), true);
            $this->userModel->setLastname($_GET['lastname']);
            $this->userModel->save();
        } elseif (isset($_GET['id']) && isset($_GET['firstname'])) {
            //On alimente notre objet $this->userModel
            $this->userModel->getOneBy(array('id'=>$_GET['id']), true);
            $this->userModel->setFirstname($_GET['firstname']);
            $this->userModel->save();
        }
        //redirection vers la listes des utilisateurs
        header('location: '.Routing::getSlug('User', 'default'));
    }

    //Affiche les informations concernant le profil utilsateur avec la possibilité de les modifier
    public function profileAction(): void
    {
        $userData = $this->userModel->getUser($_SESSION["user"]["id"]);

        if ($userData != null) {
            $view = new View("profil", "front", "Profil");
            //Permet d'associer chaque valeur de l'utilisateur à l'emplacement destiner dans le formulaire, afin de pré-remplir le tableaux.
            $formWithUserValue = formUtility::insertDataIntoForm($userData, $this->userModel->getFormUpdate());
            $view->assign("configUpdateRegister", $formWithUserValue);
        } else {
            //Dans le cas ou une erreur de session arrive (Time), demande de se connecter pour re-affecter les variables de session.
            header('location: '.Routing::getSlug('User', 'login'));
        }
    }

    //Modifie les données de l'utilisateur via le formulaire du profil.
    public function updateProfileAction(): void
	{
		$configForm = $this->userModel->getFormUpdate();
		$method = strtoupper($configForm["config"]["method"]);
		$data = $GLOBALS["_".$method];

		if($_SERVER["REQUEST_METHOD"]==$method && !empty($data)){

			$validator = new Validator($configForm, $data);
			$_SESSION['form']['error'] = $validator->errors;
			if(empty($_SESSION['form']['error'])){
				$this->userModel->getOneBy(array('id'=> $_SESSION["user"]["id"]), true);
				$this->userModel->setFirstname($data["firstname"]);
				$this->userModel->setLastname($data["lastname"]);
				$this->userModel->setEmail($data["email"]);
				$this->userModel->setDateNaissance($data["date_naissance"]);
				if(isset($data["pass"])){
					$this->userModel->setPwd($data["pass"]);
				}
				$this->userModel->save();
				$_SESSION['form']['info'] = "Profil Modifié";
			}
            else
            {
                header('location: '.Routing::getSlug('User','profile'));
            }
		}
        header('location: '.Routing::getSlug('User','profile'));
        
	}

	public function loginFacebookAction(): void
	{
	        //$view=new View("loginFacebook", "login");
		$url = $this->facebookLogin->getAuthorizationUrl();
		header('location: '.$url);
	}

	public function loginFacebookGetCodeAction(): void
	{
		$slug = $_SERVER["REQUEST_URI"];
		$slugExploded = explode("?", $slug);
		$getCode = $slugExploded[1];
		$arguments = explode ("=",$getCode);
		$code = $arguments[1];
		$this->facebookLogin->setCode($code);
		$urlToken = $this->facebookLogin->getAccessTokenUrl();
		$token = file_get_contents($urlToken);
		$obj = json_decode($token);
		var_dump($obj->access_token);
    	$this->facebookLogin->setToken($access_token);
	}

	public function loginFacebookGetTokenAction(): void
	{
		$slug = $_SERVER["REQUEST_URI"];
		//echo $slug;
		//echo $this->facebookLogin->getToken();
	}

	public function saveFacebookAction(): void
	{			

				$result = $this->userModel->getUserByEmail($_SESSION['AUTHFB']["email"]);

		       	if ($result['email'] == null){
					$this->userModel->setFirstname($_SESSION['AUTHFB']['firstname']);
					$this->userModel->setLastname($_SESSION['AUTHFB']['lastname']);
					$this->userModel->setEmail($_SESSION['AUTHFB']['email']);
					$this->userModel->setPwd($_SESSION['AUTHFB']['mdp']);
					$this->userModel->setDateNaissance('1997-10-24');
					$this->userModel->setToken('');
					$this->userModel->setActive(1);
					$this->userModel->save(); 

					$this->loginValidationFbAction(); 
                }else{
                	$_SESSION['form']['error'][0]="Cette adresse email est déjà associé à un compte, veuillez vous connecter avec";
	                header('location: '.Routing::getSlug('User','login'));
	            }
	}

	public function loginValidationFbAction (): void
	{ 
		        $result = $this->userModel->getUserByEmail($_SESSION['AUTHFB']['email']);
	        if(!empty($result) && isset($result) && $result != false){
	        	$this->userModel->initUser($result);
	        	if(password_verify($_SESSION['AUTHFB']["mdp"], $this->userModel->getPassword())){
		            $_SESSION["user"] = array();
		            $_SESSION["user"]["id"] = $this->userModel->getId();
		            $_SESSION["user"]["role_id"] = $this->userModel->getRoleId();
		            $_SESSION["user"]["firstname"] = $this->userModel->getFirstname();
		            $_SESSION["user"]["lastname"] = $this->userModel->getlastname();
		            $_SESSION["user"]["email"] = $this->userModel->getEmail();
		            $_SESSION["user"]["birth_date"] = $this->userModel->getDateNaissance();
	            	header('location: /');
	        	}else{
	        	    header('location: /connexion');
	                echo "Vérifier les identifiant";
	            }
	        }
	}
        if ($_SERVER["REQUEST_METHOD"]==$method && !empty($data)) {
            $validator = new Validator($configForm, $data);
            $_SESSION['form']['error'] = $validator->errors;
            if (empty($_SESSION['form']['error'])) {
                $this->userModel->getOneBy(array('id'=> $_SESSION["user"]["id"]), true);
                $this->userModel->setFirstname($data["firstname"]);
                $this->userModel->setLastname($data["lastname"]);
                $this->userModel->setEmail($data["email"]);
                $this->userModel->setDateNaissance($data["date_naissance"]);
                if (isset($data["pass"])) {
                    $this->userModel->setPwd($data["pass"]);
                }
                $this->userModel->save();
                $_SESSION['form']['info'] = "Profil Modifié";
            } else {
                header('location: '.Routing::getSlug('User', 'profile'));
            }
        }
        header('location: '.Routing::getSlug('User', 'profile'));
    }

	public function loginGoogleAction(): void
	{
        $url = $this->googleLogin->getAuthorizationUrl();
		header('location: '.$url);
	}

	public function loginGoogleValidateAction(): void
	{
		$code = $_GET['code'];
		$this->googleLogin->setCode($code);
		$url = $this->googleLogin->getAccessTokenUrl();
		$params = $this->googleLogin->getParams();
		$ch = curl_init();
	    curl_setopt($ch, constant("CURLOPT_" . 'URL'), $url);
	    curl_setopt($ch, constant("CURLOPT_" . 'POST'), true);
	    curl_setopt($ch, constant("CURLOPT_" . 'POSTFIELDS'), $params);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    $output = curl_exec($ch);
	    $info = curl_getinfo($ch);
	    curl_close($ch);

    	$arr = json_decode($output, true);
	    $this->googleLogin->setToken($arr["access_token"]);
	    $user = $this->googleLogin->getUsersInfo();
		$this->saveGoogleAction($user);
	}

public function saveGoogleAction($userArray): void
	{			

				$result = $this->userModel->getUserByEmail($userArray["email"]);

		       	if ($result['email'] == null){
					$this->userModel->setFirstname($userArray['given_name']);
					$this->userModel->setLastname($userArray['family_name']);
					$this->userModel->setEmail($userArray['email']);
					$this->userModel->setPwd($userArray['id']);
					$this->userModel->setDateNaissance('1997-10-24');
					$this->userModel->setToken('');
					$this->userModel->setActive(1);
					$this->userModel->save(); 

    				$_SESSION['AUTHGOOGLE']['id'] = $userArray["id"];

					$this->loginValidationGoogleAction($userArray); 
                }else{
	                
					$this->loginValidationGoogleAction($userArray); 
	            }
	}

	public function loginValidationGoogleAction ($user): void
	{ 
		        $result = $this->userModel->getUserByEmail($user["email"]);
	        if(!empty($result) && isset($result) && $result != false){
	        	$this->userModel->initUser($result);
	        	if(password_verify($user["id"], $this->userModel->getPassword())){
		            $_SESSION["user"] = array();
		            $_SESSION["user"]["id"] = $this->userModel->getId();
		            $_SESSION["user"]["role_id"] = $this->userModel->getRoleId();
		            $_SESSION["user"]["firstname"] = $this->userModel->getFirstname();
		            $_SESSION["user"]["lastname"] = $this->userModel->getlastname();
		            $_SESSION["user"]["email"] = $this->userModel->getEmail();
		            $_SESSION["user"]["birth_date"] = $this->userModel->getDateNaissance();
	            	header('location: /');
	        	}else{
	        	    header('location: /connexion');
	                echo "Vérifier les identifiant";
	            }
	        }
	    }

    public function loginValidationGoogleAction(): void
    {
        $result = $this->userModel->getUserByEmail($_SESSION['AUTHGOOGLE']['email']);
        if (!empty($result) && isset($result) && $result != false) {
            $this->userModel->initUser($result);
            if (password_verify($_SESSION['AUTHGOOGLE']["id"], $this->userModel->getPassword())) {
                $_SESSION["user"] = array();
                $_SESSION["user"]["id"] = $this->userModel->getId();
                $_SESSION["user"]["role_id"] = $this->userModel->getRoleId();
                $_SESSION["user"]["firstname"] = $this->userModel->getFirstname();
                $_SESSION["user"]["lastname"] = $this->userModel->getlastname();
                $_SESSION["user"]["email"] = $this->userModel->getEmail();
                $_SESSION["user"]["birth_date"] = $this->userModel->getDateNaissance();
                header('location: /');
            } else {
                header('location: /connexion');
                echo "Vérifier les identifiant";
            }
        }
    }


    public function checkEmailExist($email): ?bool
    {
        $check = new BaseSQL();
        $result = $check->getUserByEmail($email);
        if (isset($result['id'])) {
            return false;
        }
        return true;
    }
}

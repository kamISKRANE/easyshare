<?php

declare(strict_types=1);

namespace Easyshare\Controllers;

use Easyshare\Core\View;
use Easyshare\Models\Category;
use Easyshare\Core\Routing;
use Easyshare\Core\BaseSQL;
use Easyshare\Core\Form as formUtility;
use Easyshare\Core\Acl;
use Easyshare\Views\modals\form;
use Easyshare\Core\Validator;

class CategoryController
{
    private $categoryModel;

    public function __construct(Category $category)
    {
        $this->categoryModel = $category;
    }

    //Liste toute les catégories
    public function defaultAction(): void
    {
        $v = new View("listCategory", "back", "Liste des catégorie");
        $v->assign("categories", $this->categoryModel->getAll());
        $v->assign("pathForDelete", Routing::getSlug('Category', 'deleteCategory'));
        $v->assign("pathForUpdate", Routing::getSlug('Category', 'updateCategory'));
        $v->assign("pathForCreate", Routing::getSlug('Category', 'addCategory'));
    }

    //Formulaire d'ajout de catégorie
    public function addCategoryAction(): void
    {
        $v = new View("addCategory", "back", "Liste des catégorie");
        $v->assign("configFormAddCategory", $this->categoryModel->getFormAddCategory());
        $v->assign("pathForListCategory", Routing::getSlug('Category', 'default'));
    }

    //Enregistre en BDD une nouvelle catégorie
    public function saveCategoryAction(): string
    {
        //Recupère les informations du formulaire.
        $configFormAddCategory = $this->categoryModel->getFormAddCategory();
        $method = strtoupper($configFormAddCategory["config"]["method"]);
        $data = $GLOBALS["_".$method];
        if (($_SERVER["REQUEST_METHOD"] == $method) && !empty($_POST['label'])) {
            $validatorForm = new Validator($configFormAddCategory, $data);
            $_SESSION['form']['error'] = $validatorForm->errors;
            if (empty($_SESSION['form']['error']) && $this->checkLabelCategoryExistAction($data['label']) == 1) {
                $this->categoryModel->setLabel($data['label']);
                $this->categoryModel->save();
                $_SESSION['form']['info']="La catégorie à bien été ajoutée";
                header('location: '.Routing::getSlug('Category', 'addCategory'));
            } else {
                if (! $this->checkLabelCategoryExistAction($data['label'])) {
                    $_SESSION['form']['error'] = "Ce nom de catégorie est déjà utilisé";
                }
                header('location: '.Routing::getSlug('Category', 'addCategory'));
            }
        } else {
            header('location: '.Routing::getSlug('Category', 'addCategory'));
        }
    }

    public function deleteCategoryAction(): void
    {
        if (isset($_GET['labelCategory']) && !empty($_GET['labelCategory'])) {
            $this->categoryModel->delete($_GET['labelCategory']);
        }
        header('location: '.Routing::getSlug('Category', 'default'));
    }

    public function checkLabelCategoryExistAction($labelCategory): bool
    {
        $where = ['label' => $labelCategory];
        $result = $this->categoryModel->getOneBy($where);
        if (isset($result['id'])) {
            return false;
        }
        return true;
    }

    public function updateCategoryAction(): string
    {
        if (isset($_GET['id']) && isset($_GET['label']) && !empty($_GET['id']) && !empty($_GET['label'])) {
            $this->categoryModel->setId($_GET['id']);
            $this->categoryModel->setLabel($_GET['label']);
            //Quand Id n'est pas a Null, alors save va update les données.
            $this->categoryModel->save();
        }
        header('location: '.Routing::getSlug('Category', 'default'));
    }
}

<?php
declare(strict_types=1);

namespace Easyshare\Controllers;

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

class InstallerController
{
    public static function initAction(): ?String
    {
        $_POST["etape"] = "user_data";
        return '<!DOCTYPE html>
		<html>
			<head>
				<link rel="stylesheet" href="/public/css/style.css">
			</head>
			<body id="installer">
				<section class="sign-in">
			    	<div class="container">
				        <section class="signup">
				        	<div class="signup-content">
			                	<form method="POST" action="installer?2">
			                		Titre du site : <input type="text" class="form-group" name="site_title">
			                		Nom de la base de donnée : <input type="text" class="form-group" name="db_name">
			                		Nom du host: <input type="text" class="form-group" name="db_host">
			                		Nom d\'utilisateur : <input type="text" class="form-group" name="db_username">
			                		Mot de passe de la base de donnée : <input type="text" class="form-group" name="db_password">
			                		<input type="submit" name="continue" value="Continuer">
			                	</form>
		                	</div>
			            </section>
			        </div>
			    </section>
			 </body>
		</html>';
    }
}

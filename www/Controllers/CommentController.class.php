<?php
declare(strict_types=1);

namespace Easyshare\Controllers;

use Easyshare\Core\View;
use Easyshare\Core\Routing;
use Easyshare\Models\Comment;

if (session_status() == PHP_SESSION_NONE) {
    session_start();
    if (!isset($_SESSION["user"])) {
        header("location: /connexion");
    }
}
class CommentController
{
    private $commentModel;

    public function __construct(Comment $comment)
    {
        $this->commentModel = $comment;
    }

    public function createCommentAction()
    {
        $v = new View("comment", "back");
    }

    public function saveCommentAction()
    {
        $data = $GLOBALS["_POST"];
        $content = $data["message"];
        $user_id = $_SESSION["user"]["id"];
        $page_id = Routing::getSlugArgs();
        $comment = new Comment();
        $comment->setUserId((int)$user_id);
        $comment->setContent($content);
        $comment->setPageId($page_id);
        $comment->save();
        header('Location: '.$_SERVER['HTTP_REFERER']);
    }

    public function supprimerCommentAction()
    {
        $slug = $_SERVER["REQUEST_URI"];
        $slugExploded = explode("?", $slug);

        $this->commentModel->delete($slugExploded[1]);
        header('Location: '.$_SERVER['HTTP_REFERER']);
    }
}

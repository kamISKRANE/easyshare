<?php

declare(strict_types = 1);

namespace Easyshare\Controllers;

use Easyshare\Core\View;
use Easyshare\Core\BaseSQL;
use Easyshare\Models\Menu;

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

class MenuController
{

    //injection de dependance imposible pour new Menu(), car on genère des infos particulière suivant la Methode.
    public function afficherMenuAction()
    {
        $menu = new Menu();
        $result = $menu->getAllMenu();
        $menuDisplayed = "";
        foreach ($result as $menu) {
            $menuDisplayed .= "<li class='menu_item' menu_id=".$menu["id"]." menu_name=".$menu["titre"].">".$menu["titre"]."</li>";
        }
        $v = new View("menu", "back");
        $v->assign('menuList', $menuDisplayed);
    }

    public function ajouterMenuAction()
    {
        $data = $GLOBALS["_POST"];
        $menu = new Menu($data["menu_name"]);
        $menu->save();
        header("location: /menu");
    }

    public function updateAction()
    {
        $content = null;
        if (isset($_POST["menu"])) {
            $content = $_POST["menu"];
        }
        $menu_id = intval($_POST["menu_id"]);
        $menu_name = null;
        if (isset($_POST["menu_name_edit"])) {
            $menu_name = $_POST["menu_name_edit"];
        } else {
            $menu_name = $_POST["menu_name"];
        }
        $menu = new Menu();
        $where = ["id"=>$menu_id];
        $result = $menu->getOneBy($where);
        $this->updateMenu($menu_id, $menu_name, $content, $result);
    }
    
    public function updateMenu($id, $name, $content, $hold_menu)
    {
        $menu = new Menu();
        if ($content != null) {
            $menu->initMenu($id, $name, $content);
        } else {
            $menu->initMenu($id, $name, $hold_menu["contenu"]);
        }
        $menu->setMenuDefault(intval($hold_menu["default_menu"]));
        
        $menu->save();
    }

    public function defineDefaultMenuAction()
    {
        $menu_id = intval($_POST["menu_id"]);
        $menu = new Menu();
        $menu->setDefaultMenu($menu_id);
    }
    
    public function deleteMenuAction()
    {
        $menu_id = intval($_POST["menu_id"]);
        $menu = new Menu();
        $result = $menu->deleteMenu($menu_id);
    }

    public function afficherLiensAction()
    {
        $menu_id = intval($_POST["menu_id"]);
        $menu_name = $_POST["menu_name"];
        $where = ["id"=>$menu_id];
        $menu = new Menu();
        $result = $menu->getOneBy($where);
        $liens = $this->getMenu($result["contenu"], $menu_id, $menu_name);
        if ($result["default_menu"] == 0) {
            $liens .= '<a id="default_menu_set" onclick="setMenuPrincipal()">Définir comme menu principal</a><br><br>';
        }
        echo $liens;
    }

    public function ajouterLienAction()
    {
        $menu_id = intval($_POST["menu_id"]);
        $link_name = $_POST["link_name"];
        $link_href = $_POST["link_href"];
        $where = ["id"=>$menu_id];
        $menu = new Menu();
        $result = $menu->getOneBy($where);
        $decoded = json_decode($result["contenu"], true);
        $decoded[] = [
            "name" => $link_name,
            "href" => $link_href,
            "children" => []
        ];
        $menu->setId($menu_id);
        $menu->setTitre($result["titre"]);
        $menu->setHref($result["href"]);
        $menu->setContenu(json_encode($decoded));
        $menu->setMenuDefault(intval($result["default_menu"]));
        $menu->save();
        header("location: /menu");
    }

    public function getMenu($result_sql, $menu_id, $menu_name)
    {
        /*$json_data = $this->getJsonMenu($result_sql);
        $menuList = "";*/
        $decoded = json_decode($result_sql, true);
        $list = '<ul id="menu_items_list_head" menu_id="'.$menu_id.'" menu_name="'.$menu_name.'" class="connectedSortable menu_main" >';
        $list .= $this->convertJson($decoded);
        $list .= '</ul>';
        return $list;
    }

    public function convertJson($menu)
    {
        $html = "";
        if (isset($menu)) {
            foreach ($menu as $link) {
                $action = '</br></br><a class="form-submit" href="#edit_link" id="edit_link_btn" rel="modal:open">Modifier le lien</a>';
                $action .= '</br></br><a class="menu-delete" href="#delete_link" id="delete_link_btn" rel="modal:open" onclick="deleteLink(\''.$link["name"].'\',\''.$link["href"].'\')">Supprimer le lien</a></br></br>';
                $html .= '<li data-name="'.$link["name"].'" data-href="'.$link["href"].'">'.$link["name"].$action;
                if (isset($link["children"])) {
                    $html .= '<ul id="menu_items_list" class="connectedSortable">'.$this->convertJson($link["children"]).'</ul>';
                }
                $html .= '</li>';
            }
        } else {
            echo "Le menu est vide veuillez ajouter un lien<br>";
        }
        return $html;
    }

    public function getJsonMenu($data)
    {
        foreach ($data as $row) {
            $menu_elements[$row['id']] = $row;
        }
        // starting with level 0
        $final_menu = $this->add_children($menu_elements);
        // and convert this to json
        return json_encode($final_menu);
    }

    public function add_children($menu_elements)
    {
        $menu_array = array();
        foreach ($menu_elements as $menu_link) {
            $link = array();
            $link['id'] = $menu_link['id'];
            $link['ordre'] = $menu_link['ordre'];
            $link['titre'] = $menu_link['titre'];
            $link['href'] = $menu_link['href'];
            $link['menu_id'] = $menu_link['menu_id'];
            $link['parent_id'] = $menu_link['parent_id'];
            if ($menu_link['parent_id'] == 0) {
                $menu_array[$menu_link['id']] = $link;
            } else {
                $menu_array[$menu_link['parent_id']]["children"][] = $link;
            }
        }
        return $menu_array;
    }
}

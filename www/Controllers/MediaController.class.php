<?php

declare(strict_types=1);

namespace Easyshare\Controllers;

use Easyshare\Core\View;
use Easyshare\Models\Media;
use Easyshare\Core\Routing;
use Easyshare\Core\BaseSQL;
use Easyshare\Core\Form as formUtility;
use Easyshare\Core\Acl;
use Easyshare\Views\modals\form;
use Easyshare\Core\Validator;

class MediaController
{
    private $mediaModel;

    public function __construct(Media $media)
    {
        $this->mediaModel = $media;
    }

    public function defaultAction(): void
    {
        $v = new View("listMedia", "back", "Liste des musique");
        $v->assign("medias", $this->mediaModel->getAll());
        $v->assign("pathForCreate", Routing::getSlug('Media', 'addMedia'));
        $v->assign("pathForDelete", Routing::getSlug('Media', 'deleteMedia'));
    }

    public function addMediaAction(): void
    {
        $v = new View("addMedia", "back", "ajout de musique");
        $v->assign("configFormAddMedia", $this->mediaModel->getFormAddMedia());
    }

    public function saveMediaAction(): void
    {
        //Recupère les informations du formulaire.
        $configForm = $this->mediaModel->getFormAddMedia();
        $method = strtoupper($configForm["config"]["method"]);
        $data = $GLOBALS["_".$method];
        if ($_SERVER["REQUEST_METHOD"]==$method && !empty($_FILES)) {

            //Verifie le fichier uploaded, notament s'il n'est pas null et que son extension soit valide.
            $validatorFile = new Validator($configForm, $_FILES['music'], "upload");
            //Ajoute les erreurs dans la session $_SESSION['form']['error']
            formUtility::exportArrayDataError($validatorFile->errors);

            //Verifie la taille des labels reçu.
            $validatorTextInput = new Validator($configForm, $data, "checkOnlyLength");
            //Ajoute les erreurs dans la session $_SESSION['form']['error']
            formUtility::exportArrayDataError($validatorTextInput->errors);

            //Recupère dynamiquement l'extension du fichier uploaded
            $extension = pathinfo($_FILES['music']['name'], PATHINFO_EXTENSION);
            $labelMusic = $_POST['label'].'.'.$extension;

            //Verifie que qu'il n'a pas d'erreur et que le nom est dispo.
            if (empty($_SESSION['form']['error']) && $this->checkIfNameMediaIsAvaibleAction($labelMusic)) {

                //l'emplacement ou seront enregistrer les medias
                $targetDir = dirname(__DIR__).'/public/music/';
                

                //Le chemin d'accès complet de la musique/media qui sera enregister.
                $targetFile = $targetDir . $labelMusic;
                
                //déplace le fichier temporaire de l'upload dans le nouveau chemin d'accès complet.
                if (move_uploaded_file($_FILES["music"]["tmp_name"], $targetFile)) {

                    //Ajout du media / musique  dans la base de donnée
                    $userId = $this->mediaModel->getUserByEmail($_SESSION['user']['email'])['id'];
                    $this->mediaModel->setUser_id($userId);
                    $this->mediaModel->setLabel($labelMusic);
                    $this->mediaModel->setCateg_media($_POST['category']);
                    $this->mediaModel->setCount_media_played('0');
                    $this->mediaModel->save();

                    //Informe l'utilisateur que l'opération à reussi
                    $_SESSION['form']['info'] = "Votre musique à bien été ajoutée.";
                } else {
                    $_SESSION['form']['error'] = "Une erreur est survenu";
                }
            }
        } else {
            $_SESSION['form']['error'] = array("Veuillez selectioner un fichier ( inferieur à 30 Mo )");
        }

        header('location: '.Routing::getSlug('Media', 'addMedia'));
    }

    public function deleteMediaAction(): void
    {
        if (isset($_GET['media']) & $_GET['media'] != null) {
            $mediaLabel = $_GET['media'] ;

            //recup le chemin d'accès complet pour notre media.
            $pathToTheMedia = dirname(__DIR__).'/public/music/'.$mediaLabel;
            //Supprime le fichier situé dans le serveur.
            if (unlink($pathToTheMedia)) {
                //Supprime le fichier en BDD
                $where = ["label"=>$mediaLabel];
                $this->mediaModel->deleteElementBy(['label'=>$mediaLabel]);
                $_SESSION['form']['info'] = "Votre musique à bien été suprimée.";
            } else {
                $_SESSION['form']['error'] = "Une erreur est survenu";
            }
        } else {
            $_SESSION['form']['error'] = "Une erreur est survenu";
        }
        header('location: '.Routing::getSlug('Media', 'default'));
    }

    //Methode qui return true si aucun media n'a le même label que la varible recu en parametre.
    public function checkIfNameMediaIsAvaibleAction($nameMedia): ?bool
    {
        $where = ['label' => $nameMedia];
        $result = $this->mediaModel->getOneBy($where);
        if ($result['id'] != null) {
            $_SESSION['form']['error'] = "Ce nom de musique est déja utilisé.";
            return false;
        }
        return true;
    }

    public function incraseNbViewOfMediaAction(): ?bool
    {
        $where = ['label' => $_POST['label']];
        $media = $this->mediaModel->getOneBy($where);
        $newNumberOfView = ($media["count_media_played"] == null)? 1 : $media["count_media_played"] + 1 ;
        $this->mediaModel->incraseTheNbViewOfMedia($media['id'], $newNumberOfView);
        return true;
    }
}

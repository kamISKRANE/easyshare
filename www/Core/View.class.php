<?php

declare(strict_types=1);
namespace Easyshare\Core;

//use Easyshare\Views\loginUser;


if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

class View
{
    private $v;
    private $t;
    private $title;
    private $description;
    private $data = [];

    public function __construct($v, $t="back", $title = "", $description = "")
    {
        $this->setView($v);
        $this->setTemplate($t);
        $this->setTitle($title);
        $this->setDescription($description);
    }

    //Se rajoute dans la la balie <meta name="description"> afin d'améliorer le SEO.
    public function setDescription($description)
    {
        $this->description = ($description == "")? '' : $description;
    }

    //Le titre de la page suivi du nom de site si il à bien été fournis.
    public function setTitle($title)
    {
        //defined() : Vérifie l'existence d'une constante.
        if (defined('SITENAME') && SITENAME != "") {
            $this->title = ($title == "")? SITENAME : $title.' - '.SITENAME;
        } else {
            $this->title = ($title == "")? 'Easyshare' : $title.' - Easyshare';
        }
    }
    
    public function setView($v): void
    {
        $pathView = __DIR__."/../Views/".$v.".view.php";
        if (file_exists($pathView)) {
            $this->v = $pathView;
        } else {
            die("La vue n'existe pas :".$pathView);
        }
    }
    public function setTemplate($t): void
    {
        $pathTemplate = __DIR__."/../Views/templates/".$t.".tpl.php";
        if (file_exists($pathTemplate)) {
            $this->t = $pathTemplate;
        } else {
            die("Le template n'existe pas :".$pathTemplate);
        }
    }

    public function addModal($modal, $config): void
    {
        $pathModal = __DIR__."/../Views/modals/".$modal.".mod.php";
        if (file_exists($pathModal)) {
            include $pathModal;
        } else {
            die("Le modal n'existe pas :".$pathModal);
        }
    }

    public function assign($key, $value): void
    {
        $this->data[$key]=$value;
    }

    public function __destruct()
    {
        extract($this->data);
        include $this->t;
    }
}

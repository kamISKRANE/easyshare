<?php
namespace Easyshare\Core;

class Validator
{
    public $errors = [];

    public function __construct($config, $data, $typeForm = "classicForm")
    {
        if ($typeForm == "classicForm") {
            //Verifie que les valeurs que l'utilisateur envoie ne soit pas inférieur au nombre d'inputs préssent dans le formulaire de base.
            if (count($data) < count($config["data"])) {
                header("location: /erreur");
            }

            foreach ($config["data"] as $name => $input) {
                
                //required
                if ($input["required"] && empty($data[$name])) {
                    header("location: /erreur");
                } else {

                    //Minlength
                    if (isset($input["minlength"]) && !self::checkMinLength($data[$name], $input["minlength"])) {
                        $this->errors[]=$input["error"];
                        continue;
                    }
                    //MaxLength
                    if (isset($input["maxlength"]) && !self::checkMaxLength($data[$name], $input["maxlength"])) {
                        $this->errors[]=$input["error"];
                        continue;
                    }
                        
                    //Email
                    if ($input["type"]=="email") {
                        //Verifie si l'eamil est correct
                        if (!self::checkEmail($data[$name])) {
                            $this->errors[]=$input["error"];
                        }
                        continue;
                    }

                    //Birthdate
                    if ($input["type"] =="date" && !self::checkBirthdate($data[$name])) {
                        $this->errors[] = $input["error"];
                        continue;
                    }

                    //Confirm
                    if (isset($input["confirm"]) && $data[$name]!=$data[$input["confirm"]]) {
                        $this->errors[] = $input["error"];
                        continue;
                    }

                    //Pwd
                    if ($input["type"]=="password" && !isset($input["confirm"]) && !self::checkPwd($data[$name])) {
                        $this->errors[] = $input["error"];
                        continue;
                    }
                }
            }
        }

        //Si le formulaire à verifié est un uploadFile.
        elseif ($typeForm == "upload") {
            //Si aucun fichier a été envoyé.
            if (count($data) == null) {
                header("location: /erreur");
            }
            foreach ($config["data"] as $name => $input) {
                
                //Si le Type MIME ou l'extension du fichier ne sont pas accepté alors on affiche une erreur.
                if ($input["type"]=="file" && (!self::checkTypeMIME($config["data"][$name]["extension"], $data["type"]) or !self::checkEndOfFileExtension($config["data"][$name]["extensionEndOfFile"], $data["name"]))) {
                    $this->errors[] = $input["error"];
                    continue;
                }
                if ($input["type"]=="file") {
                }
                if ($input["type"]=="file" && isset($input["maxlength"]) && strlen(basename($data["name"])) > $input["maxlength"]) {
                    $this->errors[] = $input["error"];
                    continue;
                }
            }
        }

        if ($typeForm =="checkOnlyLength") {
            foreach ($config["data"] as $name => $input) {

                //Minlength
                if ($input["type"]=="text" && isset($input["minlength"]) && !self::checkMinLength($data[$name], $input["minlength"])) {
                    $this->errors[]=$input["error"];
                    continue;
                }

                //MaxLength
                if ($input["type"]=="text" && isset($input["maxlength"]) && !self::checkMaxLength($data[$name], $input["maxlength"])) {
                    $this->errors[]=$input["error"];
                    continue;
                }
            }
        }
    }


    public static function checkMinLength($string, $length)
    {
        return (strlen(trim($string))>=$length);
    }
    public static function checkMaxLength($string, $length)
    {
        return (strlen(trim($string))<=$length);
    }
    public static function checkEmail($string)
    {
        return filter_var($string, FILTER_VALIDATE_EMAIL);
    }
    public static function checkPwd($string)
    {
        return (preg_match("#[A-Z]#", $string) &&
                    preg_match("#[a-z]#", $string) &&
                    preg_match("#[0-9]#", $string)
        );
    }

    public static function checkBirthdate($birthdate)
    {
        $yearGivenByUser = date("Y", strtotime($birthdate));
    
        //l'intervale pour l'année de naissance est de [ 16 - 140 ] ans.
        if (($yearGivenByUser <= (date("Y") - 16)) and ($yearGivenByUser >= (date("Y") - 140))) {
            return  true;
        }
        return false;
    }

    //Verifie que le type MIME uploaded correspond aux extension accepté par le formulaire.
    public static function checkTypeMIME($extensionAvailable, $extensionOfTheFile)
    {
        //Supprimer tout les espaces pour eviter les erreur de format.
        $extensionAvailable = str_replace(' ', '', $extensionAvailable);

        //Si il y a un '&', alors il y a plusieurs extensions acceptés, donc on explode().
        if (preg_match("$&$", $extensionAvailable)) {
            //On à pas à declarer $arrayExtension comme tableau avant l'association, car sinon ça cree un tableau à deux dimension.
            $arrayExtension = explode('&', $extensionAvailable);
        } else {
            $arrayExtension[] = $extensionAvailable;
        }
        //Si l'extension du fichier est dans le tableau des type MIME accepté => return true
        if (in_array($extensionOfTheFile, $arrayExtension)) {
            return true;
        }
        return false;
    }

    //Verifie que l'extension du fichier uploaded correspond aux extension accepté par le formulaire.
    public static function checkEndOfFileExtension($endOfFileAvailable, $extensionEndOfTheFile)
    {
        //Supprimer tout les espaces pour eviter les erreur de format.
        $endOfFileAvailable = str_replace(' ', '', $endOfFileAvailable);

        //Si il y a un '&', alors il y a plusieurs extensions acceptés, donc on explode().
        if (preg_match("$&$", $endOfFileAvailable)) {
            //On à pas à declarer $arrayExtension comme tableau avant l'association, car sinon ça cree un tableau à deux dimension.
            $arrayExtensionEndOfFile = explode('&', $endOfFileAvailable);
        } else {
            $arrayExtensionEndOfFile[] = $endOfFileAvailable;
        }

        //recupère la dernière extension du fichier qui à été upload
        $ext = pathinfo($extensionEndOfTheFile, PATHINFO_EXTENSION);

        //Si l'extension du fichier est dans le tableau des extensions de fin de fichié accepté => return true
        if (in_array($ext, $arrayExtensionEndOfFile)) {
            return true;
        }
        return false;
    }
}

<?php
declare(strict_types=1);

namespace Easyshare\Core;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

require_once("public/vendor/PHPMailer/src/PHPMailer.php");
require_once("public/vendor/PHPMailer/src/Exception.php");
require_once("public/vendor/PHPMailer/src/SMTP.php");

class Mail
{
    private $mail;

    public function __construct()
    {
        $this->mail = new PHPMailer();
    }

    public function initMail(): void
    {
        $this->mail->SMTPDebug = 0;                                       // Enable verbose debug output
        $this->mail->isSMTP();                                            // Set mailer to use SMTP
        $this->mail->Host       = 'smtp.mail.yahoo.com'; // Specify main and backup SMTP servers
        $this->mail->SMTPAuth   = true;                                   // Enable SMTP authentication
        $this->mail->Username   = 'easyshare@yahoo.com';                     // SMTP username
        $this->mail->Password   = 'ESGI1234@';                               // SMTP password
        $this->mail->SMTPSecure = 'ssl';                                  // Enable TLS encryption, `ssl` also accepted
        $this->mail->Port       = 465;                                    // TCP port to connect t
        $this->mail->setFrom('easyshare@yahoo.com', 'Easyshare');
    }

    public function registrationMail($sendTo, $key)
    {
        $isSent;
        $this->initMail();
        $message = 'Bienvenue sur EasyShare,

		Pour activer votre compte, veuillez cliquer sur le lien ci dessous
		ou copier/coller dans votre navigateur internet.<br>
		 
		<a href="http://easyshare.site/activation?'.urlencode($sendTo).'&'.urlencode($key).'">Activer mon compte</a><br>
		 
		
		Ceci est un mail automatique, Merci de ne pas y repondre.';

        $this->mail->addAddress($sendTo, 'Saliou');     // Add a recipient

        // Content
        $this->mail->isHTML(true);                                  // Set email format to HTML
        $this->mail->Subject = 'Activation de votre compte';
        $this->mail->Body    = $message;
        
        if ($this->mail->send()) {
            $isSent = true;
        } else {
            $isSent = false;
        }
        return $isSent;
    }

    public function reinitialisationMail($sendTo, $key)
    {
        $isSent;
        $this->initMail();

        $message = 'Bienvenue sur EasyShare,
		 
		Pour réinitialiser votre mot de passe, veuillez cliquer sur le lien ci dessous
		ou copier/coller dans votre navigateur internet.<br>
		 
		<a href="http://easyshare.site/change_mot_de_passe?'.urlencode($sendTo).'&'.urlencode($key).'">Réinitialiser mon mot de passe</a><br>
		 
		
		Ceci est un mail automatique, Merci de ne pas y repondre.';

        $this->mail->addAddress($sendTo, 'Saliou');     			// Add a recipient

        // Content
        $this->mail->isHTML(true);                                  // Set email format to HTML
        $this->mail->Subject = 'Réinitialisation de mot de passe';
        $this->mail->Body    = $message;
        if ($this->mail->send()) {
            $isSent = true;
        } else {
            $isSent = false;
        }
        return $isSent;
    }
}

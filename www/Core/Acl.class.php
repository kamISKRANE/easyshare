<?php

namespace Easyshare\Core;

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

    class Acl
    {
        public static function userConnected()
        {
            if (isset($_SESSION["user"]["id"])) {
                return true;
            }
            return false;
        }

        public static function currentUserIsAdmin()
        {
            if (isset($_SESSION["user"]["role_id"])) {
                return $_SESSION["user"]["role_id"] == 1;
            }
            return false;
        }

        public static function addMessageErrorAcl()
        {
            $_SESSION['acl']['error'] = true;
            $_SESSION['acl']['errorText'] = "Vous n'avez pas les droits suffisants, veuillez vous connecter avec un compte administrateur";
            header('location: '.Routing::getSlug('User', 'login'));
        }

        public static function showMessageErrorAcl()
        {
            if (isset($_SESSION['acl']['error']) && $_SESSION['acl']['error'] == true) {
                echo '<script>alert("Vous n\'avez pas les droits suffisants. \nVeuillez vous connecter avec un compte administrateur");</script>';
                unset($_SESSION['acl']['error']);
            }
            //Affiche aussi le texte au cas ou l'utilisateur a desactiver JavaScript et qu'il ne voit pas l'alert() .
            if (isset($_SESSION['acl']['errorText'])) {
                echo '<div style="background-color:#ffcccc; margin-left:5px; margin-right:5px;margin-top:1px; border-radius:16px"><span style="color:#ff0000; font-size:15px"><b>&nbsp'.$_SESSION['acl']['errorText'].'</b></span></div>';
                unset($_SESSION['acl']['errorText']);
            }
        }
    }

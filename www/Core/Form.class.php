<?php

namespace Easyshare\Core;

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

    class Form
    {
        public static function getElementFromTheForm($form, $firstIndex, $secondIndex = false, $thirdIndex = false)
        {
            if ($secondIndex == false) {
                return $form[$firstIndex];
            } elseif ($thirdIndex == false) {
                return $form[$firstIndex][$secondIndex];
            }
            return $form[$firstIndex][$secondIndex][$thirdIndex];
        }

        public static function tranformDoubeDimensionArrayToAnSingle($array)
        {
            $arraySingleDimension = array();
            foreach ($array as $key => $value) {
                foreach ($value as $key1 => $value1) {
                    array_push($arraySingleDimension, $value1);
                }
            }
            return $arraySingleDimension;
        }

        //Permet de alimenter le formulaire(index : 'value') du model par les données en BDD en croisant leurs index.
        public static function insertDataIntoForm($data, $form)
        {
            foreach ($form['data'] as $name => $elements) {
                foreach ($data as $key => $value) {
                    if (isset($elements['index']) && $elements['index'] == $key) {
                        $form['data'][$name]['value'] = $value;

                        if ($elements['index'] == "label" && $name == "newUrl") {
                            $form['data'][$name]['value'] = preg_replace('#/#', '', $value);
                        }
                    }
                }
            }
            return $form;
        }

        public static function exportArrayDataError($data)
        {
            foreach ($data as $key => $value) {
                $_SESSION['form']['error'][] = $value;
            }
        }

        public static function showFormInformationsAndErrors()
        {
            //String $_SESSION['form']['info']    exemple => (Profil Modifié, ect ...)
            if (isset($_SESSION['form']['info'])) {
                echo '<div style="background-color:#cce5cc; margin-left:5px; margin-right:5px;margin-top:6px; border-radius:16px ;margin-bot:6px;"><p style="color:#008000; font-size:20px ; margin-left:5px"><b>'.$_SESSION['form']['info'].'</b></p></div>' ;
                unset($_SESSION['form']['info']);
            }
            //Array $_SESSION['form']['error']    exemple => (la taille doit être comprise entre 2 et 50)
            if (isset($_SESSION['form']['error'])) {
                
                //Si il y a plusieur erreur alors ce sera un tableau qu'il faut parcourir.
                if (is_array($_SESSION['form']['error'])) {
                    foreach ($_SESSION['form']['error'] as $key => $value) {
                        echo '<div style="background-color:#ffcccc; margin-left:5px; margin-right:5px;margin-top:6px; border-radius:16px"><span style="color:#ff0000; font-size:18px ; margin-left:5px"><b>'.$value.'</b></span></div>';
                    }
                } else {
                    echo '<div style="background-color:#ffcccc; margin-left:5px; margin-right:5px;margin-top:6px; border-radius:16px"><p style="color:#ff0000; font-size:20px ; margin-left:10px"><b>'.$_SESSION['form']['error'].'</b></p></div>';
                }
                unset($_SESSION['form']['error']);
            }
        }
    }

<?php

namespace Easyshare\Core;

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

class Installer
{
    public static function getInfosForm(): ?String
    {
        $step = $_SESSION["etape"];
        echo Routing::getSlugArgs();
        $form = [];
        echo $step;
        
        return $form;
    }

    public static function getDbForm()
    {
        return [
            "config" => [
                    "action" => "installer?2",
                    "method" => "POST",
                    "class" => "form-control",
                    "id" => "",
                    "submit" => "Continuer"
                ],
            "data" => [
                    "title" => [
                            "tag" => "input",
                            "type" => "text",
                            "placeholder" => "Titre du site",
                            "class" => "form-group",
                            "id" => 'title',
                            "name" => "title",
                            "required"=>true,
                            "minlength"=>5,
                            "maxlength"=>250,
                            "error"=>"Votre titre doit faire entre 2 et 250 caractères"
                        ],
                    "db_name" => [
                            "tag" => "input",
                            "type" => "text",
                            "placeholder" => "Nom de la base de données",
                            "class" => "form-group",
                            "id" => "db_name",
                            "name" => "db_name",
                            "required"=>true,
                            "minlength"=>3,
                            "maxlength"=>250
                        ],
                    "db_host" => [
                            "tag" => "input",
                            "type" => "text",
                            "placeholder" => "Nom du Host",
                            "class" => "form-group",
                            "id" => "db_host",
                            "name" => "db_name",
                            "required"=>true,
                            "minlength"=>5,
                            "maxlength"=>250
                        ],
                    "username" => [
                            "tag" => "input",
                            "type" => "text",
                            "placeholder" => "Nom d'utilisateur",
                            "class" => "form-group",
                            "id" => "username",
                            "name" => "username",
                            "required"=>true,
                            "minlength"=>5,
                            "maxlength"=>250
                        ],
                    "password" => [
                            "tag" => "input",
                            "type" => "text",
                            "placeholder" => "Mot de passe",
                            "class" => "form-group",
                            "id" => "password",
                            "name" => "password",
                            "required"=>true,
                            "minlength"=>5,
                            "maxlength"=>250
                        ]
                ]
        ];
    }

    public static function getUserForm()
    {
        return [
            "config" => [
                    "action" => "installer?3",
                    "method" => "POST",
                    "class" => "form-control",
                    "id" => "",
                    "submit" => "Continuer"
                ],
            "data" => [
                    "lastname"=>[
                                "tag"=>"input",
                                "type"=>"text",
                                "placeholder"=>"Votre nom",
                                "message"=> "",
                                "class"=>"form-group",
                                "id"=>"lastname",
                                "name"=>"lastname",
                                "required"=>true,
                                "minlength"=>2,
                                "maxlength"=>100,
                                "error"=>"Votre nom doit faire entre 2 et 100 caractères"
                            ],
                    "firstname"=>[
                                "tag"=>"input",
                                "type"=>"text",
                                "placeholder"=>"Votre prénom",
                                "message"=> "",
                                "class"=>"form-group",
                                "id"=>"firstname",
                                "name"=>"firstname",
                                "required"=>true,
                                "minlength"=>2,
                                "maxlength"=>50,
                                "error"=>"Votre prénom doit faire entre 2 et 50 caractères"
                            ],
                    "email"=>[
                                "tag"=>"input",
                                "type"=>"email",
                                "placeholder"=>"Votre email",
                                "message"=> "",
                                "class"=>"form-group",
                                "id"=>"email",
                                "name"=>"email",
                                "required"=>true,
                                "minlength"=>7,
                                "maxlength"=>250,
                                "error"=>"Votre email est incorrect ou fait plus de 250 caractères"
                            ],
                    "date_naissance"=>[
                                "tag"=>"input",
                                "type"=>"date",
                                "placeholder"=>"Votre date de naissance",
                                "message"=> "",
                                "class"=>"form-group",
                                "id"=>"birth",
                                "name"=>"anniversaire",
                                "required"=>true,
                                "error"=>"Veuillez fournir une année de naissance correcte."
                            ],
                    "password"=>	[
                                "tag"=>"input",
                                "type"=>"password",
                                "placeholder"=>"Votre mot de passe",
                                "message"=> "",
                                "class"=>"form-group",
                                "id"=>"pass",
                                "name"=>"pass",
                                "required"=>true,
                                "minlength"=>6,
                                "error"=>"Votre mot de passe doit faire plus de 6 caractères avec des minuscules, majuscules et des chiffres"
                            ],
                    "re_password"=>[
                                "tag"=>"input",
                                "type"=>"password",
                                "placeholder"=>"Confirmation",
                                "message"=> "",
                                "class"=>"form-group",
                                "id"=>"re_pass",
                                "name"=>"re_pass",
                                "required"=>true,
                                "confirm"=>"password",
                                "error"=>"Le mot de passe de confirmation ne correspond pas"
                            ]
                ]
        ];
    }

    public function checkDB($db_array)
    {
        $_SESSION["title"] = $db_array["title"];
        $_SESSION["db_name"] = $db_array["db_name"];
        $_SESSION["db_host"] = $db_array["db_host"];
        $_SESSION["username"] = $db_array["username"];
        $_SESSION["password"] = $db_array["password"];
        try {
            $pdo = new \PDO("mysql:host=".$_SESSION["db_host"], $_SESSION["username"], $_SESSION["password"]);
            $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            return $pdo;
        } catch (Exception $e) {
            return false;
        }
    }

    public function editConf($data)
    {
        $conf = '<?php
		define("SITENAME","'.$_SESSION["title"].'");
		define("DBNAME", "'.$_SESSION["db_name"].'");
		define("DBHOST", "'.$_SESSION["db_host"].'");
		define("DBUSER", "'.$_SESSION["username"].'");
		define("DBPWD", "'.$_SESSION["password"].'");';

        $conf_file = fopen('conf.inc.php', 'r+');
        fputs($conf_file, $conf);
        fclose($conf_file);
    }

    public function initDB($pdo, $sql)
    {
        $query = $pdo->prepare($sql);
        $query->execute();
    }

    public function setupSite($data)
    {
        try {
            $pdo = new \PDO("mysql:host=".$_SESSION["db_host"].";dbname=".$_SESSION["db_name"], $_SESSION["username"], $_SESSION["password"]);
        } catch (Exception $e) {
            return false;
        }
        $sql = "INSERT INTO `user` (`id`, `role_id`, `firstname`, `lastname`, `email`, `password`, `birth_date`, `active`) VALUES (NULL, '1', '".$data["firstname"]."', '".$data["lastname"]."', '".$data["email"]."', '".password_hash($data["password"], PASSWORD_DEFAULT)."', '".$data["date_naissance"]."', '1')";
        $query = $pdo->prepare($sql);
        $query->execute();
    }

    public function getSQL()
    {
        return "CREATE SCHEMA IF NOT EXISTS ".$_SESSION["db_name"]."  DEFAULT CHARACTER SET utf8 ;
		USE  ".$_SESSION["db_name"]." ;

		CREATE TABLE `comment` (
		  `id` int(11) NOT NULL,
		  `user_id` int(11) NOT NULL,
		  `page_id` int(11) NOT NULL,
		  `content` text NOT NULL,
		  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
		  `update_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;

		CREATE TABLE `media` (
		  `id` int(11) NOT NULL,
		  `user_id` int(11) DEFAULT NULL,
		  `label` varchar(250) DEFAULT NULL,
		  `categ_media` varchar(250) DEFAULT NULL,
		  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
		  `update_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
		  `count_media_played` int(11) DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;

		CREATE TABLE `menu` (
		  `id` int(11) NOT NULL,
		  `titre` varchar(20) NOT NULL,
		  `href` varchar(20) NOT NULL,
		  `contenu` text,
		  `default_menu` tinyint(1) NOT NULL DEFAULT '0'
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;

		INSERT INTO `menu` (`id`, `titre`, `href`, `contenu`, `default_menu`) VALUES
		(1, 'Default menu', '/', '', 1);

		CREATE TABLE `page` (
		  `id` int(11) NOT NULL,
		  `label` varchar(200) DEFAULT NULL,
		  `title` varchar(250) DEFAULT NULL,
		  `content` text,
		  `available` tinyint(1) DEFAULT NULL,
		  `comment` tinyint(1) DEFAULT NULL,
		  `homepage` tinyint(1) DEFAULT NULL,
		  `description` text,
		  `count_views` int(11) DEFAULT NULL,
		  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
		  `update_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;

		CREATE TABLE `role` (
		  `id` int(11) NOT NULL,
		  `role_name` varchar(45) DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;

		INSERT INTO `role` (`id`, `role_name`) VALUES
		(1, 'Administrateur'),
		(2, 'Utilisateur');

		CREATE TABLE `user` (
		  `id` int(11) NOT NULL,
		  `role_id` int(11) NOT NULL,
		  `firstname` varchar(45) NOT NULL,
		  `lastname` varchar(45) NOT NULL,
		  `email` varchar(45) NOT NULL,
		  `password` varchar(60) NOT NULL,
		  `birth_date` date NOT NULL,
		  `active` tinyint(1) DEFAULT NULL,
		  `token` varchar(255) DEFAULT NULL,
		  `disabled` tinyint(1) DEFAULT NULL,
		  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
		  `update_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=' ';

		ALTER TABLE `comment`
		  ADD PRIMARY KEY (`id`),
		  ADD KEY `fk_comment_user1_idx` (`user_id`),
		  ADD KEY `fk_comment_page1_idx` (`page_id`);

		ALTER TABLE `media`
		  ADD PRIMARY KEY (`id`),
		  ADD KEY `fk_media_user1_idx` (`user_id`);

		ALTER TABLE `menu`
		  ADD PRIMARY KEY (`id`);

		ALTER TABLE `page`
		  ADD PRIMARY KEY (`id`);

		ALTER TABLE `role`
		  ADD PRIMARY KEY (`id`);

		ALTER TABLE `user`
		  ADD PRIMARY KEY (`id`),
		  ADD KEY `fk_users_role1_idx` (`role_id`);

		ALTER TABLE `comment`
		  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

		ALTER TABLE `media`
		  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

		ALTER TABLE `page`
		  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

		ALTER TABLE `role`
		  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

		ALTER TABLE `user`
		  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

		ALTER TABLE `comment`
		  ADD CONSTRAINT `fk_comment_page1` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
		  ADD CONSTRAINT `fk_comment_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

		ALTER TABLE `media`
		  ADD CONSTRAINT `fk_media_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

		ALTER TABLE `user`
		  ADD CONSTRAINT `fk_user_role1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

		";
    }
}

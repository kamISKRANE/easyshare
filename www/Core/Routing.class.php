<?php
 
namespace Easyshare\Core;

use Easyshare\Core\Acl;

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

class Routing
{
    public static $routeFile = "routes.yml";

    public static function getRoute($slug)
    {

        //Récupération de toutes les routes se trouvant dans notre yml
        $routes = yaml_parse_file(self::$routeFile);
        //Est ce que la route existe
        if (!empty($routes[$slug])) {

            //On vérifie qu'il n'y a pas une mauvaise écrite des routes dans le fichier
            if (empty($routes[$slug]["controller"]) || empty($routes[$slug]["action"])) {
                die("Il y a une erreur dans le routes.yml");
            }
            if (isset($routes[$slug]["permission"])) {
                if ($routes[$slug]["permission"] == "admin") {
                    if (Acl::currentUserIsAdmin() == false) {
                        //Si l'action demander require un droit Admin et que l'user ne la pas. => Redirection vers l'acceuil.
                        die(Acl::addMessageErrorAcl());
                    }
                } elseif ($routes[$slug]["permission"] == "connected") {
                    if (Acl::userConnected() == false) {
                        Acl::addMessageErrorAcl();
                    }
                }
            }
            //Ajout des suffixes
            $controller = '\\Easyshare\\Controllers\\'.ucfirst($routes[$slug]["controller"])."Controller";
            $action = $routes[$slug]["action"]."Action";
        } else {
            //Aucune route ne correspond
            return null;
        }

        return ["controller"=>$controller,"action"=>$action];
    }



    public static function getSlug($controller=null, $action=null)
    {
        $routes = yaml_parse_file(self::$routeFile);

        foreach ($routes as $slug => $controllerAndAction) {
            if (!empty($controllerAndAction["controller"]) &&
                !empty($controllerAndAction["action"]) &&
                $controllerAndAction["controller"] == $controller &&
                $controllerAndAction["action"] == $action) {
                return $slug;
            }
        }
        return null;
    }
    public static function getSlugUrl()
    {
        $slug = $_SERVER["REQUEST_URI"];
        $slugExploded = explode("?", $slug);
        return $slugExploded[0];
    }

    public static function getSlugArgs()
    {
        $slug = $_SERVER["REQUEST_URI"];
        $slugExploded = explode("?", $slug);
        return isset($slugExploded[1])?$slugExploded[1]:null;
    }
}

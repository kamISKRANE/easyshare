<?php 

declare(strict_types=1);

namespace Easyshare\Core\OAuth2;

use Easyshare\Core\OAuth2\ProviderInterface;

if (session_status() == PHP_SESSION_NONE) 
{
        session_start();
}

class OauthGoogle implements ProviderInterface {

    private $client_id; 
    private $client_secret;
    private $code;
    private $token;


    public function __construct(){
        $this->endpoint = "https://accounts.google.com/o/oauth2/";
        $this->client_id ="391186458705-gujh2sbe7nsltb7pklqp9qf35hcq6834.apps.googleusercontent.com"; 
        $this->client_secret = "JhgDAgOXcvnvYGGKTX9ahGyu";
        //$this->url = $this->endpoint.'?client_id='.$this->client_id.'&client_secret='.$this->client_secret;
    }

    public function setCode($code): void
    {
        $this->code = $code;
    }

     /**
     * Return the provider name
     * @example google, facebook, ...
     */
    public function getProviderName(): string
    {
        return 'google';
    }
    
    /**
     * Return the authorization endpoint
     * @example http://auth-server/auth
     */
    public function getAuthorizationUrl(): string
    {
        return $this->endpoint.'v2/auth?scope=https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email&redirect_uri=http://localhost:82/loginGoogle&response_type=code&client_id='.$this->client_id;
    }
    
    /**
     * Return the access token endpoint
     * @example http://auth-server/token
     */
    public function getAccessTokenUrl(): string
    {
        return $this->endpoint.'token';
    }

    public function setToken($token): void
    {
        $this->token = $token;
    }

    public function getParams(): array{
        return $params = array(
            "code" => $this->code,
            "client_id" => $this->client_id,
            "client_secret" => $this->client_secret,
            "redirect_uri" => 'http://localhost:82/loginGoogle',
            "grant_type" => "authorization_code"
        );
    }
    
    /**
     * Return the user information
     * @example [
     *  "id" => "123456",
     *  "name" => "John Smith",
     *  "email" => "john.smith@domain.com"
     * ]
     */
    public function getUsersInfo():array
    {
        $data = file_get_contents('https://www.googleapis.com/oauth2/v2/userinfo?access_token=' .$this->token);
        $arr = json_decode($data, true);
        //var_dump($arr);
        return $arr;
    }

}


/*

    // try to get an access token
    //$code = $_GET['code'];
    //echo $code;
    $url = 'https://accounts.google.com/o/oauth2/token';
    $params = array(
        "code" => $code,
        "client_id" => '391186458705-gujh2sbe7nsltb7pklqp9qf35hcq6834.apps.googleusercontent.com',
        "client_secret" => 'JhgDAgOXcvnvYGGKTX9ahGyu',
        "redirect_uri" => 'http://localhost:82/loginGoogle',
        "grant_type" => "authorization_code"
    );

    $ch = curl_init();
    curl_setopt($ch, constant("CURLOPT_" . 'URL'), $url);
    curl_setopt($ch, constant("CURLOPT_" . 'POST'), true);
    curl_setopt($ch, constant("CURLOPT_" . 'POSTFIELDS'), $params);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $output = curl_exec($ch);
    $info = curl_getinfo($ch);
    curl_close($ch);
    "<br><br>";
    //echo $output;
    $age = $output;
    $arr = json_decode($age, true);
    $obj = json_decode($age);
    @$obj->access_token."<br/>";
    @$access_token = $obj->access_token;
    //echo $access_token;

    @$data = file_get_contents('https://www.googleapis.com/oauth2/v2/userinfo?access_token=' .$access_token);
    //echo $data;

    $age = $data;
    $arr = json_decode($age, true);
    $obj = json_decode($age);
    @$googleid = $obj->id;
    @$googlelast_name = $obj->given_name;
    @$googlename = $obj->family_name;
    @$googleemail = $obj->email;
    @$googleimage = $obj->picture;


    //echo $googlename;
    $_SESSION['AUTHGOOGLE']=array();
    $_SESSION['AUTHGOOGLE']['id'] = $googleid;
    $_SESSION['AUTHGOOGLE']['firstname'] = $googlename;
    $_SESSION['AUTHGOOGLE']['lastname'] = $googlelast_name;
    $_SESSION['AUTHGOOGLE']['birthday'] = "24/10/1997";
    $_SESSION['AUTHGOOGLE']['email'] = $googleemail;
    $_SESSION['AUTHGOOGLE']['mdp'] = $googleid;
    //print_r($_SESSION['AUTHGOOGLE']);
    ?>*/
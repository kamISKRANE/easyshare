<?php

declare(strict_types=1);

namespace Easyshare\Core\OAuth2;

use Easyshare\Core\OAuth2\ProviderInterface;

if (session_status() == PHP_SESSION_NONE) 
{
        session_start();
}


class OauthSpotify implements ProviderInterface {
	 private $code = array();
	 private $endpoint;
	 private $client_id;
	 private $client_secret;
	 private $token;
	 private $url;

	public function __construct(){
		$this->endpoint = "https://accounts.spotify.com/authorize";
		$this->client_id = "152bd6e0408a46af9655a290f53a3ed3"; 
		$this->client_secret = "b0206e4d35844d34b25ee6399bc17e3f";
		//$this->url = $this->endpoint.'?client_id='.$this->client_id.'&client_secret='.$this->client_secret;
	}

	public function setCode($code) : void
	{
		$this->code[] = $code;
	}

	public function getCode(): array
	{
		return $this->code;
	}

	public function setToken($token): void
	{
		$this->token = $token;
	}

	public function getToken(): string
	{
		return $this->token;
	}

	 /**
     * Return the provider name
     * @example google, facebook, ...
     */
    public function getProviderName(): string
    {
    	return 'facebook';
    }
    
    /**
     * Return the authorization endpoint
     * @example http://auth-server/auth
     */
    public function getAuthorizationUrl(): string
    {
    	return $this->endpoint.'dialog/oauth?client_id='.$this->client_id.'&client_secret='.$this->client_secret.'&redirect_uri=http://localhost:82/loginFacebookCode';
    }

    /**
     * Return the access token endpoint
     * @example http://auth-server/token
     */
    public function getAccessTokenUrl(): string
    {	
    	return 'https://graph.facebook.com/v3.3/oauth/access_token?client_id='.$this->client_id.'&client_secret='.$this->client_secret.'&code='.$this->code[0].'&redirect_uri=http://localhost:82/loginFacebookCode';
    }

    /**
     * Return the user information
     * @example [
     *  "id" => "123456",
     *  "name" => "John Smith",
     *  "email" => "john.smith@domain.com"
     * ]
     */
    public function getUsersInfo():array
    {
		@$fba = file_get_contents($endpoint.$user_id.'?fields=id,last_name,birthday,first_name,email&access_token=' .$token);
		$age = $fba;
		$arr = json_decode($age, true);
		$obj = json_decode($age);
		$user = array();
		$user['id'] = $obj->id;
		$user['firstname'] = $obj->first_name;
		$user['lastname'] = $obj->last_name;
		$user['birthday'] = $obj->birthday;
		$user['email'] = $obj->email;

		return $user;
    }
}
<?php

declare(strict_types=1);

namespace Easyshare\Core\OAuth2;

use Easyshare\Core\OAuth2\ProviderInterface;

if (session_status() == PHP_SESSION_NONE) 
{
        session_start();
}


class OauthFacebook implements ProviderInterface {
	 private $code = array();
	 private $endpoint;
	 private $client_id;
	 private $client_secret;
	 private $token;
	 private $url;
	 private $appToken;

	public function __construct(){
		$this->endpoint = "https://www.facebook.com/v3.3/";
		$this->client_id = "506377843464609"; 
		$this->client_secret = "4784fe37e23481d68c8370cf659d1582";
		$this->appToken = "506377843464609|9V_oDve4leF3jkBRkVEiihwygL0";
		//$this->url = $this->endpoint.'?client_id='.$this->client_id.'&client_secret='.$this->client_secret;
	}

	public function setCode($code) : void
	{
		$this->code[] = $code;
	}

	public function getCode(): array
	{
		return $this->code;
	}

	public function setToken($token): void
	{
		$this->token = $token;
	}

	public function getToken(): string
	{
		return $this->token;
	}

	 /**
     * Return the provider name
     * @example google, facebook, ...
     */
    public function getProviderName(): string
    {
    	return 'facebook';
    }
    
    /**
     * Return the authorization endpoint
     * @example http://auth-server/auth
     */
    public function getAuthorizationUrl(): string
    {
    	return $this->endpoint.'dialog/oauth?client_id='.$this->client_id.'&client_secret='.$this->client_secret.'&redirect_uri=http://localhost:82/loginFacebookCode';
    }

    /**
     * Return the access token endpoint
     * @example http://auth-server/token
     */
    public function getAccessTokenUrl(): string
    {	
    	return 'https://graph.facebook.com/v3.3/oauth/access_token?client_id='.$this->client_id.'&client_secret='.$this->client_secret.'&code='.$this->code[0].'&redirect_uri=http://localhost:82/loginFacebookCode';
    }

    /**
     * Return the user information
     * @example [
     *  "id" => "123456",
     *  "name" => "John Smith",
     *  "email" => "john.smith@domain.com"
     * ]
     */
    public function getUsersInfo():array
    {
		@$fba = file_get_contents($endpoint.$user_id.'?fields=id,last_name,birthday,first_name,email&access_token=' .$token);
		$age = $fba;
		$arr = json_decode($age, true);
		$obj = json_decode($age);
		$user = array();
		$user['id'] = $obj->id;
		$user['firstname'] = $obj->first_name;
		$user['lastname'] = $obj->last_name;
		$user['birthday'] = $obj->birthday;
		$user['email'] = $obj->email;

		return $user;
    }

/*
<?php //Récupère le code dans l'URL
$endpoint = "https://graph.facebook.com/";
@$code = $_GET['code'];
$client_id = "1278389162338858"; 
$client_secret = "38bd7ebfa2985eab29e205ae8ec8eeec";
$at="1278389162338858|MIsZlPJ_nQgfSAa-SdfKYvDVsIA";
?>

<?php // Retourne le token
@$token = file_get_contents($endpoint. 'v3.3/oauth/access_token?client_id=' .$client_id.'&redirect_uri=http://localhost:82/loginFacebook&client_secret=' .$client_secret.'&code=' .$code);
$age = $token;
$arr = json_decode($age, true);
$obj = json_decode($age);
@$obj->access_token."<br/>";

// Accès aux données de l'application grace au token
@$info = file_get_contents($endpoint. 'debug_token?input_token=' .$obj->access_token.'&access_token=' .$at);
$arr = json_decode($info, true);
$user_id = $arr['data']['user_id'];
$user_id;"<br/>"; 

// Récupère les data de l'utilisateur
@$fba = file_get_contents($endpoint.$user_id.'?fields=id,last_name,birthday,first_name,email&access_token=' .$at);
$age = $fba;
$arr = json_decode($age, true);
$obj = json_decode($age);
@$fbid = $obj->id;
@$fblast_name = $obj->last_name;
@$fbname = $obj->first_name;
@$fbbirthday = $obj->birthday;
@$fbemail = $obj->email;

$_SESSION['AUTHFB']=array();
$_SESSION['AUTHFB']['id'] = $fbid;
$_SESSION['AUTHFB']['firstname'] = $fbname;
$_SESSION['AUTHFB']['lastname'] = $fblast_name;
$_SESSION['AUTHFB']['birthday'] = "24/10/1997";
$_SESSION['AUTHFB']['email'] = $fbemail;
$_SESSION['AUTHFB']['mdp'] = $fbid;

?>*/

}
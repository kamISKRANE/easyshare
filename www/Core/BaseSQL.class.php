<?php

//declare(strict_types=1);
namespace Easyshare\Core;

class BaseSQL
{
    private $pdo;
    private $table;

    public function __construct()
    {
        $this->pdo = $this->getContructPdo();
        $this->table = $this->getContructTable();
    }

    public function getContructPdo()
    {
        try {
            return new \PDO("mysql:host=".DBHOST.";dbname=".DBNAME, DBUSER, DBPWD);
        } catch (Exception $e) {
            die(" Erreur SQL : ".$e->getMessage());
        }
    }

    public function getContructTable()
    {
        $calledClassNamespace = explode('\\', get_called_class());
        $calledClass = end($calledClassNamespace);
        return strtolower($calledClass);
    }

    public function getColumns()
    {
        $objectVars = get_object_vars($this);
        $classVars = get_class_vars(get_class());
        $columns = array_diff_key($objectVars, $classVars);
        return $columns;
    }

    //Dynamique en fonction de l'enfant qui en hérite
    public function save()
    {
        $columns = $this->getColumns();

        if (is_null($columns["id"])) {
            //INSERT
            
            $sql = "INSERT INTO ".$this->table." (".implode(",", array_keys($columns)).") VALUES (:".implode(",:", array_keys($columns)).")";
            $query = $this->pdo->prepare($sql);
            $query->execute($columns);
        } else {
            //UPDATE
            
            foreach ($columns as $key => $value) {
                $sqlSet[] = $key."=:".$key;
            }

            $sql = "UPDATE ".$this->table." SET ".implode(",", $sqlSet)." WHERE id=:id";
            
            $query = $this->pdo->prepare($sql);
            $query->execute($columns);
        }
    }

    public function delete($where)
    {
        $sql = "DELETE FROM ".$this->table." WHERE id='".$where."';";
        $query = $this->pdo->prepare($sql);
        $query->execute();
    }

    public function getAll($target = false)
    {
        $sql = "SELECT * FROM ".$this->table;
        if ($target != false) {
            $sql = "SELECT ".$target." FROM ".$this->table;
        }
        $query = $this->pdo->prepare($sql);
        $query->execute();
        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getAllUserAvailable($target = false)
    {
        $sql = "SELECT * FROM user WHERE disabled IS NULL ";
        if ($target != false) {
            $sql = "SELECT ".$target." FROM user WHERE disabled IS NULL ";
        }
        $query = $this->pdo->prepare($sql);
        $query->execute();
        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getUser($id)
    {
        $sql = "SELECT * FROM user WHERE id =".$id;
        $query = $this->pdo->prepare($sql);
        $query->execute();
        return $query->fetch(\PDO::FETCH_ASSOC);
    }

    public function getUserByEmail($email)
    {
        $sql = "SELECT * FROM user WHERE email ='".$email."';";
        $query = $this->pdo->prepare($sql);
        $query->execute();
        return $query->fetch(\PDO::FETCH_ASSOC);
    }

    //Example : array $where=["id"=>1, "email"=>"te.st@test.fr"]
    public function getOneBy($where, $object = false)
    {
        foreach ($where as $key => $value) {
            $sqlWhere[] = $key."=:".$key;
        }
        $sql = "SELECT * FROM ".$this->table." 
				WHERE ".implode(" AND ", $sqlWhere).";";
                
        $query = $this->pdo->prepare($sql);
        if ($object) {
            $query->setFetchMode(\PDO::FETCH_INTO, $this);
        } else {
            $query->setFetchMode(\PDO::FETCH_ASSOC);
        }
        $query->execute($where);
        return $query->fetch();
    }

    //Same function than getOneBy but return all elements
    public function getAllBy($where, $object = false)
    {
        foreach ($where as $key => $value) {
            $sqlWhere[] = $key."=:".$key;
        }
        $sql = "SELECT * FROM ".$this->table." 
				WHERE ".implode(" AND ", $sqlWhere).";";
        $query = $this->pdo->prepare($sql);
        if ($object) {
            $query->setFetchMode(\PDO::FETCH_INTO, $this);
        } else {
            $query->setFetchMode(\PDO::FETCH_ASSOC);
        }
        $query->execute($where);
        return $query->fetchAll();
    }

    public function deleteElementBy($where)
    {
        foreach ($where as $key => $value) {
            $sqlWhere[] = $key."=:".$key;
        }
        $sql = "DELETE FROM ".$this->table."
				WHERE ".implode(" AND ", $sqlWhere).";";
        $query = $this->pdo->prepare($sql);
        $query->execute($where);
    }

    public function disabledElementBy($where)
    {
        foreach ($where as $key => $value) {
            $sqlWhere[] = $key."=:".$key;
        }
        $sql = "UPDATE ".$this->table." SET disabled = 1 
				WHERE ".implode(" AND ", $sqlWhere).";";
        $query = $this->pdo->prepare($sql);
        $query->execute($where);
    }

    public function deletePage($label)
    {
        $sql = "DELETE FROM page where label = :label ;";

        $query = $this->pdo->prepare($sql);
        $query->execute(array(':label'=> $label));
    }

    public function getComments($page_id)
    {
        $sql = "SELECT * FROM comment where page_id = :page_id ORDER BY create_date";
        $query = $this->pdo->prepare($sql);
        $query->execute([':page_id'=>$page_id]);
        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function depublierComment($id)
    {
        $sql = "DELETE FROM comment WHERE id=:id";
        $query = $this->pdo->prepare($sql);
        $query->execute(array(':id'=>$id));
    }

    public function getAllMenu()
    {
        $sql = "SELECT * FROM menu";
        $query = $this->pdo->prepare($sql);
        $query->execute();
        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function deleteMenu($id)
    {
        $sql = "DELETE FROM menu where id = :id ;";
        $query = $this->pdo->prepare($sql);
        $query->execute(array(':id'=>$id));
    }

    public function setDefaultMenu($id)
    {
        $sql = "UPDATE menu SET default_menu = CASE WHEN id = :id THEN 1 ELSE 0 END";
        $query = $this->pdo->prepare($sql);
        $query->execute(array(':id' => $id));
    }

    public function getDefaultMenu()
    {
        $sql = "SELECT * FROM menu WHERE default_menu = 1";
        $query = $this->pdo->prepare($sql);
        $query->execute();
        return $query->fetch(\PDO::FETCH_ASSOC);
    }

    public function incraseViewsOfPage($idPage, $nbViews)
    {
        $sql = "UPDATE page SET count_views = :nbViews WHERE id = :idPage";
        $query = $this->pdo->prepare($sql);
        $query->execute(array(	':nbViews'=> $nbViews,
                                ':idPage'=>$idPage));
    }

    public function updateAclUser($upgrade = false, $idUser)
    {
        $sql = 'UPDATE user SET role_id = 2 WHERE id = :idUser';
        if ($upgrade == true) {
            $sql = 'UPDATE user SET role_id = 1 WHERE id = :idUser';
        }
        $query = $this->pdo->prepare($sql);
        $query->execute(array(	':idUser'=> $idUser));
    }

    public function disableAllHomepage()
    {
        $sql = "UPDATE page SET homepage = NULL ";
        $query = $this->pdo->prepare($sql);
        $query->execute();
    }

    public function getNbUser()
    {
        $sql = "SELECT COUNT(id) as nbUser from user" ;
        $query = $this->pdo->prepare($sql);
        $query->execute();
        return $query->fetch(\PDO::FETCH_ASSOC);
    }

    public function getNbPageView()
    {
        $sql = "SELECT SUM(count_views) as nbPageView from page";
        $query = $this->pdo->prepare($sql);
        $query->execute();
        return $query->fetch(\PDO::FETCH_ASSOC);
    }

    public function getNbMusic()
    {
        $sql = "SELECT COUNT(id) as nbMusic from media";
        $query = $this->pdo->prepare($sql);
        $query->execute();
        return $query->fetch(\PDO::FETCH_ASSOC);
    }

    public function getNbComment()
    {
        $sql = "SELECT COUNT(id) as nbComment from comment";
        $query = $this->pdo->prepare($sql);
        $query->execute();
        return $query->fetch(\PDO::FETCH_ASSOC);
    }

    public function getNbCommentToday()
    {
        $sql = "SELECT COUNT(id) as nbComment from comment where DATE(create_date)=CURDATE()";
        $query = $this->pdo->prepare($sql);
        $query->execute();
        return $query->fetch(\PDO::FETCH_ASSOC);
    }

    public function getStatMusic()
    {
        $sql = "SELECT id as idMusic, label as titleMusic, count_media_played as countView from media order by  count_media_played desc limit 5;";
        $query = $this->pdo->prepare($sql);
        $query->execute();
        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getStatCategory()
    {
        $sql = "SELECT categ_media as category, count(categ_media) as count_category from media group by categ_media limit 5;";
        $query = $this->pdo->prepare($sql);
        $query->execute();
        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getStatPage()
    {
        $sql = "SELECT title as titlePage, count_views as nbViewsPage from page limit 5;";
        $query = $this->pdo->prepare($sql);
        $query->execute();
        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function incraseTheNbViewOfMedia($idMedia, $count_media_played)
    {
        $sql = "UPDATE media SET count_media_played = :count_media_played WHERE id = :idMedia";
        $query = $this->pdo->prepare($sql);
        $query->execute(array(	':count_media_played'=> $count_media_played,
                                ':idMedia'=>$idMedia));
    }

    public function getCategoryBylabel($email)
    {
        $sql = "SELECT * FROM user WHERE email ='".$email."';";
        $query = $this->pdo->prepare($sql);
        $query->execute();
        return $query->fetch(\PDO::FETCH_ASSOC);
    }
}

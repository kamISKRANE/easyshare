<?php
//Contient toutes les constantes de notre projet

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

use Easyshare\Core\Routing;
use Easyshare\Core\Installer;
use Easyshare\Core\View;

ini_set('display_errors', 'on');
require "conf.inc.php";

//AutoLoader qui respect la norme PSR4
spl_autoload_register(function ($class) {
    $prefix = 'Easyshare';
    $base_dir = __DIR__ . '/';
    $len = strlen($prefix);

    if (strncmp($prefix, $class, $len) !== 0) {
        // no, move to the next registered autoloader
        return;
    }
    
    $relative_class = substr($class, $len);
    $file = $base_dir . str_replace('\\', '/', $relative_class) . '.class.php';
    if (file_exists($file)) {
        require $file;
    }
});

$slug = Routing::getSlugUrl();

//Récupère la route correspondant au slug
$route = Routing::getRoute($slug);
if (is_null($route)) {
    //On va faire appelle a la methode adminPage pour verifier
    //si la page existe en BDD
    $route = Routing::getRoute('/page_admin');
}
if (!defined('DBNAME')) {
    $_SESSION["etape"] = 1;
    $form = null;
    switch (Routing::getSlugArgs()) {
        case null:
            $form = Installer::getDbForm();
            break;
        case 2:
            $pdo = Installer::checkDB($_POST);
            if ($pdo) {
                //Installer::editConf($_POST);
                $sql = Installer::getSQL();
                Installer::initDB($pdo, $sql);
                $form = Installer::getUserForm();
            } else {
                $_SESSION['form']['error'] = "test";
                header("localtion: /");
            }
            break;
        case 3:
                Installer::setupSite($_POST);
                Installer::editConf($_POST);
                header("location: /");
                break;
        default:
    }
    $v = new View("installer", "login");
    $v->assign("installForm", $form);
} else {
    //Transforme un tableau en une multitude de variable prenant pour nom les clés
    extract($route);

    //Notre container de dependance.
    $container = require 'config/di.global.php';

    //Vérifier que le fichier et la class controller existe
    if (class_exists($controller)) {
        //Instance dynamique du controller
        $controllerObject = $container[$controller]($container);
        //Vérification de l'existance de la méthode
        if (method_exists($controllerObject, $action)) {
            //Appel de la méthode
            $controllerObject->$action();
        } else {
            die("L'action ".$action." n'existe pas");
        }
    } else {
        die("La class ".$controller." n'existe pas");
    }
}

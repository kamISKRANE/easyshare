var lienAModifier;

$(document).ready(function () {
    $("#menu_list").selectable({
      selected: function(event, ui){
        var menu_id = $(ui.selected).attr("menu_id");
        var menu_name = $(ui.selected).attr("menu_name");
        $.ajax({
            type: 'POST',
            url: 'afficher_liens_menu',
            data: {'menu_id':menu_id,'menu_name':menu_name},
            success: function(response){
                var options = "</br></br><a class='form-submit' name='test' id='menu_save_btn' onclick='getMenus()'>Enregistrer le menu</a></br></br>";
                options += '<a href="#ajout_lien" class="form-submit" rel="modal:open">Ajouter un lien</a></br></br>';
                options += '<a href="#edit_menu" class="form-submit" rel="modal:open">Modifier le menu</a></br></br>';
                options += '<a href="#delete_menu" class="form-submit" rel="modal:open">Supprimer le menu</a>';
                options += '<script>$("#menu_items_list_head,#menu_items_list").sortable({connectWith: ".connectedSortable"}).disableSelection();</script>';
                response += options;
                //document.getElementById("#menu_list_items").innerHTML = response;
                $("#menu_list_items").html(response);
            }
        }); 
      }
    });
    $("#menu_list_items").on("click","#edit_link_btn",function(){
        var name = $(this).parent().attr("data-name");
        var href = $(this).parent().attr("data-href");
        lienAModifier = $(this).parent();
        //updateLink($(this).parent());
    });
});

    function getMenuId(){
        var menu_id = $("#menu_items_list_head").attr("menu_id");
        var link_name = $("#link_name_input").val();
        var link_href = $("#link_href_input").val();
        console.log(link_name);
        $.ajax({
            type: 'POST',
            url: 'ajouter_lien',
            data: {'menu_id':menu_id, 'link_name':link_name,'link_href':link_href},
            success: function(response){
              location.reload();
            }
        });
    }

    function addLink(){
        var content = '<input id="link_name_input" type="textarea" placeholder="Titre du lien" name="menu_name">';
        content += '<input id="link_href_input" type="textarea" placeholder="href du lien" name="menu_href">';
        content += '<input type="submit" name="link_add" id="add_link_item_btn" value="Ajouter" onclick="getMenuId()">';
        $("#add_link_section").html(content);setMenuPrincipal
    }

    function getMenus(){
        var result = [];
        var data = getList($('#menu_items_list_head'),result,0);
        var json_upload = JSON.stringify(data);
        var menu_id = $("#menu_items_list_head").attr("menu_id");
        var menu_name = $("#menu_items_list_head").attr("menu_name");
        $.ajax({
            type: 'POST',
            url: 'mise_a_jour_menu',
            data: {'menu':json_upload,'menu_id':menu_id, 'menu_name':menu_name},
            success: function(response){
                location.reload();
            }

        }); 
    }

    function setMenuPrincipal(){
        var menu_id = $("#menu_items_list_head").attr("menu_id");
        $.ajax({
            type: 'POST',
            url: 'set_default_menu',
            data: {'menu_id':menu_id},
            success: function(response){
                location.reload();
            }

        }); 
    }

    function updateMenu(){
        var menu_id = $("#menu_items_list_head").attr("menu_id");
        var menu_name = $("#menu_name_edit").val();
        $.ajax({
            type: 'POST',
            url: 'mise_a_jour_menu',
            data: {'menu_id':menu_id, 'menu_name_edit':menu_name},
            success: function(response){
                //console.log(response);
                location.reload();
            }
        }); 
    }

    function deleteMenu(){
        var menu_id = $("#menu_items_list_head").attr("menu_id");
        $.ajax({
            type: 'POST',
            url: 'delete_menu',
            data: {'menu_id':menu_id},
            success: function(response){
                location.reload();
            }
        }); 
    }

    var lienASupprimer;

    function deleteLink(name, href){
        lienASupprimer = getElements($("#menu_items_list_head"),name);  
    }

    function getElements(head, name){
        var object;
        $.each($(head).children('li'), function() {
            //console.log($(this).attr("data-name"));
            //console.log(name);
            if($(this).attr("data-name") == name){
                object = $(this);
            }
            else if($(this).has('ul')) {
                object = getElements($(this).children('ul'),name);        
            }
        });
        return object;
    }

    function deleteLinkSubmit(node){
        $(lienASupprimer).remove();
    }

    function infoForUpdatelink(){
        $("#link_name_edit").val("test");
    }



    function updateLink(node){
        var link_name = $("#link_name_edit").val();
        var link_href = $("#link_href_edit").val();
        $(lienAModifier).attr({"data-name":link_name, "data-href":link_href});
        getMenus();
    }
    
    function getList(element, json_object, parent_id) {
        $.each($(element).children('li'), function() {
            var node = {
                id: $(this).attr("data-id"),
                name: $(this).attr("data-name"),
                href: $(this).attr("data-href"),
            };
            if($(this).has('ul')) {
                node.children = []
                getList($(this).children('ul'), node.children, $(this).attr("data-id"));        
            }
            json_object.push(node);
        });
    return json_object;
  }
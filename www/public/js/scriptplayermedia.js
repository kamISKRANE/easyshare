$(document).ready(function() {

  var song;
  var tracker = $('.tracker');


  function addViewToTheMusic(label){
    $.ajax({
        type: 'POST',
        url: 'augmentation_des_lectures_des_musiques',
        data: {'label':label} ,
    }); 
  }

  function initAudio(elem) {
    var url = elem.attr('audiourl');

    var title = elem.text();

    $('.player .title').text(title);

    // song = new Audio('media/'+url);
    song = new Audio(url);

    // timeupdate event listener
    song.addEventListener('timeupdate', function() {
      var curtime = parseInt(song.currentTime, 10);
      tracker.slider('value', curtime);
    });

    $('.playlist li').removeClass('active');
    elem.addClass('active');
  }

  function playAudio() {

    song.play();

    tracker.slider("option", "max", song.duration);

    $('.play').addClass('hidden');
    $('.pause').addClass('visible');
  }

  function stopAudio() {
    song.pause();

    $('.play').removeClass('hidden');
    $('.pause').removeClass('visible');
  }

  // play click
  $('.play').click(function(e) {
    e.preventDefault();
    // playAudio();

    song.addEventListener('ended', function() {
      var next = $('.playlist li.active').next();
      if (next.length == 0) {
        next = $('.playlist li:first-child');
      }
      //Increase the number of music played
      addViewToTheMusic(next[0].getAttribute('label'));

      initAudio(next);

      song.addEventListener('loadedmetadata', function() {
        playAudio();
      });

    }, false);

    tracker.slider("option", "max", song.duration);
    //Increase the number of music played
    addViewToTheMusic($('.playlist li.active')[0].getAttribute('label'));

    song.play();
    $('.play').addClass('hidden');
    $('.pause').addClass('visible');

  });

  // pause click
  $('.pause').click(function(e) {
    e.preventDefault();
    stopAudio();
  });

  // next track
  $('.fwd').click(function(e) {
    e.preventDefault();

    stopAudio();

    var next = $('.playlist li.active').next();
    
    if (next.length === 0) {
      next = $('.playlist li:first-child');
    }
    //Increase the number of music played
    addViewToTheMusic(next[0].getAttribute('label'));

    initAudio(next);
    song.addEventListener('loadedmetadata', function() {
      playAudio();
    });
  });

  // prev track
  $('.rew').click(function(e) {
    e.preventDefault();

    stopAudio();

    var prev = $('.playlist li.active').prev();
    
    if (prev.length === 0) {
      prev = $('.playlist li:last-child');
    }
    //Increase the number of music played
    addViewToTheMusic(prev[0].getAttribute('label'));

    initAudio(prev);

    song.addEventListener('loadedmetadata', function() {
      playAudio();
    });
  });

  // show playlist
  $('.playlistIcon').click(function(e) {
    e.preventDefault();
    $('.playlist').toggleClass('show');
  });

  // playlist elements - click
  $('.playlist li').click(function() {
    stopAudio();
    initAudio($(this));
  });

  // initialization - first element in playlist
  initAudio($('.playlist li:first-child'));



  // empty tracker slider
  tracker.slider({

    range: 'min',
    min: 0,
    max: 10,
    start: function(event, ui) {},
    slide: function(event, ui) {
      song.currentTime = ui.value;
    },
    stop: function(event, ui) {}
  });

  $("#deletePage").click(function(){
        var slug = document.getElementById("slugPage").getAttribute('name');
        //var slug = $('#slugPage').getElementsByTagName('name');
        var response = confirm('Voulez-Vous vraiment supprimer cette page ?');

        if(response == true){
          window.location.href = "suppression_de_page?slug="+slug;  
        }
      });
});


<?php

declare(strict_types=1);

namespace Easyshare\Models;

use Easyshare\Core\BaseSQL;
use Easyshare\Core\Routing;
use Easyshare\Core\Form;
use Easyshare\Models\Category;

class Media extends BaseSQL
{
    protected $id = null ;
    protected $user_id ;
    protected $label ;
    protected $categ_media ;
    protected $count_media_played ;

    public function __construct()
    {
        parent::__construct();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUser_id()
    {
        return $this->user_id;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function getCateg_media()
    {
        return $this->categ_media;
    }

    public function getCount_media_played()
    {
        return $this->count_media_played;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setUser_id($userId)
    {
        $this->user_id = $userId ;
    }

    public function setLabel($label)
    {
        $this->label = $label;
    }

    public function setCateg_media($categ_media)
    {
        $this->categ_media = $categ_media;
    }

    public function setCount_media_played($count_media_played)
    {
        $this->count_media_played = $count_media_played ;
    }

    public function getAllCategory(): string
    {
        $category = new Category();
        $arrayDoubleDimensionLabel = $category->getAll('label');
        $arrayLabelList = Form::tranformDoubeDimensionArrayToAnSingle($arrayDoubleDimensionLabel);
        $stringLabelList = implode(';', $arrayLabelList);
        return $stringLabelList;
    }

    public function getFormAddMedia()
    {
        return [
            "config"=>[
                            "action"=>Routing::getSlug("Media", "saveMedia"),
                            "method"=>"POST",
                            "class"=>"",
                            "id"=>"",
                            "enctype"=>"multipart/form-data",
                            "reset"=>"Annuler",
                            "submit"=>"Enregistrer"
                        ],
            "data"=>[
                "label"=>[
                                "tag"=>"input",
                                "type"=>"text",
                                "message"=>"Le titre de votre musique",
                                "placeholder"=>"Saisir le titre sans l'extension ( .mp3 .aac )",
                                "class"=>"form-control",
                                "id"=>"labelMusic",
                                "required"=>true,
                                "minlength"=> "1",
                                "maxlength"=> "250",
                                "error"=>"Le nom de fichier doit être compris entre 1 et 250 caratères"
                            ],
                "category"=>[
                                "tag"=>"select",
                                "type"=>"",
                                "value"=> $this->getAllCategory(),
                                "message"=>"Le catégorie de votre musique",
                                "placeholder"=>"la categorie de la musique",
                                "class"=>"form-control",
                                "id"=>"categoryMusic",
                                "required"=>true,
                                "minlength"=> "1",
                                "maxlength"=> "250",
                                "error"=>"Le nom de la categorie doit être compris entre 1 et 250 caratères"
                            ],
                "music"=>[
                                "tag"=>"input",
                                "type"=>"file",
                                "message"=>"Votre musique :",
                                "placeholder"=>"",
                                "class"=>"form-control",
                                "id"=>"music",
                                "required"=>true,
                                //Pour mettre plusieur valeurs MIME, il faut les séparer avec: &
                                "extension"=>"audio/mp3 & audio/mpeg & audio/vnd.dlna.adts",
                                //Pour avoir différent valeurs de fin de fichier (mp3, aac), il faut les separer avec: &
                                "extensionEndOfFile"=>"mp3 & aac ",
                                "error"=>"Le fichier doît au format '.mp3' ou '.acc' ou , être inferieur à 30 Mega Octets et avoir un nom de fichier inferieur à 250 caratères"
                            ],
            ]
        ];
    }
}
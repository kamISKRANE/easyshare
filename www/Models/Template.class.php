<?php

declare(strict_types=1);

namespace Easyshare\Models;

use Easyshare\Core\BaseSQL;
use Easyshare\Core\Routing;

class Template extends BaseSQL
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getFormConfigStyleWebsite(): ?array
    {
        return [
            "config"=>[
                            "action"=>Routing::getSlug("Template", "saveConfigAdmin"),
                            "method"=>"POST",
                            "class"=>"",
                            "id"=>"form",
                            "reset"=>"Annuler",
                            "submit"=>"Appliquer les modification"
            ],
            "data"=>[
                    "colorText"=>[
                                    "tag"=>"input",
                                    "type"=>"text",
                                    "placeholder"=>"Couleur en anglais ou en hexadécimal (blue, red, #035ee8, #c1fa00)",
                                    "message"=>"la couleur de votre text",
                                    "class"=>"",
                                    "id"=>"color",
                                    "required"=>true,
                                    "minlength"=>2,
                                    "maxlength"=>8,
                                    "error"=>"Veuillez ajouter une couleur en hexadécimal ou en anglais, entre 2 et 8 caractères"
                    ],
                    "fontSizeText"=>[
                                    "tag"=>"input",
                                    "type"=>"text",
                                    "placeholder"=>"Ex : 14px, 16px, 20px ect...",
                                    "message"=>"La taille d'écriture",
                                    "class"=>"",
                                    "id"=>"color",
                                    "required"=>true,
                                    "minlength"=>3,
                                    "error"=>"Veuillez respecter le format minimum 3 caractères , exemple pour une taille de 16 :' 16px '"
                    ],
                    "fontFamilyText"=>[
                                    "tag"=>"input",
                                    "type"=>"text",
                                    "placeholder"=>"Arial, Verdana, Georgia ",
                                    "message"=>"la police d'ecriture",
                                    "class"=>"",
                                    "id"=>"color",
                                    "required"=>true,
                                    "minlength"=>2,
                                    "maxlength"=>60,
                                    "error"=>"Veuillez rentrer une police d'écriture valide, entre 2 et 60 caractères"
                    ]
            ]
        ];
    }
}

<?php

declare(strict_types=1);

namespace Easyshare\Models;

use Easyshare\Core\BaseSQL;

class Menu extends BaseSQL
{
    public $id;
    public $titre;
    public $href;
    public $contenu;
    public $default_menu;

    public function __construct($titre = null)
    {
        parent::__construct();
        $this->titre = $titre;
        $this->href = str_replace(' ', '_', $titre);
        $this->contenu = "";
        $this->default_menu = 0;
    }

    public function initMenu($id, string $mName, $contenu)
    {
        $this->id = $id;
        $this->titre = $mName;
        $this->href = str_replace(' ', '_', $mName);
        $this->contenu = $contenu;
    }

    public function setId($menu_id): void
    {
        $this->id = $menu_id;
    }

    public function setTitre($titre): void
    {
        $this->titre = $titre;
    }

    public function setHref($href): void
    {
        $this->href = $href;
    }

    public function setContenu($contenu): void
    {
        $this->contenu = $contenu;
    }

    public function addContenu($contenu): void
    {
        $this->contenu = $contenu;
    }

    public function getMenu(): Menu
    {
        return $this->contenu;
    }

    public function setMenuDefault(): void
    {
        $this->default_menu = 1;
    }
}

<?php

declare(strict_types=1);

namespace Easyshare\Models;

use Easyshare\Core\BaseSQL;

class Comment extends BaseSQL
{
    public $id = null;
    public $user_id;
    public $page_id = 1;
    public $content;
    private $create_date;
    private $update_date;

    public function __construct()
    {
        parent::__construct();
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    public function setPageId($page_id)
    {
        $this->page_id = $page_id;
    }

    public function setContent($content)
    {
        $this->content = $content;
    }

    public function getId(): ?integer
    {
        return $this->id;
    }

    public function getUserId(): ?integer
    {
        return $this->user_id;
    }

    public function getPageId(): ?integer
    {
        return $this->page_id;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function getCreateDate(): ?DateTime
    {
        return $this->create_date;
    }

    public function getUpdateDate(): ?DateTime
    {
        return $this->update_date;
    }
}

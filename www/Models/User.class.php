<?php

declare(strict_types=1);
namespace Easyshare\Models;

use Easyshare\Core\BaseSQL;
use Easyshare\Core\Routing;

class User extends BaseSQL
{
    public $id = null;
    public $role_id = 2;
    public $firstname;
    public $lastname;
    public $email;
    public $password;
    public $birth_date;
    public $active = 0;
    public $token;

    public function __construct()
    {
        parent::__construct();
    }

    public function initUser($user) : void
    {
        $this->id = $user["id"];
        $this->role_id = $user["role_id"];
        $this->firstname = $user["firstname"];
        $this->lastname = $user["lastname"];
        $this->email = $user["email"];
        $this->password = $user["password"];
        $this->birth_date = $user["birth_date"];
        $this->active = intval($user["active"]);
    }

    public function setId($id) : void
    {
        $this->id=$id;
    }

    public function setFirstname($firstname) : void
    {
        $this->firstname = ucwords(strtolower(trim($firstname)));
    }

    public function setLastname($lastname) : void
    {
        $this->lastname = strtoupper(trim($lastname));
    }

    public function setEmail($email) : void
    {
        $this->email = htmlspecialchars(strtolower(trim($email)), ENT_QUOTES);
    }

    public function setPwd($password) : void
    {
        $this->password = password_hash($password, PASSWORD_DEFAULT);
    }

    public function setDateNaissance($date_naiss) : void
    {
        $this->birth_date = $date_naiss;
    }

    public function setRoleId($role_id) : void
    {
        $this->role_id=$role_id;
    }

    public function setActive($active) : void
    {
        $this->active = $active;
    }

    public function setToken($key): void
    {
        $this->token = $key;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getFirstname() : ?string
    {
        return $this->firstname;
    }

    public function getLastname() : ?string
    {
        return $this->lastname;
    }

    public function getEmail() : ?string
    {
        return $this->email;
    }

    public function getPassword() : ?string
    {
        return $this->password;
    }

    public function getDateNaissance() : ?string
    {
        return $this->birth_date;
    }

    public function getRoleId() : ?string
    {
        return $this->role_id;
    }

    public function getActive() : ?int
    {
        return $this->active;
    }

    public function getTest(): ?string
    {
        return "Je suis";
    }

    public function insertDataIntoFormUpdateUser($data, $form)
    {
        foreach ($form['data'] as $name => $elements) {
            foreach ($data as $key => $value) {
                if (isset($elements['index']) && $elements['index'] == $key) {
                    $form['data'][$name]['value'] = $value;

                    if ($elements['index'] == "label" && $name == "newUrl") {
                        $form['data'][$name]['value'] = preg_replace('#/#', '', $value);
                    }
                }
            }
        }
        return $form;
    }

	public function getFormRegister () : ?array
	{
		return [
					"config"=>[
									"action"=>Routing::getSlug("User", "save"),
									"method"=>"POST",
									"class"=>"",
									"id"=>"",
									"reset"=>"Annuler",
									"submit"=>"S'enregistrer"
								],
					"data"=>[
						"lastname"=>[
										"tag"=>"input",
										"type"=>"text",
										"placeholder"=>"Votre nom",
										"message"=> "",
										"value"=>"ISKRANE",
										"class"=>"form-group",
										"id"=>"lastname",
										"name"=>"lastname",
										"required"=>true,
										"minlength"=>2,
										"maxlength"=>100,
										"error"=>"Votre nom doit faire entre 2 et 100 caractères"
									],
						"firstname"=>[
										"tag"=>"input",
										"type"=>"text",
										"placeholder"=>"Votre prénom",
										"message"=> "",
										"value"=>"Kamel",
										"class"=>"form-group",
										"id"=>"firstname",
										"name"=>"firstname",
										"required"=>true,
										"minlength"=>2,
										"maxlength"=>50,
										"error"=>"Votre prénom doit faire entre 2 et 50 caractères"
									],
						"email"=>[
										"tag"=>"input",
										"type"=>"email",
										"placeholder"=>"Votre email",
										"message"=> "",
										"value"=>"kamel.iskrane@gmail.comm",
										"class"=>"form-group",
										"id"=>"email",
										"name"=>"email",
										"required"=>true,
										"minlength"=>7,
										"maxlength"=>250,
										"error"=>"Votre email est incorrect ou fait plus de 250 caractères"
									],
						"date_naissance"=>[
										"tag"=>"input",
										"type"=>"date",
										"placeholder"=>"Votre date de naissance",
										"message"=> "",
										"class"=>"form-group",
										"id"=>"birth",
										"name"=>"anniversaire",
										"required"=>true,
										"error"=>"Veuillez fournir une année de naissance correcte."
									],
						"pass"=>	[
										"tag"=>"input",
										"type"=>"password",
										"placeholder"=>"Votre mot de passe",
										"message"=> "",
										"value"=>"Test75011!",
										"class"=>"form-group",
										"id"=>"pass",
										"name"=>"pass",
										"required"=>true,
										"minlength"=>6,
										"error"=>"Votre mot de passe doit faire plus de 6 caractères avec des minuscules, majuscules et des chiffres"
									],
						"re_pass"=>[
										"tag"=>"input",
										"type"=>"password",
										"placeholder"=>"Confirmation",
										"message"=> "",
										"value"=>"Test75011!",
										"class"=>"form-group",
										"id"=>"re_pass",
										"name"=>"re_pass",
										"required"=>true,
										"confirm"=>"pass",
										"error"=>"Le mot de passe de confirmation ne correspond pas"
									],
					]
				];
	}

    public function getFormLogin() : array
    {
        $form = [
            "config" =>[
                        "action"=>Routing::getSlug("User", "loginValidation"),
                        "method"=>"POST",
                        "class"=>"form-control",
                        "id"=>"login-forms",
                        "reset"=>"Annuler",
                        "submit"=>"Connexion"
                        ],
            "data" =>[
                    "email"=>[
                            "tag"=>"input",
                            "type"=>"email",
                            "placeholder"=>"Votre email",
                            "message"=> "",
                            "class"=>"form-control",
                            "id"=>"your_mail",
                            "name"=>"your_mail",
                            "required"=>true,
                            "error"=>"Votre email est incorrect"
                            ],
                    "pwd"=>	[
                            "tag"=>"input",
                            "type"=>"password",
                            "placeholder"=>"Votre mot de passe",
                            "message"=> "",
                            "class"=>"form-control",
                            "id"=>"your_pass",
                            "name"=>"your_pass",
                            "required"=>true,
							"error"=>"Votre mot de passe doit faire plus de 6 caractères avec des minuscules, majuscules et des chiffres"
                        ]
                ]
            ];
        return $form;
    }

    public function getFormPasswordForgot(): ?array
    {
        $form = [
            "config" =>[
                        "action"=>Routing::getSlug("User", "reinitPassword"),
                        "method"=>"POST",
                        "class"=>"form-control",
                        "id"=>"forgotenPassword-form",
                        "reset"=>"Annuler",
                        "submit"=>"Réinitialiser"
                        ],
            "data" =>[
                    "email"=>[
                            "tag"=>"input",
                            "type"=>"email",
                            "placeholder"=>"Entrer votre adresse e-mail",
                            "message"=> "",
                            "class"=>"form-control",
                            "id"=>"inputEmail",
                            "name"=>"inputEmail",
                            "required"=>true,
                            "error"=>"Votre email est incorrect"
                            ]
                ]
            ];
        return $form;
    }

    public function getFormNewPassword(): ?array
    {
        $form = [
            "config" =>[
                        "action"=>Routing::getSlug("User", "changePasswordSubmit"),
                        "method"=>"POST",
                        "class"=>"form-control",
                        "id"=>"forgotenPassword-form",
                        "reset"=>"Annuler",
                        "submit"=>"Réinitialiser"
                        ],
            "data" =>[
                    "pass"=>	[
                                    "tag"=>"input",
                                    "type"=>"password",
                                    "placeholder"=>"Votre mot de passe",
                                    "message"=> "",
                                    "class"=>"form-group",
                                    "id"=>"pass",
                                    "name"=>"pass",
                                    "required"=>true,
                                    "minlength"=>6,
                                    "error"=>"Votre mot de passe doit faire plus de 6 caractères avec des minuscules, majuscules et des chiffres"
                                ],
                    "re_pass"=>[
                                    "tag"=>"input",
                                    "type"=>"password",
                                    "placeholder"=>"Confirmation",
                                    "message"=> "",
                                    "class"=>"form-group",
                                    "id"=>"re_pass",
                                    "name"=>"re_pass",
                                    "required"=>true,
                                    "confirm"=>"pass",
                                    "error"=>"Le mot de passe de confirmation ne correspond pas"
                                ]
                ]
            ];
        return $form;
    }

    public function getFormUpdate() : ?array
    {
        return [
                    "config"=>[
                                    "action"=>Routing::getSlug("User", "updateProfile"),
                                    "method"=>"POST",
                                    "class"=>"",
                                    "id"=>"",
                                    "reset"=>"Annuler",
                                    "submit"=>"Valider"
                                ],
                    "data"=>[
                        "lastname"=>[
                                        "tag"=>"input",
                                        "type"=>"text",
                                        "placeholder"=>"Votre nom",
                                        "message"=> "Nom",
                                        "class"=>"form-group",
                                        "id"=>"lastname",
                                        "value"=>"",
                                        "index"=>"lastname",
                                        "required"=>true,
                                        "minlength"=>2,
                                        "maxlength"=>100,
                                        "error"=>"Votre nom doit faire entre 2 et 100 caractères"
                                    ],
                        "firstname"=>[
                                        "tag"=>"input",
                                        "type"=>"text",
                                        "placeholder"=>"Votre prénom",
                                        "message"=> "Prénom",
                                        "class"=>"form-group",
                                        "id"=>"firstname",
                                        "value"=>"",
                                        "index"=>"firstname",
                                        "required"=>true,
                                        "minlength"=>2,
                                        "maxlength"=>50,
                                        "error"=>"Votre prénom doit faire entre 2 et 50 caractères"
                                    ],
                        "email"=>[
                                        "tag"=>"input",
                                        "type"=>"text",
                                        "placeholder"=>"Votre email",
                                        "message"=> "Adresse Mail",
                                        "class"=>"form-group",
                                        "id"=>"email",
                                        "value"=>"",
                                        "index"=>"email",
                                        "required"=>true,
                                        "minlength"=>7,
                                        "maxlength"=>250,
                                        "error"=>"Votre email est incorrect ou fait plus de 250 caractères"
                                    ],
                        "date_naissance"=>[
                                        "tag"=>"input",
                                        "type"=>"date",
                                        "placeholder"=>"Votre date de naissance",
                                        "message"=> "Date de naissance",
                                        "class"=>"form-group",
                                        "id"=>"birth",
                                        "value"=>"",
                                        "index"=>"birth_date",
                                        "required"=>false,
                                        "error"=>"Veuillez fournir une année de naissance correcte."
                                    ],
                        "pass"=>	[
                                        "tag"=>"input",
                                        "type"=>"password",
                                        "placeholder"=>"Votre mot de passe",
                                        "message"=> "Votre mot de passe",
                                        "class"=>"form-group",
                                        "id"=>"pass",
                                        "name"=>"pass",
                                        "required"=>false,
                                        "minlength"=>6,
                                        "error"=>"Votre mot de passe doit faire plus de 6 caractères avec des minuscules, majuscules et des chiffres"
                                    ],
                        "re_pass"=>[
                                        "tag"=>"input",
                                        "type"=>"password",
                                        "placeholder"=>"Confirmation",
                                        "message"=> "Confirmation de mot de passe",
                                        "class"=>"form-group",
                                        "id"=>"re_pass",
                                        "name"=>"re_pass",
                                        "required"=>false,
                                        "confirm"=>"pass",
                                        "error"=>"Le mot de passe de confirmation ne correspond pas"
                                    ]
                    ]
                ];
    }
}

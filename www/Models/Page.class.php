<?php

    declare(strict_types=1);
    namespace Easyshare\Models;

use Easyshare\Core\BaseSQL;
    use Easyshare\Core\Routing;

    class Page extends BaseSQL
    {
        protected $id = null;
        protected $label ; 		//URL ex : '/blog_actu'
        protected $title ;
        protected $content ;
        protected $available;
        protected $comment;
        protected $homepage;
        protected $description;
        protected $count_views ;

        public function __construct()
        {
            parent::__construct();
        }

        //Getters
        public function getId()
        {
            return $this->id;
        }

        public function getLabel()
        {
            return $this->label;
        }

        public function getTitle()
        {
            return $this->title;
        }

        public function getContent()
        {
            return $this->content;
        }

        public function getAvailable()
        {
            return $this->available ;
        }

        public function getComment()
        {
            return $this->available ;
        }

        public function getHomepage()
        {
            return $this->homepage ;
        }

        public function getDescription()
        {
            return $this->description ;
        }
        public function getCount_views()
        {
            return $this->count_views;
        }

        //Setters
        public function setId($id)
        {
            $this->id = $id;
        }

        public function setLabel($label)
        {
            $this->label = $label;
        }

        public function setTitle($title)
        {
            $this->title = $title;
        }

        public function setContent($content)
        {
            $this->content = $content;
        }

        public function setAvailable($available)
        {
            $this->available = $available;
        }
        public function setComment($comment)
        {
            $this->comment = $comment;
        }

        public function setHomepage($homepage)
        {
            $this->homepage = $homepage;
        }

        public function setDescription($description)
        {
            $this->description = $description;
        }

        public function setCount_views($count_views)
        {
            $this->count_views = $count_views;
        }

        /*	public function setContent($content){
            $this->content = $this->formatContent($content);
        }*/

        //Methode a Ameliorer Car les guilements ne sont pas pris en compte
        
        //Pour l'instant ces deux méthode ne sont pas utiliser, on les garde au cas ou on a besoin.
        //Methode a Ameliorer Car les guilements ne sont pas pris en compte
        public function formatContent($target)
        {
            $recherche = array('@&(lt|#60);@i', '@&(gt|#62);@i' , '@&amp;nbsp;@i');
             
            $remplace = array('<', '>', ' ');
            $text = preg_replace($recherche, $remplace, $target);
            return($text);
        }
        public function formatContentReverse($target)
        {
            $remplace = array('<', '>');
            $recherche = array('@&(lt|#60);@i', '@&(gt|#62);@i');
             
            $text = preg_replace($recherche, $remplace, $target);
            return($text);
        }

        //Permet d'inserer les valeurs stockées en Bdd directement dans le model du formulaire, qui sera ensuite généré avec form.mod.php
        public function insertDataIntoFormPage($data, $form)
        {
            foreach ($form['data'] as $name => $elements) {
                foreach ($data as $key => $value) {
                    if (isset($elements['index']) && $elements['index'] == $key) {
                        $form['data'][$name]['value'] = $value;

                        if ($elements['index'] == "label" && $name == "newUrl") {
                            $form['data'][$name]['value'] = preg_replace('#/#', '', $value);
                        }
                    }
                }
            }
            return $form;
        }

        public function getFormCreatePage(): ?array
        {
            return [
                "config"=>[
                                "action"=>Routing::getSlug("Page", "savePage"),
                                "method"=>"POST",
                                "class"=>"",
                                "id"=>"form",
                                "reset"=>"Annuler",
                                "submit"=>"Créer la page",
                                "onsubmit"=>"return checkIfUrlIsAvailable()"
                ],
                "data"=>[
                        "url"=>[
                                        "tag"=>"input",
                                        "type"=>"text",
                                        "placeholder"=>"Url",
                                        "message"=>"Le lien de votre page : ",
                                        "class"=>"",
                                        "id"=>"url",
                                        "required"=>true,
                                        "minlength"=>2,
                                        "maxlength"=>100,
                                        "error"=>"Veuillez effectuer un lien correct, par exemple :'blog_music'."
                        ],
                        "available"=>[
                                        "tag"=>"input",
                                        "type"=>"checkbox",
                                        "placeholder"=>"",
                                        "message"=> "Cochez la case pour rendre la page visible ",
                                        "class"=>"",
                                        "id"=>"available",
                                        "required"=>false,
                                        "error"=>"Veuillez effectuer un choix correct"
                        ],
                        "comment"=>[
                                        "tag"=>"input",
                                        "type"=>"checkbox",
                                        "placeholder"=>"",
                                        "message"=> "Cochez la case pour activer les commentaires sur la page",
                                        "class"=>"",
                                        "id"=>"comment",
                                        "required"=>false,
                                        "error"=>"Veuillez effectuer un choix correcte"
                        ],
                        "homepage"=>[
                                        "tag"=>"input",
                                        "type"=>"checkbox",
                                        "placeholder"=>"",
                                        "message"=> "Cochez la case si vous voulez que ce soit votre page d'accueil",
                                        "class"=>"",
                                        "id"=>"homepage",
                                        "required"=>false,
                                        "error"=>"Veuillez effectuer un choix correct"
                        ],
                        "title"=>[
                                        "tag"=>"input",
                                        "type"=>"text",
                                        "placeholder"=>"Titre",
                                        "message"=> "Le titre de la page ",
                                        "class"=>"",
                                        "id"=>"titlepage",
                                        "required"=>true,
                                        "minlength"=>2,
                                        "maxlength"=>100,
                                        "error"=>"Veuillez fournir un titre de page correcte"
                        ],
                        "ckeditor"=>[
                                        "tag"=>"textarea",
                                        "type"=>"",
                                        "placeholder"=>"",
                                        "message"=> "Contenu de la page",
                                        "class"=>"",
                                        "id"=>"editor",
                                        "required"=>true,
                                        "error"=>"Le contenu de la page est vide, veuillez la remplir"
                        ],
                        "description"=>[
                                        "tag"=>"textarea",
                                        "type"=>"text",
                                        "placeholder"=>"Entrez une description",
                                        "message"=> "Votre description permettra de mieux reférencer la page",
                                        "class"=>"desc",
                                        "id"=>"description",
                                        "required"=>true,
                                        "minlength"=>2,
                                        "maxlength"=>160,
                                        "error"=>"Veuillez fournir une description correcte, inférieur à 160 caractères"
                        ],
                ]
                        
            ];
        }

        public function getFormUpdatePage(): ?array
        {
            return [
                "config"=>[
                                "action"=>Routing::getSlug("Page", "saveUpdatedPage"),
                                "method"=>"POST",
                                "class"=>"",
                                "id"=>"form",
                                "reset"=>"Annuler",
                                "submit"=>"modifier la page",
                                "onsubmit"=>"return checkIfUrlIsAvailable()"
                ],
                "data"=>[
                        "newUrl"=>[
                                        "tag"=>"input",
                                        "type"=>"text",
                                        "placeholder"=>"Le lien Ex :'blog_music'",
                                        "message"=>"",
                                        "class"=>"",
                                        "id"=>"newUrl",
                                        "value"=>"",
                                        "index"=>"label",
                                        "required"=>true,
                                        "minlength"=>2,
                                        "maxlength"=>100,
                                        "error"=>"Veuillez fournir un lien correct, par exemple :'blog_music'."
                        ],
                        "available"=>[
                                        "tag"=>"input",
                                        "type"=>"checkbox",
                                        "placeholder"=>"",
                                        "message"=> "Cochez la case pour rendre la page visible ",
                                        "class"=>"",
                                        "id"=>"available",
                                        "value"=>"",
                                        "index"=>"available",
                                        "required"=>false,
                                        "error"=>"Veuillez effectuer un choix correct"
                        ],
                        "comment"=>[
                                        "tag"=>"input",
                                        "type"=>"checkbox",
                                        "placeholder"=>"",
                                        "message"=> "Cochez la case pour activer les commentaires sur la page",
                                        "class"=>"",
                                        "id"=>"comment",
                                        "value"=>"",
                                        "index"=>"comment",
                                        "required"=>false,
                                        "error"=>"Veuillez effectuer un choix correct"
                        ],
                        "homepage"=>[
                                        "tag"=>"input",
                                        "type"=>"checkbox",
                                        "placeholder"=>"",
                                        "message"=> "Cochez la case si vous voulez que ce soit votre page d'accueil",
                                        "class"=>"",
                                        "id"=>"homepage",
                                        "value"=>"",
                                        "index"=>"homepage",
                                        "required"=>false,
                                        "error"=>"Veuillez effectuer un choix correct"
                        ],
                        "title"=>[
                                        "tag"=>"input",
                                        "type"=>"text",
                                        "placeholder"=>"Le titre de la page",
                                        "message"=> "",
                                        "class"=>"",
                                        "id"=>"title",
                                        "value"=>"",
                                        "index"=>"title",
                                        "required"=>true,
                                        "minlength"=>2,
                                        "maxlength"=>100,
                                        "error"=>"Veuillez fournir un titre de page correcte"
                        ],
                        "ckeditor"=>[
                                        "tag"=>"textarea",
                                        "type"=>"",
                                        "placeholder"=>"",
                                        "message"=> "",
                                        "class"=>"",
                                        "id"=>"editor",
                                        "value"=>'',
                                        "index"=>"content",
                                        "required"=>true,
                                        "error"=>"Le contenu de la page est vide, veuillez la remplir"
                        ],
                        "description"=>[
                                        "tag"=>"textarea",
                                        "type"=>"text",
                                        "placeholder"=>"Entrez une description",
                                        "message"=> "Votre description permettra de mieux reférencer la page",
                                        "class"=>"",
                                        "id"=>"description",
                                        "value"=>"",
                                        "index"=>"description",
                                        "required"=>true,
                                        "minlength"=>2,
                                        "maxlength"=>160,
                                        "error"=>"Veuillez fournir une description correcte, inférieur à 160 caractères"
                        ],
                        "oldUrl"=>[
                                        "tag"=>"input",
                                        "type"=>"hidden",
                                        "placeholder"=>"E",
                                        "class"=>"",
                                        "id"=>"oldUrl",
                                        "required"=>"",
                                        "value"=>"",
                                        "index"=>"label"
                        ]
                ]
                        
            ];
        }
    }

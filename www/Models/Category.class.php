<?php

declare(strict_types=1);

namespace Easyshare\Models;

use Easyshare\Core\BaseSQL;
use Easyshare\Core\Routing;

class Category extends BaseSQL
{
    protected $id = null ;
    protected $label ;

    public function __construct()
    {
        parent::__construct();
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function setLabel($label): void
    {
        $this->label = $label ;
    }

    public function getFormAddCategory(): ?array
    {
        return [
            "config"=>[
                            "action"=>Routing::getSlug("Category", "saveCategory"),
                            "method"=>"POST",
                            "class"=>"",
                            "id"=>"",
                            "enctype"=>"multipart/form-data",
                            "reset"=>"Annuler",
                            "submit"=>"Enregistrer"
                        ],
            "data"=>[
                "label"=>[
                                "tag"=>"input",
                                "type"=>"text",
                                "message"=>"Le nom de votre catégorie de musique",
                                "placeholder"=>"Jazz, Rap, Rock, Dance - Electro, Hip Hop - RnB",
                                "class"=>"form-control",
                                "id"=>"labelCategory",
                                "required"=>true,
                                "minlength"=> "1",
                                "maxlength"=> "250",
                                "error"=>"Le nom de la categorie doit être compris entre 1 et 250 caratères"
                            ],
            ]
        ];
    }
}

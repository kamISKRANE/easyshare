#!/bin/bash

cp /main.cf /etc/postfix/main.cf

echo "[smtp.gmail.com]:587 easyshare.esgi@gmail.com:Easyshare75!" \
>> /etc/postfix/sasl_passwd

postmap /etc/postfix/sasl_passwd

chown root:root /etc/postfix/sasl_passwd /etc/postfix/sasl_passwd.db
chmod 0600 /etc/postfix/sasl_passwd /etc/postfix/sasl_passwd.db

postconf compatibility_level=2

service postfix restart
postfix start
service rsyslog start